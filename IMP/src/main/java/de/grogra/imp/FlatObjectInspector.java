package de.grogra.imp;

import java.util.EventObject;
import java.util.Iterator;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.Node.NType;
import de.grogra.pf.ui.Context;

public class FlatObjectInspector extends ObjectInspector {

	public FlatObjectInspector(Context ctx, GraphManager graph) {
		super(ctx, graph);
	}
	
	@Override
	public void initialize() {
	}
	
	@Override
	public void buildTree() {
		Node graphRoot = graph.getRoot();
		rootNode = new TreeNode("Nodes", null);
		visitNodes(graphRoot, rootNode);
	}
	
	private void visitNodes(Node node, TreeNode rootNode) {
		if (filter != null) {
			if (hierarchicFilter) {
				Iterator<NType> it = filter.iterator();
				while (it.hasNext()) {
					NType type = it.next();
					if (type.isInstance(node)) {
						rootNode.addChild(new TreeNode(node, rootNode));
						break;
					}
				}
			}
			else {
				if (filter.contains(node.getNType()) && (node.getId() != 0L))
					rootNode.addChild(new TreeNode(node, rootNode));
			}
		}
		else if (node.getId() != 0L)
			rootNode.addChild(new TreeNode(node, rootNode));
		
		for (Edge edge = node.getFirstEdge(); edge != null; edge = edge.getNext(node)) {
			if (edge.getTarget() == node)
				continue;
			if (edge.testEdgeBits(Graph.SUCCESSOR_EDGE) ||  edge.testEdgeBits(Graph.BRANCH_EDGE))
				visitNodes(edge.getTarget(), rootNode);
		}
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getParent(Object child) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean nodesEqual(Object a, Object b) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getType(Object node) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName(Object node) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAvailable(Object node) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled(Object node) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object resolveLink(Object node) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getDescription(Object node, String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eventOccured(Object node, EventObject event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object invoke(Object node, String method, Object arg) {
		// TODO Auto-generated method stub
		return null;
	}

}
