package de.grogra.imp;

import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.EventObject;

import javax.swing.SwingUtilities;

import de.grogra.persistence.ShareableBase;
import de.grogra.util.DisposableEventListener;

/**
 * The <code>NavigatorFactory</code> is the factory for the <code>Navigator3D</code>.
 * It defines the activation events that trigger the navigation mode.
 * The activation events are: 
 * <ul>
 *   <li>Right mouse button pressed.</li>
 *   <li>Wheel used.</li>
 *   <li>Middle button pressed.</li>
 *   <li>Left mouse button dragged over empty space.</li>
 * </ul>
 */
public abstract class NavigatorFactory extends ShareableBase
{
	public boolean isActivationEvent (EventObject e)
	{
		if (!(e instanceof MouseEvent))
		{
			return false;
		}
		MouseEvent me = (MouseEvent) e;
		return (SwingUtilities.isRightMouseButton(me) && (me.getID () == MouseEvent.MOUSE_PRESSED)) || 
			   (SwingUtilities.isMiddleMouseButton(me) && (me.getID () == MouseEvent.MOUSE_PRESSED)) ||
			   (e instanceof MouseWheelEvent) ||
			   (SwingUtilities.isLeftMouseButton(me) && (me.getID () == MouseEvent.MOUSE_DRAGGED));
	}


	public abstract DisposableEventListener createNavigator
		(ViewEventHandler h, EventObject e);
}
