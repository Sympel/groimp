
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.imp;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.SwingUtilities;

import de.grogra.graph.ArrayPath;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphState;
import de.grogra.graph.GraphUtils;
import de.grogra.graph.Path;
import de.grogra.imp.edit.Tool;
import de.grogra.imp.edit.ToolRoot;
import de.grogra.imp.edit.ViewSelection;
import de.grogra.imp.registry.ToolFactory;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.event.ClickEvent;
import de.grogra.pf.ui.event.DragEvent;
import de.grogra.util.Disposable;
import de.grogra.util.DisposableEventListener;
import de.grogra.util.EventListener;
import de.grogra.util.Map;
import de.grogra.util.Utils;

public abstract class ViewEventHandler implements Disposable, EventListener
{
	private boolean disposed = false;
	private JobManager jm; 
	private View view;
	private DisposableEventListener navigator = null;

	private PickList list, list2;
	private PickElement pickInfo = new PickElement ();
	private ArrayPath lastSelected = new ArrayPath ((Graph) null);
	private ArrayPath lastPicked = new ArrayPath ((Graph) null);

	private static final int NOTHING = 0, HIGHLIGHTED = 1, SELECTED = 2;
	private int highlightState = NOTHING, highlightIndex;

	private static final int NORMAL = 1, NAVIGATING = 2, DRAGGING = 3;
	private int state = NORMAL;

	private MouseEvent dragEvent = null, pressEvent = null;
	private int pickX, pickY, lastDragX, lastDragY;

	private int tolerance = 10, highlightDelay = 50;

	private final int[] a2 = new int[2];
	
	private int chX = -1, chY;


	private class Unhighlight implements Command
	{
		private boolean canceled = false;

		void cancel ()
		{
			canceled = true;
		}

		public String getCommandName ()
		{
			return null;
		}

		public void run (Object info, Context ctx)
		{
			if (!canceled)
			{
				resetHighlight ();
			}
		}
	}

	private Unhighlight unhighlight = null;


	public ViewEventHandler (View view, boolean allowNegativePickDist)
	{
		this.view = view;
		jm = view.getWorkbench ().getJobManager ();
		list = new PickList (10, allowNegativePickDist); 
		list2 = new PickList (10, allowNegativePickDist); 
	}


	public void dispose ()
	{
		if (disposed)
		{
			return;
		}
		disposed = true;
		if (unhighlight != null)
		{
			unhighlight.cancel ();
			unhighlight = null;
		}
		list.reset ();
		list2.reset ();
		pickInfo = null;
		if (navigator != null)
		{
			navigator.dispose ();
			navigator = null;
		}
		dragEvent = null;
		view = null;
		jm = null;
	}


	public final View getView ()
	{
		return view;
	}


	private void setState (int newState)
	{
		if (state == newState)
		{
			return;
		}
		switch (state)
		{
			case NORMAL:
				if (unhighlight != null)
				{
					unhighlight.cancel ();
					unhighlight = null;
				}
				if (newState != DRAGGING)
				{
					resetHighlight ();
				}
				break;
			case NAVIGATING:
				navigator.dispose ();
				navigator = null;
				break;
			case DRAGGING:
				if (dragEvent != null)
				{
					mouseDragged (dragEvent, DragEvent.DRAGGING_FINISHED,
								  0, 0);
					dragEvent = null;
				}
				break;
		}
		state = newState;
	}


	public void disposeNavigator (EventObject e)
	{
		setState (NORMAL);
		if (e != null)
		{
			eventOccured (e);
		}
	}

	protected abstract NavigatorBase getNavigatorBase ();

	
	/**
	* The events are managed as follow:
	* <ul>
	*  <li>The view check if the navigation mode is currently used (i.e. if the mouse is already moving the navigation).
	*  If its the case then, the navigation continue.
	*  </li>
	*  <li>If the view is not in navigation mode, check if an object is selected or if the mouse is over one 
	*  (objects are highlighted when selected or covered by the mouse). If its the case resolve the object 
	*  interaction first.
	*  </li>
	*  <li>If no objects are selected, check if the navigation is triggered. 
	*  (see <code>NavigatorFactory</code> for the navigation triggers). 
	*  </li>
	*  <li>If no other activation occured, the object interaction is processed by default. </li>
	* </ul>
	* @see manageObject(EventObject e)
	* @see manageNavigation(EventObject e, NavigatorBase nf)
	*/
	public void eventOccured (EventObject e)
	{
		try
		{
			if (e instanceof MouseEvent) {
				if (((MouseEvent) e).getID() == MouseEvent.MOUSE_PRESSED){
					boolean t = false;
				}
			}
			View.set (GraphState.current (view.getGraph ()), view);
			NavigatorBase nf = getNavigatorBase();
			switch (state) {
			case NAVIGATING:
				manageNavigation(e, nf);
				break;
			case DRAGGING:
				manageTool(e);
				break;
			case NORMAL:
				if (isHMouseOnTool() && (e instanceof MouseEvent) &&
					((MouseEvent) e).getID() == MouseEvent.MOUSE_DRAGGED) {
					startToolManagement(e);
					break;
				}
				else if ((nf != null) && nf.isActivationEvent (e)) {
					startNavigation(e, nf);
					break;
				}
				
				manageObject(e);
			}
			manageView(e);

		}
		finally
		{
			View.set (GraphState.current (view.getGraph ()), null);
		}
	}
	
	private void manageObject(EventObject e) {
		if (!(e instanceof MouseEvent) || UI.isConsumed(e))
		{
			return;
		}
		MouseEvent event = (MouseEvent) e;
		switch (event.getID ())
		{
			case MouseEvent.MOUSE_PRESSED:
				if (state == NORMAL)
				{
					pressEvent = event;
					buttonClicked (event);
					break;
				}
				break;
			case MouseEvent.MOUSE_RELEASED:
				if (state == NORMAL)
				{
					buttonClicked (event);
				}
				else
				{
					setState (NORMAL);
				}
				break;
			case MouseEvent.MOUSE_CLICKED:
				if (state == NORMAL)
				{
					handleClick (event);
					if (!event.isConsumed ())
					{
						buttonClicked (event);
					}
				}
				break;
		}	
	}
	
	private void manageView(EventObject e) {
		if (!(e instanceof MouseEvent) || UI.isConsumed(e))
		{
			return;
		}
		MouseEvent event = (MouseEvent) e;
		switch (event.getID ())
		{
			case MouseEvent.MOUSE_MOVED:
				if (state != NORMAL)
				{
					setState (NORMAL);
				}
				mouseMoved (event);
				event.consume ();
				break;
			case MouseEvent.MOUSE_EXITED:
				enqueueUnhighlight ();
				break;
		}	
	}

	
	private void manageTool(EventObject e) {
		if (!(e instanceof MouseEvent) || UI.isConsumed(e))
		{
			return;
		}
		MouseEvent event = (MouseEvent) e;
		if (event.getID () == MouseEvent.MOUSE_DRAGGED) {
				dragEvent = event;
				if (pressEvent != null)
				{
					mouseDragged (pressEvent,
								  DragEvent.DRAGGING_STARTED, 0, 0);
					lastDragX = pressEvent.getX ();
					lastDragY = pressEvent.getY ();
					pressEvent = null;
				}
				mouseDragged (event, DragEvent.DRAGGING_CONTINUED,
							  event.getX () - lastDragX,
							  event.getY () - lastDragY);
				lastDragX = event.getX ();
				lastDragY = event.getY ();
				event.consume ();
			}
	}
	
	private void startToolManagement(EventObject e) {
		setState (DRAGGING);
	}
	
	private boolean isHMouseOnTool() {

		boolean tool = false;
		for (int s = 0; s < list.getSize (); s++)
		{
			Path p = getHighlightedPath (s);
			if (p != null)
			{
				if  (view.isToolGraph (p.getGraph ()))
				{
					if (hasListener (p)) {
						tool = true;
					}
				}
				}
			}
		return tool;
	}
	
	private void startNavigation(EventObject e, NavigatorBase nf) {
		setState (NAVIGATING);
		navigator = nf.createNavigator (this, e);		
	}
	
	private void manageNavigation(EventObject e, NavigatorBase nf) {
		if (!UI.isConsumed(e)) {
			navigator.eventOccured (e);
			UI.consume (e);
		}
	}


	private Path getHighlightedPath (int shift)
	{
		if (highlightState == NOTHING)
		{
			return null;
		}
		else
		{
			list.getItem ((highlightIndex + shift + list.getSize ()) % list.getSize (), pickInfo);
			return pickInfo.path;
		}
	}
	
	
	private Path getToolPath ()
	{
		if (!isHMouseOnTool())
		{
			return null;
		}
		else
		{
			for (int shift=0; shift<list.getSize(); shift++) {
				list.getItem ((highlightIndex + shift + list.getSize ()) % list.getSize (), pickInfo);
				if  (view.isToolGraph (pickInfo.path.getGraph ())) {
					return pickInfo.path;
				}
			}
			return null;
		}
	}


	private void handleClick (MouseEvent event)
	{
		if (event.isAltDown ())
		{
			if ((highlightState != NOTHING) && (list.getSize () > 1))
			{
				event.consume ();
				a2[0] = highlightIndex;
				if (event.isControlDown ())
				{
					if (highlightIndex == 0)
					{
						highlightIndex = list.getSize ();
					}
					highlightIndex--;
				}
				else
				{
					highlightIndex++;
					if (highlightIndex == list.getSize ())
					{
						highlightIndex = 0;
					}
				}
				a2[1] = highlightIndex;
				highlight (list, highlightIndex, a2);
				highlightState = SELECTED;
				lastSelected.set (getHighlightedPath (0));
//				objectPicked (event);
				if (unhighlight != null)
				{
					enqueueUnhighlight ();
				}
			}
		}
	}


	protected void mouseMoved (final MouseEvent event)
	{
		Map opt = UI.getOptions (getView().getWorkbench());
		if (!Utils.getBoolean(opt, "highlightOnMove")) {
			return;
		}
		if ((highlightState != SELECTED)
			|| (Math.abs (event.getX () - pickX) > tolerance)
			|| (Math.abs (event.getY () - pickY) > tolerance))
		{
			UI.executeLockedly
				(view.getGraph (), false,
				 new Command ()
				 {
					public String getCommandName ()
					{
						return null;
					}

					public void run (Object arg, Context c)
					{
						calculateHighlight (event.getX (), event.getY ());
					}
				 }, event, view, JobManager.RENDER_FLAGS);
		}
	}


	public void updateHighlight ()
	{
		if (chX >= 0)
		{
			calculateHighlight (chX, chY);
		}
	}


	private void calculateHighlight (int x, int y)
	{
		chX = x;
		chY = y;
		view.pick (x, y, list2);
		if (list2.getSize () > 0)
		{
			if (unhighlight != null)
			{
				unhighlight.cancel ();
				unhighlight = null;
			}
			if (list2.equals (list))
			{
				list2.reset ();
			}
			else
			{
				resetHighlight ();
				PickList l = list;
				list = list2;
				list2 = l;
				pickX = x;
				pickY = y;
				highlightIndex = -1;
				for (int i = 0; i < list.getSize (); i++)
				{
					list.getItem (i, pickInfo);
					if (GraphUtils.equal (pickInfo.path, lastSelected))
					{
						highlightIndex = i;
						break;
					}
				}
				if (highlightIndex < 0)
				{
					lastSelected.clear (null);
					highlightIndex = 0;
				}
				// option to disable highlighting - almost untested
				Map opt = UI.getOptions (getView().getWorkbench());
				if (Utils.getBoolean(opt, "highlightOnMove", true)) {
					highlight (list, highlightIndex, null);
				}
				highlightState = HIGHLIGHTED;
//				objectPicked (event);
			}
		}
		else if ((highlightState != NOTHING) && (unhighlight == null))
		{
			enqueueUnhighlight ();
		}
	}


	private void enqueueUnhighlight ()
	{
		if (unhighlight != null)
		{
			unhighlight.cancel ();
		}
		jm.runLater	(highlightDelay, unhighlight = new Unhighlight (), null,
					 view);
	}


	private void resetHighlight ()
	{
		if (unhighlight != null)
		{
			unhighlight.cancel ();
			unhighlight = null;
		}
		if (highlightState != NOTHING)
		{
			highlight (list, -1, null);
			list.reset ();
			highlightState = NOTHING;
		}
	}


	private void highlight (PickList list, int index, int[] changed)
	{
		ViewSelection s = ViewSelection.get (view);
		int i = -1, j = -1, k;
		while (true)
		{
			if (changed == null)
			{
				i++;
				if (i == list.getSize ())
				{
					break;
				}
			}
			else
			{
				j++;
				if (j == changed.length)
				{
					break;
				}
				i = changed[j];
			}
			if (index >= 0)
			{
				if (i == index)
				{
					k = ViewSelection.MOUSE_OVER_SELECTED | ViewSelection.MOUSE_OVER;
				}
				else
				{
					k = ViewSelection.MOUSE_OVER;
				}
			}
			else
			{
				k = 0;
			}
			Path p = list.getPath (i);
			if (!view.isToolGraph (p.getGraph ()))
			{
				s.removeAndAdd (ViewSelection.MOUSE_OVER_SELECTED | ViewSelection.MOUSE_OVER,
								k, p);
			}
		}
	}


	protected void buttonClicked (MouseEvent e)
	{
		e.consume ();
		
		Map opt = UI.getOptions (getView().getWorkbench());
		// if the option to highlight object by mouse move is disabled - the objects are 
		// never picked. So force it when the mouse click. The highlight is computed in the 
		// same thread - otherwise the click would be resolved before the pick.
		if (!Utils.getBoolean(opt, "highlightOnMove")) {
			calculateHighlight (e.getX (), e.getY ());
		}
		Path path = null;
		boolean tool = false;
		for (int s = 0; s < list.getSize (); s++)
		{
			Path p = getHighlightedPath (s);
			if (p != null)
			{
				if  (view.isToolGraph (p.getGraph ()))
				{
					if (s == 0)
					{
						tool = true;
					}
				}
				else
				{
					path = p;
					break;
				}
			}
		}
		if ((e.getID () == MouseEvent.MOUSE_RELEASED) && 
				GraphUtils.equal (getHighlightedPath (0), lastPicked) &&
				(highlightState != NOTHING) &&
				(SwingUtilities.isLeftMouseButton (e)) &&
				Utils.getBoolean(opt, "changeToolOnClick", true)) {
			view.nextTool();
			
		}
		if (path == null) {
			lastPicked.clear(null);
		}
		else { if ((e.getID () == MouseEvent.MOUSE_RELEASED)
				&& (SwingUtilities.isLeftMouseButton (e))) {
			lastPicked.set(path);}
		}

		if (!tool && (e.getID () == MouseEvent.MOUSE_PRESSED)
			&& !(e.isAltDown () || e.isMetaDown ()))
		{
			ViewSelection s = ViewSelection.get (view);
			if (s != null)
			{
				if (path != null)
				{
					if (e.isControlDown ())
					{
						s.toggle (ViewSelection.SELECTED, path);
					}
					else
					{
						s.set (ViewSelection.SELECTED, new Path[] {path}, true);				
					}
				}
				else
				{
					s.set (ViewSelection.SELECTED, Path.PATH_0, true);
				}
			}
		}
		if (hasListener (path))
		{
			path = new ArrayPath (path);
			ClickEvent me = createClickEvent (e);
			me.set (getView (), path);
			me.set (e);
			send (me, path);
		}
	}
	
	private static void send (EventObject e, Path p)
	{
		Object last = p.getObject (-1);
		if (last instanceof EventListener)
		{
			((EventListener) last).eventOccured (e);
		}
		if (p.getGraph () instanceof EventListener)
		{
			((EventListener) p.getGraph ()).eventOccured (e);
		}
	}

	private static boolean hasListener (Path p)
	{
		if (p == null)
		{
			return false;
		}
		return (p.getObject (-1) instanceof EventListener)
			|| (p.getGraph () instanceof EventListener);
	}


	protected void mouseDragged (MouseEvent event, int dragState, int dx, int dy)
	{
//		getHighlightedPath (0) doesn't select the tools if they are on the 2nd row
//		Path p = getHighlightedPath (0);
		Path p;
		if (isHMouseOnTool()) {
			p = getToolPath();
		}
		else {p = getHighlightedPath (0);}
		if (hasListener (p))
		{
			p = new ArrayPath (p);
			DragEvent me = createDragEvent (event);
			me.set (getView (), p);
			me.set (event);
			me.setDragData (dragState, dx, dy);
			send (me, p);
		}
	}

	protected abstract ClickEvent createClickEvent (MouseEvent event);

	protected abstract DragEvent createDragEvent (MouseEvent event);
}
