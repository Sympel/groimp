package de.grogra.imp.fswatcher;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;

import de.grogra.pf.io.IO;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Window;
import de.grogra.imp.IMPJobManager;
import de.grogra.imp.io.LocalFileManager;

public class WatchEventResolver {
	
	public static void resolveEvent(WatchEvent<Path> ev, Path child, Context ctx) throws IOException {
		Registry r = ctx.getWorkbench().getRegistry();
		if ( StandardWatchEventKinds.ENTRY_CREATE == ev.kind() ) {
			if (child.toFile().isDirectory()) {
				UI.executeLockedly(
				ctx.getWorkbench().getRegistry().getProjectGraph (),
					 true, loadDirectoryCommand, child, 
					 ctx, JobManager.ACTION_FLAGS);
			}
			else if (LocalFileManager.shouldBeLoaded(child, ctx)) {
				FileChannel channel = new RandomAccessFile(child.toFile(), "rw").getChannel();
				try (channel){
					channel.lock();
				UI.executeLockedly(
				ctx.getWorkbench().getRegistry().getProjectGraph (),
					 true, loadFileCommand, child.toFile(), 
					 ctx, JobManager.ACTION_FLAGS);
				}
			}				
        }
		
		if ( StandardWatchEventKinds.ENTRY_DELETE == ev.kind() ) {
			if (child.toFile().isDirectory()) {
				UI.executeLockedly(
				ctx.getWorkbench().getRegistry().getProjectGraph (),
					 true, deleteDirectoryCommand, child, 
					 ctx, JobManager.ACTION_FLAGS);
			}
			// check if the file has already been deleted
			else {
				UI.executeLockedly(
				ctx.getWorkbench().getRegistry().getProjectGraph (),
					 true, deleteItemCommand, child.toFile(), 
					 ctx, JobManager.ACTION_FLAGS);
			}
		}
		
//		if ( StandardWatchEventKinds.ENTRY_MODIFY == ev.kind() ) {
//			
//		}
	}
	
	
	
	static Command loadFileCommand = new Command ()
	{
		@Override
		public void run (Object info, Context ctx)
		{
			try {
				LocalFileManager.loadFile((File) info, ctx);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public String getCommandName ()
		{
			return null;
		}
	};

	
	static Command loadDirectoryCommand = new Command ()
	{
		@Override
		public void run (Object info, Context ctx)
		{
			try {
				LocalFileManager.loadFromDirectory(info.toString(), ctx);
			} catch (IOException e) {
				e.printStackTrace();
			}		}

		@Override
		public String getCommandName ()
		{
			return null;
		}
	};

	
	
	static Command deleteItemCommand = new Command ()
	{
		@Override
		public void run (Object info, Context ctx)
		{
			LocalFileManager.deleteItem((File) info, ctx);
		}

		@Override
		public String getCommandName ()
		{
			return null;
		}
	};

	
	static Command deleteDirectoryCommand = new Command ()
	{
		@Override
		public void run (Object info, Context ctx)
		{
			LocalFileManager.deleteDirectory(info.toString(), ctx);
		}

		@Override
		public String getCommandName ()
		{
			return null;
		}
	};


}
