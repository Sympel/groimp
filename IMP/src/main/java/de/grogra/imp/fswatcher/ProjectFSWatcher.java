package de.grogra.imp.fswatcher;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import javax.swing.tree.TreePath;

import de.grogra.imp.IMP;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.Context;
import de.grogra.vfs.FileSystem;

/**
 * The project filesystem watcher is a event listner that links the project fs 
 * open by a Groimp project and the fs on the device. When the watcher is active
 * the CREATE, and DELETE events are watched and will trigger when they happen 
 * on the device. (example you delete a project file outside of groimp while the 
 * project is open in groimp). 	
 * It uses {@code WatcherService} to register and manage the events, and {@code WatchEventResolver}
 * to resolve the events.
 * 
 * @see     de.grogra.imp.fswatcher.WatcherService
 * @see     de.grogra.imp.fswatcher.WatchEventResolver
 */
public class ProjectFSWatcher extends Thread implements RegistryContext {
	
	private boolean watch = true;
    private WatcherService watcherService;
    private final Context context;

    public ProjectFSWatcher(String searchingPath, Context ctx) {
        this.context = ctx;
		try {
			watcherService = new WatcherService(Paths.get(searchingPath), ctx);
		} catch (IOException e) {
			System.err.println (IMP.I18N.msg("filewatcher.start.error.msg"));
		}
    }

    @Override
    public void run() {
    	Registry.setCurrent (getRegistry());
        while(watch) {
            try {
				if (!watcherService.watch()) {
				    break;
				}
			} catch (IOException | InterruptedException e) {
				System.err.println (IMP.I18N.msg("filewatcher.running.error.msg", e));
			}
        }
    }

    public void stopThread() {
    	try {
			watcherService.close();
			watch = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

	@Override
	public Registry getRegistry() {
		return context.getWorkbench().getRegistry();
	}

}
