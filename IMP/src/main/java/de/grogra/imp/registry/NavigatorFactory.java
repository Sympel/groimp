
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.imp.registry;

import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.EventObject;

import javax.swing.SwingUtilities;

import de.grogra.graph.impl.Node.NType;
import de.grogra.imp.NavigatorBase;
import de.grogra.imp.View;
import de.grogra.imp.ViewComponent;
import de.grogra.imp.ViewEventHandler;
import de.grogra.imp.edit.Tool;
import de.grogra.imp.edit.ToolRoot;
import de.grogra.persistence.ShareableBase;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.ChoiceGroup;
import de.grogra.pf.ui.registry.UIItem;
import de.grogra.pf.ui.tree.UINodeHandler;
import de.grogra.util.DisposableEventListener;
import de.grogra.util.WrapException;

/**
 * The <code>NavigatorFactory</code> is the factory for the <code>Navigator3D</code>.
 * It defines the activation events that trigger the navigation mode.
 * The activation events are: 
 * <ul>
 *   <li>Right mouse button pressed.</li>
 *   <li>Wheel used.</li>
 *   <li>Middle button pressed.</li>
 *   <li>Left mouse button dragged over empty space.</li>
 * </ul>
 */
public class NavigatorFactory extends Item implements UIItem
{
	private String cls;

//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new NavigatorFactory ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new NavigatorFactory ();
	}

//enh:end
	
	
	public NavigatorFactory ()
	{
		super (null);
	}

	public NavigatorBase createNavigatorBase (Context ctx)
	{
		try
	{
		NavigatorBase c = (NavigatorBase) classForName (cls, true).getDeclaredConstructor().newInstance();
		return c;
	}
	catch (ClassNotFoundException e)
	{
		throw new WrapException (e);
	}
	catch (IllegalAccessException e)
	{
		throw new WrapException (e);
	}
	catch (InstantiationException e)
	{
		throw new WrapException (e);
	} catch (IllegalArgumentException e) {
		throw new WrapException (e);
	} catch (InvocationTargetException e) {
		throw new WrapException (e);
	} catch (NoSuchMethodException e) {
		throw new WrapException (e);
	} catch (SecurityException e) {
		throw new WrapException (e);
	}
	}

		
	public int getUINodeType ()
	{
		return UINodeHandler.NT_ITEM;
	}


	public Object invoke (Context ctx, String method, Object arg)
	{
		return null;
	}


	public boolean isAvailable (Context ctx)
	{
		return true;
	}


	public boolean isEnabled (Context ctx)
	{
		return true;
	}
	
	@Override
	protected boolean readAttribute (String uri, String name, String value)
		throws org.xml.sax.SAXException
	{
		if ("".equals (uri))
		{
			if ("class".equals (name))
			{
				cls = value;
				return true;
			}
		}
		return super.readAttribute (uri, name, value);
	}

	public static NavigatorFactory get (View view, String itemPath)
	{
		Item g = Item.resolveItem (view.getWorkbench (), itemPath);
		Item i = (g instanceof ChoiceGroup) ? ((ChoiceGroup) g).getPropertyValue (view)
			: null;
		if ((i instanceof NavigatorFactory)
			&& ((NavigatorFactory) i).isAvailable (view))
		{
			return (NavigatorFactory) i;
		}
		else
		{
			for (i = (Item) g.getBranch (); i != null; i = (Item) i.getSuccessor ())
			{ 
				if ((i instanceof NavigatorFactory)
					&& ((NavigatorFactory) i).isAvailable (view))
				{
					return (NavigatorFactory) i;
				}
			}
		}
		return null;
	}
	
}
