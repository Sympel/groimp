package de.grogra.pf.ui;

import de.grogra.pf.registry.Registry;

public interface ProjectContext {

	public Project getProject();
	public Registry getProjectRegistry();
}
