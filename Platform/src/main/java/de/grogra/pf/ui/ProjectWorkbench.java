package de.grogra.pf.ui;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.persistence.LogStore;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.registry.CommandItem;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.util.Map;
import de.grogra.util.MimeType;
import de.grogra.util.WrapException;

public abstract class ProjectWorkbench extends Workbench implements ProjectContext{
	protected Project project;
	public ProjectWorkbench(Registry registry, JobManager jm, UIToolkit ui, Map initParams) {
		super(registry, jm, ui, initParams);
	}
	public void setProject(Project project) {
		this.project=project;
		updateName();
	}
	
	@Override
	public Project getProject() {
		return project;
	}
	
	@Override
	public Registry getProjectRegistry() {
		return project.getRegistry();
	}
	
	
	public static void getProjectInfo(Item item, Object info, Context ctx) {
		((ProjectWorkbench)ctx.getWorkbench()).getProjectInfo(info);		
	}
	
	public abstract void getProjectInfo(Object info);
	
	public static void export(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).export(info);

	}

	public abstract void export(Object info) throws Exception;

	@Override
	public boolean save (boolean saveAs) {
		if(project.save()) {
			return true;
		}else if(saveAs) {
			return saveAs(null);
		}else {
			return false;
		}
		
	}
	public abstract boolean saveAs(Object info);
	public boolean saveAs(File file, MimeType mimeType) {
		return project.saveAs(file,mimeType);
	}
	
	
	public static void close(Item item, Object info, Context ctx) {
		ctx.getWorkbench().close(null);
	}
	
	public void loadProjectFile(FilterSource fs, Map initParams) {
			project.loadProjectFile(fs, initParams,this);
	}
	
	public static void addNode(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).addNode(info);
	}
	
	public abstract void addNode(Object info) throws Exception;
	
	public void addNode(Node node) {
		project.addNode(node,this);
	}
		
	public static void addSourceFile(Item item, Object info, Context ctx) throws Exception {
		Workbench.setCurrent(ctx.getWorkbench());
		Registry.setCurrent(ctx.getWorkbench().getProjectRegistry());
		((ProjectWorkbench)ctx.getWorkbench()).addSourceFile(info);
	}
	
	public abstract void addSourceFile(Object info) throws Exception;
	

	public SourceFile addSourceFile(String fileName, MimeType mt, InputStream ins, Object dest){
		return project.addSourceFile(fileName, mt, ins,dest);
	}
	
	public SourceFile addSourceFile (File file, MimeType mt) {
		return project.addSourceFile(file, mt);
	}

	public SourceFile addSourceFile (File file, MimeType mt, Object dest) {
		
		return project.addSourceFile(file, mt,dest);
	}
	
	public static void renameSourceFile(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).renameSourceFile( info);
	}
	public abstract void renameSourceFile(Object info) throws Exception;
	
	public void renameSourceFile(String file, String newName) {
		SourceFile sf = getFileNode(file);
		if(sf!=null) {
			project.renameFile(sf, newName, this);
		}
	}
	
	public static void removeSourceFile(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).removeSourceFile( info);
		
	}
	public abstract void removeSourceFile(Object info) throws Exception;
	
	public void removeSourceFile(String name) {
		SourceFile sf =getFileNode(name);
		if(sf!=null) {
			project.removeFile(sf,this);
		}
	}
	
	public static void listFunctions(Item item, Object info, Context ctx) {
		((ProjectWorkbench) ctx.getWorkbench()).listFunctions(info);
	}
	
	public abstract void listFunctions(Object info);
	
	public Command[] getFunctions() {
		return project.listFunctions();
	}
	
	
	
	public static void compile(Item item, Object info, Context ctx) {
		((ProjectWorkbench)ctx.getWorkbench()).compile(info);
	}

	public void compile(Object info) {
		project.compile(this, null);
	}

	public void compile() {
		project.compile(this, null);
	}
	
	public static void execute(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).execute(info);
	}
	
	public abstract void execute(Object info) throws Exception;
	
	public void execute(String commandName) {
		project.execute(commandName, this);
	}
	
	public void execute(CommandItem command) {
		project.execute(command, this);
	}
		
	@Override
	public Workbench open(FilterSource fs, Map initParams) {
		return null;
	}
	
	
	
	protected SourceFile getFileNode(String name) {
		String systemId =IO.toSystemId(getProjectRegistry().getFileSystem(), name);
		Item node = Item.findFirst (getProjectRegistry(), "/project/objects",
	    		new ItemCriterion ()
    		{
    			@Override
    			public boolean isFulfilled (Item item, Object info)
    			{
    				return item.getSystemId()!=null && item.getSystemId().equals(systemId);
    			}
				@Override
				public String getRootDirectory() {return null;}
    		}, systemId, false);
		return (SourceFile)node;
	}
	
	// just some placeholder to overwrite the GUI orientated functions in workbench
	
	public void open(Object info) {
		
	}
	
	public void openRecent(Object info) {
		
	}
	
//	public void openAsDemo(Object info) {
//		
//	}
	public void delete(Object info) {
		
	}
	
	
	public abstract Object getToken();
	public abstract void setToken(Object token);
	public abstract String getApplicationName();
	public abstract void dispose(Command afterDispose);
	
	
}
