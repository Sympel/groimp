package de.grogra.pf.ui.registry;

import java.io.IOException;

import de.grogra.pf.io.FilterSource;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Project;
import de.grogra.pf.ui.ProjectManager;
import de.grogra.util.Map;

public interface ProjectFactory {
	public Project createProject(Registry registry, ProjectManager pm) throws IOException;
	public Project loadProject(FilterSource fs, Map initParams,Registry registry, ProjectManager pm) throws IOException;
}