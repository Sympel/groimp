
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.ui.registry;

import de.grogra.pf.io.IO;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.projectexplorer.ProjectDescription;
import de.grogra.util.ComparableVersion;

import javax.swing.event.TreeModelEvent;
import java.lang.module.ModuleDescriptor;
import java.util.ArrayList;

public class ProjectDirectory extends Item implements ProjectDescription
{

	// represent the latest version of the project
	final String LATEST = "latest";
	ArrayList<ComparableVersion> versions = new ArrayList<>();

//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new ProjectDirectory());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new ProjectDirectory();
	}

//enh:end


	private ProjectDirectory()
	{
		this (null, null);
	}


	public ProjectDirectory(String key, String systemId)
	{
		super (key);
		this.systemId=systemId;
		setDirectory ();
	}


	public String getBaseName ()
	{
		String s = (String) getDescription ("Base");
		if (s == null)
		{
			s = getName ();
			if (s.endsWith ("s"))
			{
				s = s.substring (0, s.length () - 1);
			}
		}
		return s;
	}
	
	
	public static ProjectDirectory get (Item child)
	{
		while ((child != null) && !(child instanceof ProjectDirectory))
		{
			child = (Item) child.getAxisParent ();
		}
		return (ProjectDirectory) child;
	}
	
	@Override
	protected Object getDescriptionImpl (String type)
	{
		if (NAME.equals (type))
		{
			return IO.toName (getName ());
		}
		return super.getDescriptionImpl (type);
	}

	@Override
	public void addRequiredFiles (java.util.Collection list)
	{
		Object f = getRegistry ().getProjectFile ((String)getSystemId ());
		if (f != null)
		{
			list.add (f);
		}
	}
	
	@Override
	public void delete() {
		//shouldn't be deletable
	}
	
	@Override
	protected void activateImpl ()
	{
		// for backward compatibility
		IO.setSystemIdFromName(this);
		getRegistry ().getProjectFile ((String)getSystemId ());
	}

	@Override
	protected void deactivateImpl ()
	{
		
	}
	
	public void treeNodesInserted (TreeModelEvent e)
	{
		// add children here
	}
	
	public void treeNodesRemoved (TreeModelEvent e)
	{
		// remove children here
	}

	public void treeNodesChanged (TreeModelEvent e)
	{
	}

	public void treeStructureChanged (TreeModelEvent e)
	{
		//save the project when structure changed or directory added
	}

	// Getter methods on description to enable RemoteProjectDirectory to
	// Override them with web queries instead

	/**
	 *
	 * @return an ImageAdapter of the image loaded by IMP (should even be a FixedImageAdapter)
	 */
	public Object getImage(){
		return getDescription(ProjectDescription.IMAGE);
	}

	/**
	 *
	 * @return An array of authors String[]
	 */
	public String[] getAuthors(){
		String[] r=null;
		String o = (String) getDescription(ProjectDescription.AUTHORS);
		if (o!=null)
			 r = o.split(",");
		return r;
	}

	/**
	 *
	 * @return An array of tags
	 */
	public String[] getTags(){
		String[] r=null;
		String o = (String) getDescription(ProjectDescription.TAGS);
		if (o!=null)
			r = o.split(",");
		return r;
	}

	/**
	 *
	 * @return a category as String
	 */
	public Object getCategory(){
		return getDescription(ProjectDescription.CATEGORY);
	}

	/**
	 *
	 * @return the short description as String
	 */
	public Object getShorDescription(){
		return getDescription(ProjectDescription.SHORT_DESCRIPTION);
	}

	/**
	 *
	 * @return the project name as String
	 */
	public Object getProjectName(){
		return getDescription(ProjectDescription.PROJECT_NAME);
	}

	/**
	 * An ArrayList<ComparableVersion> containing the versions
	 */
	public ArrayList<ComparableVersion> getVersions(){
		if (!versions.isEmpty()){
			return versions;
		}
		Item[] versionsItem = findAll(this, ItemCriterion.INSTANCE_OF,
				de.grogra.pf.registry.Directory.class, true);
		for (Item i : versionsItem){
			versions.add(new ComparableVersion(i.getName()));
		}
		return versions;
	}


	public Item getProjectForVersion(String version){
		String toUse;
		if (version.equals(LATEST)){
			toUse = getLatestVersion().toString();
		}
		else {
			toUse = version;
		}
		Item i = Item.resolveItem(Workbench.current(), getAbsoluteName()+'/'+toUse);
		if (i!=null){
			return findFirst(i, ItemCriterion.INSTANCE_OF, FilterSourceFactory.class, true);
		}
		return null;
	}

	public ComparableVersion getLatestVersion(){
		ArrayList<ComparableVersion> allVersions = getVersions();
		if (!allVersions.isEmpty()){
			ComparableVersion latest = allVersions.get(0);
			for(ComparableVersion v : allVersions){
				if (latest.compareTo(v) < 0) {
					latest=v;
				}
			}
			return latest;
		}
		return null;
	}

}
