package de.grogra.pf.ui;

import java.io.FileNotFoundException;
import java.io.IOException;

import de.grogra.pf.io.FilterSource;
import de.grogra.pf.registry.Application;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Plugin;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.registry.CommandItem;
import de.grogra.pf.ui.registry.FilterSourceFactory;
import de.grogra.pf.ui.registry.ProjectFactory;
import de.grogra.util.Map;
import de.grogra.util.StringMap;

public abstract class UIApplication extends Plugin implements Command{
//	private ArrayList<Workbench> workbenches;
	protected ProjectManager pm;
	protected UIToolkit uitoolkit;
	protected WorkbenchManager wbm;
	public ProjectWorkbench selected;
	private ProjectWorkbench mainWorkbench;

	public void init(ProjectManager pm, UIToolkit uitoolkit,WorkbenchManager wbm) {
		this.pm = pm;
		this.uitoolkit=uitoolkit;
		this.wbm=wbm;
	}
	
	public static void run (Application app){
	}

	public void run (Object arg, Context context) {
	}

	public UIToolkit getUiToolKit() {
		return uitoolkit;
	}
	
	public ProjectManager getProjectManager() {
		return pm;
	}
	
	public WorkbenchManager getWorkbenchManager() {
		return wbm;
	}
	
	public  ProjectWorkbench getMainWorkbench() {
		return mainWorkbench;
	}
	protected void setMainWorkbench(ProjectWorkbench wb) {
		mainWorkbench=wb;
	}
	/**
	 * Get the last selected workbench by the application
	 * @return The current workbench for this application
	 */
	public abstract Workbench getCurrentWorkbench();
	
	/**
	 * Create a new workbench and load an project into it. 
	 * @throws Exception 
	 */
	public static void open(Item item, Object info, Context ctx) throws Exception {
		ctx.getWorkbench().getApplication().open(info);
	}
	public abstract ProjectWorkbench open(Object info) throws Exception;
	public abstract ProjectWorkbench open(FilterSource fs, Map initParams);

	
	public ProjectWorkbench open(FilterSource fs, ProjectFactory pf,Map initParams) throws IOException {
		 ProjectWorkbench wb = wbm.createWorkbench(pf, fs, initParams);
		 selectWorkbench(wb);
		 return wb;
	}
	
	/**
	 * Not possilbe yet due to lock issues
	 */
	
//	public static void openProject(Item item, Object info, Context ctx) {
//		ctx.getWorkbench().getApplication().openProject(info);
//	}
//	public abstract void openProject(Object info);
//	
	
	/**
	 * Close a workbench and deregister (disconnect) its project.
	 */
	public static void close(Item item, Object info, Context ctx) {
		ctx.getWorkbench().getApplication().close(info);
	}
	public abstract void close(Object info);
	
	public void close(Context ctx) throws IOException {
//		 pm.close(this, ctx);
//		TODO:create close in workbench manager
	}
	
	/**
	 * Create a new workbench with a new project.
	 * @throws IOException 
	 */
	public static void createWB(Item item, Object info, Context ctx) throws Exception {
		ctx.getWorkbench().getApplication().create(info);
	}
	
	public static void create(Item item, Object info, Context ctx) throws Exception {
		ctx.getWorkbench().getApplication().create(info);
	}
	public abstract void create(Object info) throws IOException, Exception;
	
	//creates a new empty model from the template stored in the RGG plugin
	public Object create(String name,String newName) throws IOException {
		ProjectWorkbench wb = loadWBFromRegistry("/ui/templates/",name,newName);
		if(wb!=null) {
			selectWorkbench(wb);
			return wbm.getWorkbenchId(wb);
		}
		return null;
	}
	
	public static void loadExample(Item item, Object info, Context ctx) throws Exception {
		ctx.getWorkbench().getApplication().loadExample(info);
	}
	public abstract void loadExample(Object info) throws Exception;

	public Object loadExample(String name,String newName) {
		ProjectWorkbench wb = loadWBFromRegistry("/examples/",name,newName);
		if(wb!=null) {
			selectWorkbench(wb);
			return wbm.getWorkbenchId(wb);
		}
		return null;
	}

	private ProjectWorkbench loadWBFromRegistry(String dir, String name,String newName) {
		FilterSourceFactory ex = (FilterSourceFactory) Item.resolveItem(getMainWorkbench().getProject(), dir+name);
		if(ex==null) {
			return null;
		}
		StringMap m = new StringMap();
		Object x = ex.evaluate(getMainWorkbench().getProjectRegistry(),m);
		if(x!=null ) {
			ProjectWorkbench wb = open((FilterSource)x,new StringMap());
			if(wb!=null) {
				wb.getProject().setFile(null, null);
				wb.getProject().setName(newName!=null?newName:ex.getName());
				return wb;
			}
		}
		return null;
	}	
		
	
	public static void listExamples(Item item, Object info, Context ctx) {
		ctx.getWorkbench().getApplication().listExamples(info);
		
	}
	public abstract void listExamples(Object info);

	public static void listTemplates(Item item, Object info, Context ctx) {
		ctx.getWorkbench().getApplication().listTemplates(info);
		
	}

	
	public abstract void listTemplates(Object info);

	
	public static void selectWorkbench(Item item, Object info, Context ctx) {
		ctx.getWorkbench().getApplication().selectWorkbench(info);
	}
	protected abstract void selectWorkbench(Object info);
	
	public void selectWorkbench(ProjectWorkbench w) throws NullPointerException {
		if (w == null) {throw new NullPointerException();}
		this.selected=w;
	}

	public static void listWorkbenches(Item item, Object info, Context ctx) {
		ctx.getWorkbench().getApplication().listWorkbenches(info);
	}
	public abstract void listWorkbenches(Object info);
	
	public static void listProjects(Item item, Object info, Context ctx) {
		ctx.getWorkbench().getApplication().listProjects(info);
	}
	public abstract void listProjects(Object info);

	
	
	
	
	/**
	 * Terminate the application. It closes all opened workbenches managed by its wbm.
	 * Close workbenches do not necessarily close the projects.
	 */
	public static void stop(Item item, Object info, Context ctx) {
		ctx.getWorkbench().getApplication().stop();
	}

	public abstract void stop();

	public abstract void exit();


}
