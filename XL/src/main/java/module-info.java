module xl {
	exports de.grogra.xl.ode;
	exports de.grogra.xl.query;
	exports de.grogra.xl.modules;
	exports de.grogra.xl.property;

	requires xl.core;
	requires java.desktop;
	
}