<chapter id="c-types">
<title>Types</title>

<para>

The Java programming language defines types and divides them
into two categories,
the <firstterm>primitive types</firstterm> <classname>boolean</classname>,
<classname>byte</classname>, <classname>short</classname>,
<classname>char</classname>, <classname>int</classname>,
<classname>long</classname>, <classname>float</classname>,
<classname>double</classname>, and the <firstterm>reference types</firstterm>,
which are class types, interface types, array types, and the null type.
The types of the XL programming language are those types of the Java
programming language, plus <firstterm>intersection types</firstterm>.
</para>
<productionset><title>Types</title>
<production id="ebnf.type">
  <lhs>Type</lhs>
  <rhs><nonterminal def="#ebnf.reftype">ReferenceType</nonterminal> |
    <nonterminal def="#ebnf.primtype">PrimitiveType</nonterminal>
  </rhs>
</production>
<production id="ebnf.primtype">
  <lhs>PrimitiveType</lhs>
  <rhs><literal>boolean</literal> |
       <literal>byte</literal> |
       <literal>short</literal> |
       <literal>char</literal> |
       <literal>int</literal> |
       <literal>long</literal> |
       <literal>float</literal> |
       <literal>double</literal>
  </rhs>
</production>
<production id="ebnf.reftype">
  <lhs>ReferenceType</lhs>
  <rhs><nonterminal def="#ebnf.name">Name</nonterminal> |
       <nonterminal def="#ebnf.arraytype">ArrayType</nonterminal>
  </rhs>
</production>
<production id="ebnf.arraytype">
  <lhs>ArrayType</lhs>
  <rhs><nonterminal def="#ebnf.type">Type</nonterminal> '[' ']'
  </rhs>
</production>
</productionset>

<section id="s-typeaffix"><title>Type Affixes and Type Letters</title>

<para>
This specification sometimes refers to <firstterm>type affixes</firstterm>.
They are used to form names with a common prefix or suffix
for different types. The type affix of a type <replaceable>T</replaceable>
is defined as follows:
</para>
<itemizedlist>
<listitem><para>If <replaceable>T</replaceable> is a primitive type,
then the type affix is the name of that type, but having the first
letter capitalized. E.g, the type affix of the type
<classname>int</classname> is <methodname>Int</methodname>.
</para></listitem>
<listitem><para>Otherwise, <replaceable>T</replaceable> is a reference type.
Then its type affix is <methodname>Object</methodname>.
</para></listitem>
</itemizedlist>

<para>
In addition, for each type a <firstterm>type letter</firstterm> is defined.
The type letter is
</para>
<itemizedlist>
<listitem><para>
<literal>a</literal> for reference types,
</para></listitem>
<listitem><para>
<literal>d</literal> for <classname>double</classname>,
</para></listitem>
<listitem><para>
<literal>f</literal> for <classname>float</classname>,
</para></listitem>
<listitem><para>
<literal>l</literal> for <classname>long</classname>,
</para></listitem>
<listitem><para>
<literal>i</literal> for the other types.
</para></listitem>
</itemizedlist>


</section>

<section id="s-intersection"><title>Intersection Types</title>

<para>
<firstterm>Intersection types</firstterm> are types that
arise in queries (<xref linkend="c-queries"/>); they cannot be written
directly as part of a programme. Intersection types have also been
defined in the Java Language Specification, Third Edition in a similar way.
</para>
<para>
An intersection type takes the form
<replaceable>T &amp; I<subscript>1</subscript> &amp; ...
&amp; I<subscript>n</subscript></replaceable>, where
<replaceable>T</replaceable> is a class type, array type, or primitive type,
and the
<replaceable>I<subscript>k</subscript></replaceable> are interface types.
An intersection type represents the intersection of its constituent
types. Thus, its values are those values which are values of all
of the types <replaceable>T</replaceable> and
<replaceable>I<subscript>k</subscript></replaceable>. It is a
compile-time error if an empty intersection type arises, i.e.,
an intersection type which cannot have any value.
</para>
<para>
For a class type <replaceable>T</replaceable>, an intersection type
<replaceable>T &amp; I<subscript>1</subscript> &amp; ...
&amp; I<subscript>n</subscript></replaceable> has the same members
as a class type with an empty body, direct superclass
<replaceable>T</replaceable> and direct superinterfaces
<replaceable>I<subscript>k</subscript></replaceable>, declared
in the same package in which the intersection type appears.
</para>

</section>

</chapter>
