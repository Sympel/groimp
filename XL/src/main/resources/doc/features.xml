<chapter id="c-features">
<title>New Features of the XL Programming Language</title>

<para>
In this chapter, some of the new features of the XL programming
language will be shown. Most examples are to be understood as
excerpts of XL programmes, they are no stand-alone programmes.
Some of the examples are only valid within the
scope of a specific data model (<xref linkend="c-datamodel"/>),
namely the RGG data model which is contained in the RGG plugin
of the modelling software GroIMP, see
<ulink url="http://www.grogra.de/">www.grogra.de</ulink>.
In any case, in order to test the examples, it is easiest to use
GroIMP which provides an integrated compiler for the XL programming
language and a text editor with syntax highlighting. The GroIMP
distribution contains a set of demo projects which cover the examples
given here (PENDING).
</para>

<section><title>Relational Extensions</title>

<para>
This section gives examples for the
relational extensions. These are concerned with
relational data defined by the data model (<xref linkend="c-datamodel"/>):
<firstterm>Rules</firstterm>, enclosed in
<firstterm>transformation blocks</firstterm>, transform the data by
searching for their left hand side patterns
(<firstterm>composite predicates</firstterm>)
and replacing matches by their right hand side structures.
<firstterm>Queries</firstterm> are the building blocks for the left hand
side of rules, but can also be used outside of rules in order to query
the relational data source for occurrences of a pattern.
<firstterm>Quasi-parallel assignments</firstterm> perform modifications
of properties of the relational data source as if they were executed in
parallel. In conjunction with the application of rules in parallel, this
is an important feature for the modelling of processes running in parallel
as they occur, e.g., in nature.
</para>

<section><title>Rules</title>

<para>
The possibility of specifying <firstterm>rules</firstterm> is a main
feature of the XL programming language; it qualifies the XL programming
language as a language with support for rule-based programming.
This support will exemplified by means of the
famous snowflake curve (three Koch curves).
Its construction is as follows: Starting with
an equilateral triangle (consisting of three lines),
replace a line by four lines with certain angles inbetween,
see <xref linkend="f-koch"/> and <xref linkend="f-snowflake"/>.
</para>
<figure id="f-koch"><title>Koch's construction step</title>
<graphic format="PNG" fileref="res/koch.png"/></figure>
<figure id="f-snowflake"><title>Sequence of structures</title>
<graphic format="PNG" fileref="res/snowflake.png"/></figure>
<para>
In the sense of the rule-based paradigm, Koch's construction step
is a rule. Roughly speaking, such a rule has a pattern on its left hand side
and a replacement instruction on its right hand side. Thorough
definitions and analyses of special kinds of rules have been made
in the context of grammars; here, the reader is only referred to
a special kind of grammars,
namely the L-system formalism <xref linkend="bib-lsystem"/>.
</para>
<para>
The L-system formalism, combined with turtle geometry,
defines a symbolic notation that can be used, e.g., for the
specification of the snowflake example in a computer-readable form.
Namely, let the symbol <classname>F</classname> represent a line segment
and <classname>RU(a)</classname> a rotation of <literal>a</literal>
degrees, then the initial triangle can be specified as a sequence of
(parametrised) symbols:
<programlisting>
F RU(120) F RU(120) F
</programlisting>
This has to be interpreted in the context of turtle geometry, i.e.,
as a sequence of instructions to a drawing device which reads
and performs instructions from left to right.
</para>
<para>
Now Koch's construction step can be specified as an L-system rule:
<programlisting>
F ==> F RU(-60) F RU(120) F RU(-60) F
</programlisting>
In other words, replace every occurence of the symbol <classname>F</classname>
in the instruction sequence by a subsequence consisting of some symbols
<classname>F</classname> and <classname>RU</classname>.
</para>
<para>
The XL programming language allows for an easy specification of
rules of this and other kinds. In the context of GroIMP and the RGG data
model as described above, the snowflake is programmed as follows:
<programlisting>
[
  Axiom ==> F(1) RU(120) F(1) RU(120) F(1);
  F(x) ==> F(x/3) RU(-60) F(x/3) RU(120) F(x/3) RU(-60) F(x/3);
]
</programlisting>
This example makes use of the following features of the XL
programming language:
<itemizedlist>
<listitem><para>
Rules are indicated by arrows (<literal>==></literal> in this case)
and grouped within transformation blocks <literal>[ ... ]</literal>.
Transformation blocks are statements of the XL programming language
and can be used like any other statement.
</para></listitem>
<listitem><para>
The pattern of the left hand side may contain class names
(<classname>Axiom</classname>) and <firstterm>predicates</firstterm>
(<classname>F(x)</classname>, a class name
plus a parameter <methodname>x</methodname>). The parameter
<methodname>x</methodname> stands for
the length of the line segment, it will be set to the length of the
currently found segment of class <classname>F</classname>.
</para></listitem>
<listitem><para>
The right hand side may contain a sequence of node expressions.
In the example, all expressions are just constructor invocations,
but without the keyword <literal>new</literal>. Thus, when the first
rule is applied, new instances of the classes <classname>F</classname>
and <classname>RU</classname> are created as if the expressions
<literal>new F(1)</literal> and <literal>new RU(120)</literal> were
evaluated.
</para></listitem>
<listitem><para>
The structure actually is a graph of nodes.
<classname>Axiom</classname>, <classname>F</classname>,
<classname>RU</classname> are node classes, i.e., instances of these
classes may be used as nodes in the graph. Initially, the graph
contains a single node of class <classname>Axiom</classname>, which
corresponds to the start word of L-systems. Subsequently, the application
of the two rules to the graph replaces nodes by sequences of nodes.
</para></listitem>
<listitem><para>
The nodes of two textually consecutive node expressions are
connected by an edge in the graph. Thus, the created nodes of classes
<classname>F</classname> and <classname>RU</classname> are connected
linearly, forming a sequence similar to the sequence of symbols
of L-systems. However, note that the structure could be a true graph
of objects which may contain branchings and even cycles, whereas
the L-system structure is just a linear sequence of plain symbols.
</para></listitem>
<listitem><para>
The rule arrow <literal>==></literal> indicates a rule with implicit
<firstterm>embedding</firstterm>. In this simple example,
this means that the first node
of the right hand side receives the incoming edges of the matching node
of the left hand side, and the last node of the right hand side
receives the outgoing edges of the matching node. This effectively
removes the matching node from the graph and inserts the sequence of
new nodes at the location of the removed node.
</para></listitem>
</itemizedlist>
</para>
<para>
The example so far is no complete programme of the XL programming
language, it could be completed as follows:
<programlisting>
import de.grogra.rgg.*;
import de.grogra.lsystem.*;

public class Koch extends RGG {
  public void derivation() [
    Axiom ==> F(1) RU(120) F(1) RU(120) F(1);
    F(x) ==> F(x/3) RU(-60) F(x/3) RU(120) F(x/3) RU(-60) F(x/3);
  ]
}
</programlisting>
</para>
<para>
The XL programming language defines other kinds of rules. The rule
arrow <literal>==>></literal> indicates a rule without implicit
embedding, the rule arrow <literal>::></literal> indicates an
<firstterm>execution rule</firstterm> which executes the
statements of its right hand side for every match of the left hand side.
</para>

</section>

<section id="s-f-queries"><title>Queries</title>

<para>
Rules are used to create and transform graphs. <firstterm>Queries</firstterm>
are used to query existing graphs for specific features. Their syntax is
exactly the same as for the left hand side of a rule, which can be seen
as a query for subgraphs that are to be replaced by the right hand side.
A query expression is a generator expression
(<xref linkend="s-f-gen"/>),
it yields all possible matches in succession. As an example, consider the
query expression
<programlisting>
(* F *)
</programlisting>
The asterisked parentheses indicate a query expression, the contained
identifier <classname>F</classname> specifies that the query yields
all nodes of class <classname>F</classname>. Using the aggregate method
(<xref linkend="s-f-gen"/>)
<methodname>count</methodname> declared in
<classname>de.grogra.xl.lang.Operators</classname>, the query could be
used to determine the number of instances of <classname>F</classname>
in the graph:
<programlisting>
count((* F *))
</programlisting>
With the help of the aggregate method <methodname>sum</methodname>,
the total length of all instances of <classname>F</classname>
can be computed:
<programlisting>
sum((* F *).length)
</programlisting>
As a more complex example, consider the following:
<programlisting>
sum((* d:Individual, ((d != c) &amp;&amp; (distance(c, d) &lt; 2)),
       d ( -(branch|successor)-> )* f:F, (isRelevant(f))
     *).length);
</programlisting>
Suppose a specific node <methodname>c</methodname> is given, then
this expression
computes the length of all nodes <methodname>f</methodname> of class
<classname>F</classname> that are relevant according to some method
<methodname>isRelevant</methodname> and that are descendants
(with respect to the edges <methodname>branch</methodname> or
<methodname>successor</methodname>) of a node
<methodname>d</methodname> of class <classname>Individual</classname>
which lies within a distance of 2 from <methodname>c</methodname>.
</para>
<para>
This example demonstrates some features of queries:
<itemizedlist>
<listitem><para>
Query components can be labelled by local identifiers called
<firstterm>term variables</firstterm>, in this case
<methodname>d</methodname> and <methodname>f</methodname>. During the
process of matching, these
variables are bound to the matches for the query components and can be
used in the remaining part of the query.
</para></listitem>
<listitem><para>
Queries may contain conditions enclosed in parentheses, in this case
<literal>(d != c) &amp;&amp; (distance(c, d) &lt; 2)</literal>
and <literal>isRelevant(f)</literal>.
</para></listitem>
<listitem><para>
Patterns for edges can be specified.
<literal>-(branch|successor)-></literal> matches edges of type
<methodname>branch</methodname> or <methodname>successor</methodname>
which are traversed in forward direction.
</para></listitem>
<listitem><para>
Transitive closures of binary relations, e.g. edge relations, can be
used in queries. For example,
<literal>(</literal><replaceable>edge</replaceable><literal>)*</literal>
stands for all paths that can be built by traversing
<replaceable>edge</replaceable> an arbitrary number of times.
</para></listitem>
</itemizedlist>
</para>

</section>

<section><title>Quasi-Parallel Assignments</title>

<para>
For the XL programming language, rules are applied
in parallel. To be more precise, they take effect as if they were
executed in parallel. This holds at least for
structural changes they apply to the graph. For example, the rule
<programlisting>
a:A ==>> a A;
</programlisting>
appends a new node of class <classname>A</classname> to every existing node
<methodname>a</methodname> of class <classname>A</classname>.
Thus, the rule creates nodes which
match its own left hand side. However, because rules take effect as
if they were executed in parallel, the newly created nodes are not
visible for the matching process until the rule has been applied
completely to the whole graph, or, to be more precise, until
a <firstterm>transformation boundary</firstterm> has been passed.
</para>
<para>
Now consider the following example of an execution rule:
<programlisting>
a:A b:A ::> b.x = a.x;
</programlisting>
For every node <methodname>a</methodname> of class <classname>A</classname>
having a successor <methodname>b</methodname> of class
<classname>A</classname> in the graph, the value of the variable
<literal>a.x</literal> is forwarded to the variable <literal>b.x</literal>.
This can be seen as the propagation of a value along a sequence
of nodes of class <classname>A</classname>.
</para>
<para>
This rule contains an intricacy: It makes modifications to variables
<literal>b.x</literal> which may happen to be the input variables
<literal>a.x</literal> for later executions of the rule. For example,
suppose the graph consists of a sequence of nodes of class
<classname>A</classname>, and suppose that the matches for the query
<literal>a:A b:A</literal> are found from left to right. Then the
node <methodname>b</methodname> of one match will be the node
<methodname>a</methodname> of the following match. In this case, the value
of <methodname>x</methodname> of the first node of the sequence propagates
through the whole sequence within just one application of the rule
to the graph. On the other hand, if the matches
are found from right to left, the value only propagates by one within
one application. This leads to indeterministic behaviour, because
the XL programming language does not specify conditions for the order
of matches.
</para>
<para>
This problem is solved by making use of
<firstterm>quasi-parallel assignments</firstterm>
(<xref linkend="s-qpa"/>). If the rule is changed to
<programlisting>
a:A b:A ::> b[x] := a[x];
</programlisting>
the assignments take effect as if they were executed in parallel, i.e.,
their modifications are not visible until a transformation boundary
has been passed. In this case, it means that the propagation of values
is by one within one application of the rule to the graph. This delayed
effect of assignments is also useful for certain numerical computations
where new values are computed based on the current ones. If the new values
overwrote immediately the current ones, these computations would use
partly the current values, partly the newly computed ones. Using
quasi-parallel assignments, computations consistently use current values.
</para>
<para>
The example reveals the ingredients for quasi-parallel assignments:
<itemizedlist>
<listitem><para>
Quasi-parallel assignment operators
<literal>:= :+= :-= :*= :/= :&amp;= :|= :^=</literal>
</para></listitem>
<listitem><para>
Property variables (<xref linkend="s-properties"/>), indicated by their
names enclosed in brackets.
</para></listitem>
</itemizedlist>
</para>

</section>

</section>


<section><title>Imperative Extensions</title>

<para>
This section gives examples for the imperative extensions. These are not
related to relational data and can be seen as extensions of the
Java programming language on their own. They remain within the
imperative programming paradigm.
</para>

<section><title>Expression Lists and the Comma Operator</title>

<para>
The C programming language defines the <firstterm>comma operator</firstterm>
which can be used to construct expression lists
like <literal>(t = a*sin(x), sqrt(1 - t*t))</literal>.
It evaluates its left and right subexpressions
and returns the value of the latter.
The comma operator is not defined in the Java programming language,
but reintroduced in the XL programming language by the specification
of <firstterm>expression lists</firstterm>
(<xref linkend="s-elist"/>).
</para>
<para>
Expression lists are useful if a function
(<literal>t -> sqrt(1 - t*t)</literal> in the example)
is to be applied to an expression
(<literal>a*sin(x)</literal>) and if
this function does not exist as a named function.
A temporary variable (<methodname>t</methodname>) is needed,
which can be declared explicitly
in the expression list,
<literal>(double t = a*Math.sin(x), Math.sqrt(1 - t*t))</literal>,
or implicitly as in
<literal>($ = a*Math.sin(x), Math.sqrt(1 - $*$))</literal>, using
the special identifier <methodname>$</methodname>.
</para>

</section>

<section><title>Instance Scope Expressions</title>

<para>
<firstterm>Instance scope expressions</firstterm>
(<xref linkend="s-iscope"/>) are similar to
the <literal>with</literal>-statement of the Basic and Pascal programming
languages. They can be used when several consecutive expressions access
fields or methods of a common instance as in
<programlisting>
c.getBounds().setLocation(0, 0);
c.getBounds().width = 100;
c.getBounds().height = 50;
</programlisting>
Here, <literal>c.getBounds()</literal> is the common instance. Using an
instance scope expression, the example is reduced to
<programlisting>
c.getBounds().(setLocation(0, 0), width = 100, height = 500)
</programlisting>
Thus, an instance scope expression
includes the instance (the expression on the left hand side
of <literal>.</literal>) in the scope of the right hand side. The main
purpose is the use within initialization of objects as in
<programlisting>
Cylinder a = new Cylinder().(setRadius(2), setColor(0xff0000), setAxis(1, 0, 0));
</programlisting>
In addition, if a local variable with identifier <methodname>$</methodname>
has not yet been defined in the current scope, then the instance can
be addressed within the right expression using this special identifier.
</para>

</section>

<section id="s-f-overloading">
<title>Operator Overloading</title>

<para>
The XL programming language provides a mechanism for operator overloading as
it is known from other programming languages, e.g., C++. This allows
programmers to add new meanings to operators like <literal>+</literal>.
For example, consider a class <classname>Complex</classname> which is declared
as follows:
<programlisting>
class Complex {
    double real;
    double imag;

    Complex (double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    Complex operator+ (Complex b) {
        return new Complex(this.real + b.real, this.imag + b.imag);
    }
}
</programlisting>
The method declaration <literal>operator+</literal> declares an
operator method (<xref linkend="s-operator-methods"/>). This method
gives the operator <literal>+</literal> a meaning in the context
of <classname>Complex</classname> numbers in the following way:
An expression <literal>a + b</literal>, where <literal>a</literal>
and <literal>b</literal> are <classname>Complex</classname> numbers,
is translated into the method invocation expression
<literal>a.operator+(b)</literal>.
</para>

</section>

<section id="s-f-gen">
<title>Generators, Aggregate Methods, and Filter Methods</title>

<para>
A very useful new feature of the XL programming language are
<firstterm>generators</firstterm>
(<xref linkend="s-genexpr"/>). They yield multiple results
in succession. As an example, consider the range expression
<literal>1 : 10</literal> (<xref linkend="s-range"/>):
This is a very simple generator
which yields the values 1 up to 10, one after another. Such a generator
may be used as the iterator of a <literal>for</literal>-statement:
<programlisting>
for (int i : (1 : 10)) { /* do something */ }
</programlisting>
Or it may be used as an argument to
<firstterm>aggregate methods</firstterm>
(<xref linkend="s-amethod-invocation"/>) which perform computations
on the set of yielded values and return a single value:
<programlisting>
int p = prod(1 : 10);
int s = sum(2 * (1 : 4));
</programlisting>
This example makes use of the aggregate methods
<methodname>prod</methodname> and <methodname>sum</methodname>
which are declared in the class
<classname>de.grogra.xl.lang.Operators</classname>. Aggregate methods
can also be used for arrays as in
<programlisting>
int[] a = {1, 2, 3, 4, 5};
int s = sum(a);
</programlisting>
</para>
<para>
Generators and aggregate methods are especially useful in the context of
queries (<xref linkend="s-f-queries"/>). For example, the following
expression computes the length of all instances of class
<classname>F</classname> whose diameter exceeds a threshold
<methodname>t</methodname>:
<programlisting>
float len = sum((* f:F, (f.diameter > t) *).length);
</programlisting>
New generators can be declared using <firstterm>generator methods</firstterm>
(<xref linkend="s-generator-methods"/>).
They are indicated in the method header by an asterisk between the
result type and the name, and they may yield multiple values via the
<literal>yield</literal>-statement:
<programlisting>
long* fibonacci(int n) {
  long a = 1, b = 1;
  while (--n >= 0) {
    yield a;
    long t = a; a = b; b += t;
  }
}
</programlisting>
Now the sum of the first 10 Fibonacci numbers can be computed by
the expression <literal>sum(fibonacci(10))</literal>, or these numbers
can be written to the console by
<programlisting>
System.out.println(fibonacci(10));
</programlisting>
The generator method could be implemented without termination condition.
In principle, then it
computes the Fibonacci numbers <foreignphrase>ad infinitum</foreignphrase>:
<programlisting>
long* fibonacci() {
  long a = 1, b = 1;
  while (true) {
    yield a;
    long t = a; a = b; b += t;
  }
}
</programlisting>
Now the termination has to be implemented externally, as in
<programlisting>
int n = 10;
for (long f : fibonacci()) {
  System.out.println(f);
  if (--n &lt; 0) {
    break;
  }
}
</programlisting>
Such a termination can also be implemented by
<firstterm>filter methods</firstterm> (<xref linkend="s-fmethod-invocation"/>)
which filter values of generator expressions. For example, the filter method
<methodname>slice</methodname> in the class
<classname>de.grogra.xl.lang.Operators</classname> filters a slice
of values:
<programlisting>
System.out.println(slice(fibonacci(), 10, 20));
</programlisting>
This writes the Fibonacci numbers from 10 (inclusive) to
20 (exclusive) to the console.
</para>

</section>

</section>

</chapter>
