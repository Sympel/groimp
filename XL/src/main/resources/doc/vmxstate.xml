<chapter id="c-vmxstate">
<title>Extended State of the Virtual Machine</title>

<para>
The implementation of some features of the XL programming language
makes uses of an <firstterm>extended state</firstterm> of the
virtual machine. This extended state is represented by an instance of
<classname>de.grogra.xl.runtime.VMXState</classname> and exists
for every thread of the current Java virtual machine. It can be obtained by
the method invocation <literal>VMXState.current()</literal>.
</para>
<para>
The extended state provides a stack similar to the stack of the
Java virtual machine. Stack frames provide storage locations for local
variables, which are represented by instances of the class
<classname>VMXState.Local</classname>. The stack has the possibility of
accessing enclosing stack frames which is needed, for example,
by nested routines (and is not possible with the stack of the Java
virtual machine). Though nested routines are not specified
by the XL programming language explicitly, they are needed
implicitly for the implementation of non-local generator invocations,
namely generator method invocations and query invocations.
</para>
<para>
It is not mandatory that a compiler for the XL programming language
generates code which makes use of the stack of the extended state
for these language extensions, with two exceptions:
<orderedlist>
<listitem><para>
Property variables (<xref linkend="s-properties"/>) are read and written
in the context of the current extended state.
</para></listitem>
<listitem><para>
As it will be discussed in <xref linkend="c-queries"/>,
query variables (see <xref linkend="s-queryvar"/>) have to be
represented by instances of <classname>VMXState.Local</classname>. Thus,
they have to be stored on the stack of the extended state.
</para></listitem>
</orderedlist>
</para>

</chapter>
