package de.grogra.billboard.vrml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.grogra.billboard.CakeBillboarder;
import de.grogra.billboard.BBResources;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.MimeType;

public class VRMLCakeBuilder extends CakeBillboarder implements VRMLBuilder{

	private String vrmlFileName		= "bb";	

	private String vrmlTempl		= "";
	
	private File vrmlDest;
	
	public VRMLCakeBuilder(File file, MimeType mt)
	{
		super (file, mt);		
		
		vrmlDest = new File(file.getPath() + 
								File.separator + 
								vrmlFileName + ".wrl"
							);
		
		vrmlTempl				=	BBResources.getVRMLTempl(this.getClass(), "vrmlcaketempl.txt");
	}
	
	public VRMLCakeBuilder(File file, MimeType mt, String subDirectionary) throws IOException
	{

		super (file, mt);	
		
		vrmlDest				= new File(file.getPath() + 
										File.separator + 
										vrmlFileName + ".wrl");

		file					= new File(file.getPath() + 
										File.separator + 
										subDirectionary);
		if(!file.isDirectory())
		{
			file.mkdir();
		}
		
		
		imageFolder 			= subDirectionary;
		
		vrmlTempl				=	BBResources.getVRMLTempl(this.getClass(), "vrmlcaketempl.txt");
		
		this.setRunLater();
	}
	
	
	public String getVRMLTempl()
	{
		return vrmlTempl;
	}
	

	public void writeVRML(int width, int height)
	{
		String vrmlOutput	= vrmlTempl;
		String vrmlProtos 	= "";		
		
		int i = 0;

		while(i < sides) 
			vrmlProtos += "PngFrame { url \"" + getBBName(i++) + "\" rotate 0 1 0 " + ( (i/(double) sides) * (Math.PI*2) )  + "}" + System.getProperty("line.separator");				
		
		double outputHeight = ((double) height) / ((double) width);
			
		vrmlOutput	=	vrmlOutput.replaceAll("::REL_HEIGHT::", Double.toString(outputHeight) );
		vrmlOutput	=	vrmlOutput.replaceAll("::PNG_PROTO_CALL::", vrmlProtos);
		vrmlOutput	=	vrmlOutput.replaceAll("::BILL_NUM::", Integer.toString(sides));
	
		try 
		{
			FileWriter outputStream 	= new FileWriter(vrmlDest);
			BufferedWriter  outputObj	= new BufferedWriter(outputStream);
			
			outputObj.write(vrmlOutput);
			
			outputObj.flush();
			outputStream.close();
			
		} catch (IOException ex)
		{
			Workbench.current().logGUIInfo ("Error: " + ex.getMessage());
		
		}			
	}
		
	@Override
	protected void finalAction()
	{
		// Write the VRML-file
		writeVRML(imgWidth, imgHeight);
		
		super.finalAction();
	}

}
