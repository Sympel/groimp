package de.grogra.billboard;

import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.vecmath.Point3d;

import de.grogra.graph.GraphState;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.imp.IMP;
import de.grogra.imp.IMPWorkbench;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.ray2.Raytracer;
import de.grogra.math.TVector3d;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.expr.ObjectExpr;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.util.LockProtectedCommand;
import de.grogra.util.Lock;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.vecmath.Matrix34d;

public abstract class Billboarder {
	
	protected RotationCamera camera;
	protected View3D view3d;
	protected double RotationAngle		= 0;
	protected TVector3d rotationPoint	= new TVector3d(0,0,0);
	protected double angleOffset		= 0;
	
	protected String pathAndFileName	= "";
	protected String imageFolder		= "";
	protected String prefix				= "bill_";
	protected String appendfix			= "";
	protected String suffix				= "";	
	
	private ArrayList<String> bbNameList;
	
	protected int countZero				= 0;
	
	protected File file;
	protected MimeType mt;
	
	protected int imgWidth				= 0;
	protected int imgHeight				= 0;
	
	protected int sides					= 1;
	protected int currentSide			= 0;
	
	private LockProtectedCommand lpc;
	private LockProtectedCommand lpcLast;
	
	/**
	 * The MIME type <code>image/jpeg</code> without parameters.
	 */
	public static final MimeType MT_JPG = new MimeType ("image/jpeg", null);
	
	/**
	 * The MIME type <code>image/pjpeg</code> without parameters.
	 */
	public static final MimeType MT_PJPG = new MimeType ("image/pjepg", null);
	
	/**
	 * The MIME type <code>image/png</code> without parameters.
	 */
	public static final MimeType MT_PNG = new MimeType ("image/png", null);
	
	/**
	 * The MIME type <code>image/exr</code> without parameters.
	 */
	public static final MimeType MT_EXR = new MimeType ("image/exr", null);
	
	/**
	 * The MIME type <code>image/x-exr</code> without parameters.
	 */
	public static final MimeType MT_X_EXR = new MimeType ("image/x-exr", null);
	
	
	protected void setDestination(File file, MimeType mt) throws IOException
	{
		if(!file.isDirectory())
		{
			file.mkdir();
		}
		
		this.file = file;
		this.mt = mt;
		
		if (mt == MT_JPG)
			suffix	= "jpg";				
			
		if (mt == MT_PJPG)
			suffix	= "jpg";				
		
		if (mt == MT_PNG)
			suffix	= "png";				
				
		if (mt == MT_EXR)
			suffix	= "exr";				
		
		if (mt == MT_X_EXR)		
			suffix	= "exr";		
	}
	
	/**
	 * This method has to be invoke before invoke {@link: #billboarding()}.
	 * 
	 * @param distance Distance between camera and the origin of the coordinates-system
	 * @param zOfsset The z-axis shift of the camera
	 * @param sides How much sides has to be rendered of the object.
	 */
	public void initialize(double distance, double zOfsset, int sides)
	{
		if(sides > 0)
		{
			this.sides					= sides;
			RotationAngle				= Math.round(360/sides);
			
			countZero 					= (int) Math.floor(Math.log10(sides));	
			
			bbNameList					= new ArrayList<String>();			
		}
		
		// Set camera to initial position
		camera.setDistance(distance, zOfsset);
		
		// Set the lpcs
		setLPCs();
	}
		
	/**
	 * The Billboarder starts at the given side. Attention: the first side
	 * has the number 0.
	 * 
	 * @param side
	 */
	public void beginAt(int side)
	{
		if(side > 0 && side <= sides)
		{
			currentSide = side;
			nextSide();
			
			String zeros	= getLeadingZeros(sides, currentSide);	
			
			String tempFileName = "";
			
			// Filename of the rendered billboard
			tempFileName = prefix + zeros + currentSide + appendfix +  "." + suffix;
			
			// Remember the path and filename for further actions
			bbNameList.add((imageFolder != "" ?  imageFolder + "/" : "") + tempFileName);
		}
	}
	
	/**
	 * Shift the camera after the rotation to the value <code>angleOffset</code>. This 
	 * is helpful if you already got some rendered images of a model and you want to
	 * render the images between. Maybe you've got a 20-image billboard. So the 
	 * <code>RotationAngle</code> is (360/20 sides). And now you want 20 images more.
	 * You can take the half of the value <code>RotationAngle</code> from the last 
	 * billboard-render. In this case <code>RotationAngle</code>/2 = 9. But beware:
	 * you have to safe the images of your last render, cause the billboard starts to
	 * count at zero. This old images could be overwritten.
	 * 
	 * @param angleOffset 
	 */
	public void angleOffset(int angleOffset)
	{
		this.angleOffset = angleOffset;
	}
	
	/**
	 * Just put the camera to the given <code>side</code> of view.
	 */
	protected void nextSide(int side)
	{
		camera.moveAroundZ((RotationAngle*side) - angleOffset);
	}
	
	/**
	 * Just put the camera to the next side of view.
	 */
	protected void nextSide()
	{
		camera.moveAroundZ((RotationAngle*currentSide) - angleOffset);
	}
		
	/** 
	  * This method rendered each side of an object given in the view. The number
	  * of sides was set by invoke the initialize - method. After a render-step
	  * the camera will rotate and it will be start the next render-step.
	  * 
	  */
	public void billboarding()
	{
			
		ObjectExpr objExpr	= (ObjectExpr) Item.resolveItem(Workbench.current(), "/renderers/3d/ray2");
		
		// Creates a new Renderer
		Raytracer raytracer	= (Raytracer) objExpr.evaluate(Workbench.current(), new StringMap());	
		
		Workbench.current().logGUIInfo (BBResources.msg ("billboard.progress", currentSide, sides));
		
		// Add a new observer to the renderer. Cause we observe the view-panel and the image, wich will
		// be write to disk.s
		raytracer.addImageObserver(new ImageObserver() {
									
				@Override
				public boolean imageUpdate(Image img, int infoflags, int x, int y, final int width, final int height) 
				{
					if ((infoflags & ALLBITS) == 0)
					{
						// Image was updated, cause its not ready yet.
						return true;
					} 			
					
					currentSide++;
														
					if(imgWidth == 0)
						imgWidth	= width;
					
					if(imgHeight == 0)
						imgHeight	= height;	
					
					String tempFileName = "";
					
					String zeros	= getLeadingZeros(sides, currentSide);		
					
					// Filename of the rendered billboard
					tempFileName = prefix + zeros + currentSide + appendfix +  "." + suffix;
					
					// Remember the path and filename for further actions
					insertName((imageFolder != "" ?  imageFolder + "/" : "") + tempFileName);

					tempFileName  = (imageFolder != "" ? imageFolder + File.separator : "") + tempFileName;
					
					pathAndFileName	= file.getPath() + File.separator + tempFileName ;
					
					// This writes the rendered image to Disk		
					IMP.writeImage(view3d,(RenderedImage) img, mt, new File(pathAndFileName));
					
					if(currentSide < sides)
					{
						// To receive information about the progress during rendering, we have to
						// invoke from main-thread the next render-task, because maybe we want to
						// render another side of view.
						view3d.getWorkbench().getJobManager().runLater(lpc, null, view3d, 0);
						
					} else if(lpcLast != null) {
						
						// The method finalAction going to executed.
						view3d.getWorkbench().getJobManager().runLater(lpcLast, null, view3d, 0);
					}
									
					// Return if image is ready (no more image updates => false)
					return false;
				}
			} 
		);
		
		// Starts the renderer		
		view3d.getViewComponent().render((raytracer));
	}	
	
	/**
	 * This generates a string with leading zeros. The number of "0" in 
	 * the string depends on the log10 of a maximum number <code>limit</code> 
	 * and a <code>position</code>. Its used by generating a filename with
	 * leading zeros. This is important to beware the order of the files, cause
	 * filenames are strings and not numbers.
	 * 
	 * For 14 files you've got one leading 0: 
	 * "bill_01.jpg, bill_02.jpg, ... bill_14.png
	 * 
	 * @param limit For creation of the maximum number of zeros.
	 * @param position How much zeros are at a position.
	 * @return A string filled with "0".
	 */
	protected String getLeadingZeros(int limit, int position)
	{
		if(limit < 10)
			return "";
		
		// Calculate the maximum number of "0"s
		int countTemp 	= (int) Math.floor(Math.log10(limit));				
		
		// Calculate the number of "0"s at a position
		countTemp		-= position > 0 ? (int) Math.floor(Math.log10(position)) : 0;	
		
		String zeros	= "";
		
		// Generates the "0"-string
		while(countTemp-- > 0)
			zeros	+=	"0";		
		
		return zeros;
	}
	
	/**
	 * Stores the name of a rendered billboard into an internal list.
	 * 
	 * @param name Name of a billboard picture file.
	 */
	protected void insertName(String name)
	{
		if(name != null && !name.equals(""))
			bbNameList.add(name);
	}
	
	/**
	 * This method return the path and filename of the billboard as
	 * an URI-String
	 * 
	 * @param index Number of the rendered billboard
	 * @return Path and filename of the given billboard-number.
	 */
	protected String getBBName(int index)
	{
		if(index >= 0 && index < bbNameList.size())
		{
			return bbNameList.get(index);			
		} else
			return null;
	}
	
	/**
	 * This is an internal method invoked during execution of {
	 * @link #initialize(double, double, int)}.
	 */
	private void setLPCs()
	{
		lpc = new LockProtectedCommand(view3d.getGraph(),true,0)
		{
			@Override
			protected void runImpl(Object arg, Context c, Lock lock)
			{					
				// Execute the LockProtectedCommand
				setRunLater();
			}
		};
		
		lpcLast = new LockProtectedCommand(view3d.getGraph(),true,0)
		{
			@Override
			protected void runImpl(Object arg, Context c, Lock lock)
			{					
				// Execute the LockProtectedCommand
				finalAction();
			}
		};
	}
	
	public static Point3d location (Node node)
	{
		Point3d p = new Point3d();
		Matrix34d m = transformation (node, false);
		p.x = m.m03;
		p.y = m.m13;
		p.z = m.m23;
		return p;
	}
	
	public static Matrix34d transformation (Node node, boolean post)
	{
		return GlobalTransformation.get (node, true, GraphState.current (node
			.getGraph ()), post);
	}
	
	public static GraphManager graph ()
	{
		IMPWorkbench w = (IMPWorkbench) Workbench.current();
		return (w != null) ? w.getRegistry ().getProjectGraph () : null;
	}
	
	/**
	 * When a Billboard-Render-Thread is finished, the implementation of this
	 * method will be executed later.
	 */	
	abstract protected void setRunLater();
	
	
	/**
	 * This method is invoked after the last billboard was rendered.
	 */
	protected void finalAction()
	{
		// If there is no final action after rendering all billboards, its not
		// necessary to implement this method. Thats why this method is not
		// abstract.
	}
	
	static public void justRenderView(final File file, final MimeType mt)
	{
		final View3D view3d = View3D.getDefaultView(Workbench.current());
		ObjectExpr objExpr	= (ObjectExpr) Item.resolveItem(Workbench.current(), "/renderers/3d/ray2");
		
		// Creates a new Renderer
		Raytracer raytracer	= (Raytracer) objExpr.evaluate(Workbench.current(), new StringMap());	
		
		// Add a new observer to the renderer. Cause we observe the view-panel and the image, wich will
		// be write to disk.s
		raytracer.addImageObserver(new ImageObserver() {
									
				@Override
				public boolean imageUpdate(Image img, int infoflags, int x, int y, final int width, final int height) 
				{
					if ((infoflags & ALLBITS) == 0)
					{
						// Image was updated, cause its not ready yet.
						return true;
					} 										
					// This writes the rendered image to Disk		
					IMP.writeImage(view3d,(RenderedImage) img, mt, file);
														
					// Return if image is ready (no more image updates => false)
					return false;
				}
			} 
		);
		
		// Starts the renderer		
		view3d.getViewComponent().render((raytracer));
	}
	
}