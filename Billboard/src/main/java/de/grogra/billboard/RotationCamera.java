package de.grogra.billboard;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Quat4d;

import de.grogra.imp3d.Camera;
import de.grogra.imp3d.ParallelProjection;
import de.grogra.imp3d.PerspectiveProjection;
import de.grogra.imp3d.View3D;
import de.grogra.math.TMatrix4d;
import de.grogra.math.TVector3d;
import de.grogra.pf.ui.Workbench;

/**
 * This is an implementation to rotate the camera around an axis by a given distance
 * and a given angle. 
 * 
 * @author adgen
 *
 */
public class RotationCamera {
	
	private Camera camera;
	private View3D view3d;
	private TVector3d rotationPoint	= new TVector3d(0,0,0);
	private TMatrix4d initMatrix4d; 

	public RotationCamera()
	{		
		view3d 						= View3D.getDefaultView(Workbench.current());
		camera 						= view3d.getCamera();
		
		initMatrix4d 				= new TMatrix4d();
		initMatrix4d.m00 			= 1;
		initMatrix4d.m12 			= 1;
		initMatrix4d.m11 			= 0;
		initMatrix4d.m22 			= 0;
		initMatrix4d.m21 			= -1;
		initMatrix4d.m03 			= 0;
		initMatrix4d.m13 			= 0;
		initMatrix4d.m23 			= 0;
		initMatrix4d.m33			= 1;
		
		camera.setTransformation(new TMatrix4d(initMatrix4d));		
	}
	
	/**
	 * Sets the height and the distance to worlds origin of the camera.
	 * 
	 * @param dist Distance to the coordinates origin on.
	 * @param zOffset Height of the camera over the ground.
	 */
	public void setDistance(double dist, double zOffset)
	{		
		initMatrix4d.m13 			= -zOffset;
		initMatrix4d.m23 			= -dist;		
		
		camera.setTransformation(initMatrix4d);
	}
	
	/**
	 * This calculate a new matrix depending on angle and the initial 
	 * matrix of the camera.
	 * 
	 * @param angle Value of the Rotation.
	 * @return Transformation-Matrix of the camera.
	 */	
	public TMatrix4d calculateMatrix(double angle)
	{
		if(angle == 0)
			return initMatrix4d;
		
		Quat4d q 					= new Quat4d();
		TMatrix4d m 				= new TMatrix4d();
		
		// Copy the transformation-matrix of current position of the camera
		TMatrix4d tm 				= new TMatrix4d(initMatrix4d);

		// Rotate the camera to an given angle around the Z-axis
		q.set((new AxisAngle4d(0, 0, 1, Math.toRadians(angle))));
		
		// Sets the rotation-point
		m.set(q, rotationPoint, 1);
		
		// Combine the 2 matrix
		tm.mul(tm, m);	 

		return tm;
	}
	
	/**
	 * Just invoke {@link #calculateMatrix(double)} and set the transformation
	 * of the camera.
	 *
	 * @param anglePerStep Value for one step of rotation.
	 * @param step Number of current step.
	 */
	public void moveAroundZ(double angle)
	{
		camera.setTransformation(calculateMatrix(angle));			
	}
	
	/**
	 * Makes the projection of the camera parallel.
	 */
	public void makeParallel()
	{
		this.camera.setProjection(new ParallelProjection());		
	}
	
	/**
	 * Makes the projection of the camera perspective.
	 */
	public void makePerspective()
	{
		this.camera.setProjection(new PerspectiveProjection());		
	}
	
	/**
	 * Gets the <code>view3d</code>
	 * 
	 * @return The used view3d-object.
	 */
	public View3D getView3D()
	{
		return this.view3d;
	}
	
	/**
	 * If the camera has to look up or look down, just invoke this method.
	 * This method rotate the camera around the local x-axis.
	 * 
	 * @param angle Rotate the Camera around local x-axis.
	 */
	public void lookUpAndDown(double angle)
	{
		TMatrix4d tm 				= new TMatrix4d(camera.getTransformation());
		TMatrix4d m 				= new TMatrix4d();
		
		m.rotX(Math.toRadians(angle));
		tm.mul(tm, m);	
		
		camera.setTransformation(tm);
	}
}
