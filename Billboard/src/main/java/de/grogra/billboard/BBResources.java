/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.billboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import de.grogra.pf.ui.Workbench;

public final class BBResources
{
	public static final ResourceBundle BUNDLE = ResourceBundle
		.getBundle ("de.grogra.billboard.Resources");

	public static final String msg (String key)
	{
		return BUNDLE.getString (key);
	}

	public static final String msg (String key, Object... args)
	{
		return MessageFormat.format (BUNDLE.getString (key), args);
	}
	
	public static String getVRMLTempl(Class<?> clazz, String resource)
	{
		String path = clazz.getPackage().getName() + "/" + msg ("billboard.vrml.pathToTemplates");
		path 		= "/" + path.replace('.','/');
		resource	= path + "/" + resource;
		
		String ret = "";		
		
		try {
			InputStream is = BBResources.class.getResourceAsStream(resource);	
			String line = "";
			
			if(is != null)
			{
				BufferedReader in = new BufferedReader(new InputStreamReader(is));
				StringBuffer buffer = new StringBuffer();
				while ((line = in.readLine()) != null) {
					buffer.append(line + System.getProperty("line.separator"));
				}						
				ret =  buffer.toString(); 
			} 
			
		} catch (IOException ex) { 
			Workbench.current().logGUIInfo ("No such a resource found: " + resource);
		}
		
		return ret;
	}

}
