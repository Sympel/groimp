/**
 * 
 */
/**
 * @author tim
 *
 */
module projectmanager {
	exports de.grogra.projectmanager;
	
	requires platform;
	requires platform.core;
	requires utilities;
	requires java.desktop;
	requires graph;
	requires java.logging;
	requires xl.core;
}