package de.grogra.projectmanager;

import java.io.IOException;

import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ProjectLoader;
import de.grogra.pf.io.RegistryLoader;
import de.grogra.pf.registry.Directory;
import de.grogra.pf.registry.Executable;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.ProjectManager;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.ProjectFactory;
import de.grogra.util.Map;
import de.grogra.util.StringMap;

public class ProjectFactoryImpl implements ProjectFactory {

	@Override
	public ProjectImpl createProject(Registry registry, ProjectManager pm) throws IOException {
		ProjectImpl p =  new ProjectImpl(registry, pm);
		loadRegistry(p.getRegistry(),null,true);
		return p;
	}

	public ProjectImpl loadProject(FilterSource fs, Map initParams, Registry registry, ProjectManager pm)
			throws IOException {
		// registry.setEmptyGraph();
		Registry.setCurrent(registry);
		Object o = registry;
		ProjectImpl ep = new ProjectImpl(registry, pm);
		FilterSource s = IO.createPipeline(fs, IOFlavor.PROJECT_LOADER);
		if (!(s instanceof ObjectSource)) {
			System.out.print("error1");
			return null;
		}
		ProjectLoader loader = (ProjectLoader) ((ObjectSource) s).getObject();
		ep.setName(IO.toSimpleName(fs.getSystemId()));
		loadRegistry(registry, loader, true);
		Registry.setCurrent(registry);
		// Workbench.setCurrent();
		loader.loadGraph(registry);
		if (fs instanceof FileSource) {
			ep.setFile(((FileSource) fs).getInputFile(), fs.getFlavor().getMimeType());
		}
		return ep;
	}

	private static void loadRegistry(Registry reg, RegistryLoader loader, boolean project) throws IOException {
		StringMap m = new StringMap().putObject("registry", reg);
		if (project) {
			((Item) reg.getRoot()).add(new Directory("project"));
		}
		Executable.runExecutables(reg.getRootRegistry(), "/hooks/configure", reg, m);
		if (loader != null) {
			loader.loadRegistry(reg);
		}
		Executable.runExecutables(reg.getRootRegistry(), "/hooks/complete", reg, m);
		reg.activateItems();
	}
	
}
