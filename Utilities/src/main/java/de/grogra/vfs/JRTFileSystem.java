
package de.grogra.vfs;

import java.io.*;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A wrapper for the JRT filesystem, as groimp filesystem do not extend java filesystem.
 * This wrapper should not be able to overwrite its content. 
 * The Entries do not contains content, only their name match the path where they are 
 * stored in the jrt path. When "opened", the Entry get the inputstream from the jrt.
 *
 */
public class JRTFileSystem extends FileSystemBase
{
	
	public static final JRTFileSystem JRT_ADAPTER
		= new JRTFileSystem (null);

	private class Entry extends ByteArrayOutputStream implements GFile
	{
		final Entry parent;
		final String name;
		long time = System.currentTimeMillis ();
		boolean isDirectory=false;


		Entry (Entry parent, String name, boolean isDirectory)
		{
			super (isDirectory ? 0 : 128);
			this.parent = parent;
			this.name = name;
			this.isDirectory = isDirectory;
		}

		
		@Override
		public void flush ()
		{
		}
		
		@Override
		public void close ()
		{
			synchronized (JRTFileSystem.this)
			{
				time = System.currentTimeMillis ();
			}
	    	fireTreeModelEvent (NODES_CHANGED, getEventFor (this));
		}
		
		/**
		 * close file without triggering event
		 */
		public void closeQuiet()
		{
			synchronized (JRTFileSystem.this)
			{
				time = System.currentTimeMillis ();
//				bufForInput = buf;
//				bufForInputCount = count;
			}
		}

		@Override
		public boolean isDirectory() {
			return isDirectory;
		}
		
		public String toString() {
	        return name;
	    }
	}


	final Entry root;


	public JRTFileSystem (String fsName)
	{
		super (fsName, "jrtfs");
		root = new Entry (null, "/modules", true);	
	}


	@Override
	public boolean isPersistent ()
	{
		return false;
	}


	@Override
	public void delete (Object file)
	{
	}


	@Override
	public synchronized String getName (Object file)
	{
		return ((Entry) file).name;
	}


	@Override
	public synchronized Object getParent (Object file)
	{
		return ((Entry) file).parent;
	}


	public synchronized boolean isLeaf (Object file)
	{
		List<Path> children=null;
		Path p = Paths.get(URI.create("jrt:/")).resolve(getCompletePath(file));
		try (Stream<Path> walk = Files.walk(p, 1)) {
			children = walk.collect(Collectors.toList());
	    } catch (IOException e) {
			e.printStackTrace();
		}
		children.remove(0); // the walk includes itself
		return children.size()==0;
	}

	
	public String getCompletePath(Object file)
	{
		StringBuffer buf = new StringBuffer ();
		Entry e = ((Entry)file);
		buf.insert(0, '/'+((Entry)file).name);
		e=e.parent;
		while (e !=null) {
			if (e.name.charAt(0)=='/')
				buf.insert(0, e.name);
			else
				buf.insert(0, '/'+e.name);
			e = e.parent;
		}
		return buf.toString();
	}
	

	@Override
	public boolean isReadOnly (Object file)
	{
		return true;
	}


	@Override
	public synchronized long getTime (Object file)
	{
		return ((Entry) file).time;
	}

	/**
	 * close file without triggering event
	 */
	public void closeQuiet(Object file)
	{
		((Entry)file).closeQuiet();
	}

	@Override
	public synchronized void setTime (Object file, long time)
	{
		((Entry) file).time = time;
	}  


	@Override
	public synchronized long getSize (Object file)
	{
		throw new UncheckedIOException(new IOException("Cannot access jrt content"));
	}


	@Override
	public Object getRoot ()
	{
		return root;
	}


	@Override
	public synchronized Object[] listFiles (Object parent)
	{
		List<Path> children=null;
		Path p = Paths.get(URI.create("jrt:/")).resolve((getCompletePath(parent)));
		try (Stream<Path> walk = Files.walk(p, 1)) {
			children = walk.collect(Collectors.toList());
	    } catch (IOException e) {
			e.printStackTrace();
		}
		children.remove(0); // the walk includes itself
		ArrayList<Entry> childrenEntry = new ArrayList<Entry>();
		children.forEach(c -> {
			childrenEntry.add( getEntry(parent, c.getFileName().toString(), false) );
		});
		return childrenEntry.toArray(new Entry[childrenEntry.size()]);
		
	}


	@Override
	public synchronized Object getFile (Object parent, String name)
	{
		throw new UncheckedIOException(new IOException("Cannot access jrt content"));
	}


	@Override
	protected Object createImpl (Object parent, String name, boolean createDirectory)
		throws IOException
	{
		throw new UncheckedIOException(new IOException("Cannot access jrt content"));
	}


	@Override
	public synchronized InputStream getInputStream (Object file)
	{
		Path p = Paths.get(URI.create("jrt:/")).resolve(getCompletePath(file));
		try {
			return Files.newInputStream(p);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public synchronized OutputStream getOutputStream (Object file, boolean append)
	{
		throw new UncheckedIOException(new IOException("Cannot access jrt content"));
	}


	@Override
	public void remove(Object file) {
		throw new UncheckedIOException(new IOException("Cannot access jrt content"));
	}
	
	public Entry getEntry(Object parent, String name, boolean isDirectory) {
		Entry e = new Entry((Entry)parent, name, isDirectory);
		return e;	
	}
}