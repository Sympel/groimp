module utilities {
	exports de.grogra.http;
	exports de.grogra.openexr;
	exports de.grogra.util;
	exports de.grogra.vfs;
	exports de.grogra.icon;
	exports de.grogra.annotation;
	
	requires xl.core;
	requires java.datatransfer;
	requires java.desktop;
	requires java.logging;
	requires java.xml;
	
	provides javax.imageio.spi.ImageReaderSpi with de.grogra.util.PPMImageReaderSpi;
	provides javax.imageio.spi.ImageWriterSpi with de.grogra.openexr.OpenExrImageWriterSpi;

}