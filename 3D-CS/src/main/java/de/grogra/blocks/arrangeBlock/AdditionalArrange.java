
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;
import de.grogra.persistence.SCOType;

public class AdditionalArrange extends ShareableArrangeBase implements ArrangeMethod 
{
	//enh:sco SCOType

	ShareableAdditionalArrangeBase method = new DartThrowing();
	//enh:field getter setter

	public AdditionalArrange() {
	}
	
	public AdditionalArrange(int number) {
		method.setNumber(number);
	}	
	
	public void calculate() {
		PointArrayList pointList = method.setAll(maxX, maxY, densityField);
		xx = new float[pointList.size()];
		yy = new float[pointList.size()];		
		for (int i = 0; i < pointList.size(); i++) {
			Point p = pointList.get(i);
			xx[i] = (float) p.getX();
			yy[i] = (float) p.getY();
		}
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field method$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (AdditionalArrange representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 1;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((AdditionalArrange) o).method = (ShareableAdditionalArrangeBase) value;
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((AdditionalArrange) o).getMethod ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new AdditionalArrange ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (AdditionalArrange.class);
		method$FIELD = Type._addManagedField ($TYPE, "method", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (ShareableAdditionalArrangeBase.class), null, Type.SUPER_FIELD_COUNT + 0);
		$TYPE.validate ();
	}

	public ShareableAdditionalArrangeBase getMethod ()
	{
		return method;
	}

	public void setMethod (ShareableAdditionalArrangeBase value)
	{
		method$FIELD.setObject (this, value);
	}

//enh:end


}
