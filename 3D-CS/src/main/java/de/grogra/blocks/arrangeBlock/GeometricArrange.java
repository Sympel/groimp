
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import de.grogra.blocks.Attributes;
import de.grogra.persistence.SCOType;
import de.grogra.rgg.Library;
import de.grogra.util.EnumerationType;

public class GeometricArrange extends ShareableArrangeBase implements ArrangeMethod 
{

	//enh:sco SCOType

	private static final EnumerationType TYPE = new EnumerationType(
			"geometricType", Attributes.I18N, 14);	
		
	int method = 0;
	// enh:field getter setter type=TYPE

	int number = ((superNumber!=-1)? superNumber: 25);
	// enh:field getter setter min=2 max=MAX_NUMBER

	int level = 5;
	// enh:field getter setter min=1 max=25

	
	public GeometricArrange() {	
	}	
	
	public GeometricArrange(int Number) {
		if (Number < MAX_NUMBER) {
			this.number = Number;
		} else {
			this.number = MAX_NUMBER;
		}
	}
	
	private boolean checkDensityField(double xi, double yi) {
		int xii = (int)(xi*(densityField.length-1)/(maxX-1));
		int yii = (int)(yi*(densityField.length-1)/(maxY-1));
		return Library.random(0,1) < densityField[xii][yii];
	}
	
	public void calculate() {
		double x=0, y=0;
		float[] xxObj = new float[number];
		float[] yyObj = new float[number];

		int vorzeichen = -1;
		int aktLevel = -1;
		int nlevel = 0;
		int j=0;
		int nextStep = 0;
		final int objProLevel = number / level;
		final float sumLevel = (float)((level*(level+1)) / 2.0);
		final float numSL = (float)(number/sumLevel);
		final int rest = number- objProLevel * level;
		double swX = (maxX-0.5) / (objProLevel+1);
		float w_n = (float)(2*Math.PI) / level;
		double wa1 = 0, wa2 = 0, rx = 0, ry = 0;		
	
		final double swY = (maxY-1) / ((rest==0)?level:level+1);
		final double swY6 = swY/6.0;
		
		// Phillotaxis
		// Verhaeltnis des goldenen Schnitts
		final double tau = (Math.sqrt(5)+1) / 2.0;
		final double phi = 2* Math.PI / (tau * tau);
		final double cX = maxXHalbe/Math.sqrt(number);
		final double cY = maxYHalbe/Math.sqrt(number);		
			
//System.out.println(" level="+level+"  opl="+objProLevel+"  rest = "+rest);
//System.out.println(swX+" SW "+swY+"   ==   "+maxX+" max "+maxY);
		int sumjj = 0;
		int anzObj = 0;		
		for (int i = 0; i < number; i++) {
			if (objProLevel != 0 && i % objProLevel == 0 && method!=9) {
				aktLevel++;
				if (method==8) {
					// kreis 1
					rx = aktLevel * (maxXHalbe-0.5) / level;
					ry = aktLevel * (maxYHalbe-0.5) / level;
					wa1 = 2*Math.PI/objProLevel;
				}				
				if (aktLevel==level) {
					swX = maxX / (rest+1);
					if (rest!=0) {
						wa1 = 2*Math.PI/rest;
					}
				}
				j=0;
				if (method==3) {
					vorzeichen = -1;
				}				
				if (method==4) {					
					vorzeichen = (aktLevel%2==0)?-1:1;
				}
			}
			if ( i >= nextStep && method==9) {
				aktLevel++;
				int jj = (int)Math.round((aktLevel+1)*numSL);
				sumjj += jj; 
				// kreis 2
				rx = aktLevel * (maxXHalbe-0.5) / level;
				ry = aktLevel * (maxYHalbe-0.5) / level;				
				wa2 = 2*Math.PI / jj;
				nextStep += jj;
				if (aktLevel==level) {
					wa2 = 2*Math.PI/(number-i);	
				}
			}

			if (i%level==0) {
				if (method==11) { 
					nlevel++;
					rx = nlevel * (maxXHalbe-0.5) / objProLevel;
					ry = nlevel * (maxYHalbe-0.5) / objProLevel;					
				}
				if (method==12) {
					nlevel++;
					rx = nlevel * (maxXHalbe-0.5) / objProLevel;
					ry = nlevel * (maxYHalbe-0.5) / objProLevel;					
					wa1 = i;
				}
			}
				
			switch (method) {
			case 0: // gleich
				x = swX * (0.25 + i - aktLevel * objProLevel);
				y = swY * (0.25 + aktLevel) + (((i - aktLevel * objProLevel)%2!=0)?swY/2.0:0);
				break;
			case 1: // linie
				x = swX * (0.5 + i - aktLevel * objProLevel);
				y = swY * (0.5 + aktLevel);		
				break;
			case 2: // dreieck
				x = swX * (0.5 + i - aktLevel * objProLevel);
				y = 0.75 * swY * (0.35 + aktLevel) + Math.abs(x - maxXHalbe)/2.0;
				break;
			case 3: // zickzack
				x = swX * (0.5 + i - aktLevel * objProLevel);
				y = swY * (0.25 + aktLevel);	
				if (j%3==0) {
					vorzeichen *= -1;
				}
				if (vorzeichen==-1) {
					y += j%3 * swY6;
				} else {
					y += (3-j%3) * swY6;	
				}
				break;
			case 4: // zickzack 2
				x = swX * (0.5 + i - aktLevel * objProLevel);
				y = swY * (0.25 + aktLevel);	
				if (j%3==0) {
					vorzeichen *= -1;
				}
				if (vorzeichen==-1) {
					y += j%3 * swY6;
				} else {
					y += (3-j%3) * swY6;	
				}
				break;								
			case 5: // welle
				x = swX * (0.4 + i - aktLevel * objProLevel);
				y = swY * (0.35 + aktLevel) + 0.6*Math.sin(x);				
				break;
			case 6: // doppelwelle
				x = swX * (0.4 + i - aktLevel * objProLevel);
				y = swY * (0.25 + aktLevel) + Math.abs(Math.sin(x));
				break;
			case 7: // parabel
				x = 0.05 + 0.95* swX * (0.5 + i - aktLevel * objProLevel);
				y = 0.7 * swY * (1 + aktLevel) + (x - maxXHalbe + swX/2.0)*(x - maxXHalbe+ swX/2.0) / (maxX+1);
				break;
			case 8: // kreis 1
				x = maxXHalbe + rx * Math.cos(i * wa1);
				y = maxYHalbe + ry * Math.sin(i * wa1);
				break;
			case 9: // kreis 2
				x = maxXHalbe + rx * Math.cos(i * wa2);
				y = maxYHalbe + ry * Math.sin(i * wa2);
				break;
			case 10: // phillotaxis
				wa2 = (float)(i * (phi+(level-1)/500.0));
				x = maxXHalbe + cX * Math.sqrt(i) * Math.cos(wa2);  
				y = maxYHalbe + cY * Math.sqrt(i) * Math.sin(wa2);
				break;				
			case 11: // Strahlen
				wa2 = (float)(i%level*w_n);
				x = maxXHalbe + rx * Math.cos(wa2);
				y = maxYHalbe + ry * Math.sin(wa2);
				break;
			case 12: // spriale 
				wa2 = (float)(i%level*w_n) + (3*wa1)*Math.PI/180.0;
				x = maxXHalbe + rx * Math.cos(wa2);
				y = maxYHalbe + ry * Math.sin(wa2);
				break;
			case 13: // Lissajou
				x = maxXHalbe + (maxXHalbe-0.5) * Math.cos(i * level);
				y = maxYHalbe + (maxYHalbe-0.5) * Math.sin(i * (level+2));				
				break;
			case 14: // test
				x = maxXHalbe + i/(float)number;
				y = maxYHalbe + i/(float)number;				
				break;				
			}
	
//System.out.println(i+"=i   "+x+":"+y);
				if (x>maxX-1) {
//	System.out.println("BB x> "+x);
					x = maxX-1.2;
				}
				if (y>maxY-1) {					
//	System.out.println("BB y> "+y);
					y = maxY-1.2;
				}
				if (x<0) {			
//	System.out.println("BB x< "+x);
					x = 0;
				}
				if (y<0) {
//	System.out.println("BB y< "+y);
					y = 0;
				}

			j++;
			if (checkDensityField(x,y)) {
				xxObj[anzObj] = (float)x;
				yyObj[anzObj] = (float)y;
				anzObj++;
			}
		}
		
		xx = new float[anzObj];
		yy = new float[anzObj];
		for (int i = 0; i < anzObj; i++) {
			xx[i] = xxObj[i];
			yy[i] = yyObj[i];
		}
	}

	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field method$FIELD;
	public static final Type.Field number$FIELD;
	public static final Type.Field level$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (GeometricArrange representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 3;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((GeometricArrange) o).method = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 1:
					((GeometricArrange) o).number = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 2:
					((GeometricArrange) o).level = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((GeometricArrange) o).getMethod ();
				case Type.SUPER_FIELD_COUNT + 1:
					return ((GeometricArrange) o).getNumber ();
				case Type.SUPER_FIELD_COUNT + 2:
					return ((GeometricArrange) o).getLevel ();
			}
			return super.getInt (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new GeometricArrange ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (GeometricArrange.class);
		method$FIELD = Type._addManagedField ($TYPE, "method", 0 | Type.Field.SCO, TYPE, null, Type.SUPER_FIELD_COUNT + 0);
		number$FIELD = Type._addManagedField ($TYPE, "number", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 1);
		level$FIELD = Type._addManagedField ($TYPE, "level", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 2);
		number$FIELD.setMinValue (new Integer (2));
		number$FIELD.setMaxValue (new Integer (MAX_NUMBER));
		level$FIELD.setMinValue (new Integer (1));
		level$FIELD.setMaxValue (new Integer (25));
		$TYPE.validate ();
	}

	public int getMethod ()
	{
		return method;
	}

	public void setMethod (int value)
	{
		this.method = (int) value;
	}

	public int getNumber ()
	{
		return number;
	}

	public void setNumber (int value)
	{
		this.number = (int) value;
	}

	public int getLevel ()
	{
		return level;
	}

	public void setLevel (int value)
	{
		this.level = (int) value;
	}

//enh:end

}
