/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;

public class Stucki6Dither extends ArrangeBase {
	
	public Stucki6Dither(float maxX, float maxY,
			float threshold, float maxThreshold, float[][] field) {
		final int FIELD_LENGTH = field.length;
//		threshold *= maxThreshold;

		// in Punkte umrechnen (Halftoning mittels: Error-Diffusion - 6 Nachbarn = Stucki)
		final double a = 8.0 / 24.0;
		final double b = 2.0 / 24.0;
		final double c = 2.0 / 24.0;
		final double d = 8.0 / 24.0;
		final double e = 2.0 / 24.0;
		final double f = 2.0 / 24.0;
		double error = 0.0, old = 0;
		for (int i = 0; i < FIELD_LENGTH - 2; i++) {
			for (int j = 1; j < FIELD_LENGTH - 2; j++) {
				old = field[j][i];
				if (field[j][i] < threshold) {
					field[j][i] = 0;
				} else {
					field[j][i] = maxThreshold;
				}
				error = Math.abs(old - field[j][i]);
				field[j + 1][i] += error * a;
				field[j + 2][i] += error * b;
				field[j - 1][i + 1] += error * c;
				field[j][i + 1] += error * d;
				field[j + 1][i + 1] += error * e;
				field[j][i + 2] += error * f;
			}
		}

		// berechneten Punkte uebertragen
		pointList = new PointArrayList();
		for (int i = 0; i < FIELD_LENGTH; i++) {
			for (int j = 0; j < FIELD_LENGTH; j++) {
				if (field[j][i] == maxThreshold) {
					pointList.add(new Point(i*(maxX-1)/FIELD_LENGTH, j*(maxY-1)/FIELD_LENGTH));
				}
			}
		}
		
		pointListToArrays();
	}

}