
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;

public class RandomBase extends ShareableBase 
{

	//enh:sco SCOType

	int initial = 0;
	// enh:field getter setter min=0 max=50

	float randomX = 0;
	// enh:field getter setter min=0 max=20

	float randomY = 0;
	// enh:field getter setter min=0 max=20

	float randomXY = 0;
	// enh:field getter setter min=0 max=20
	

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field initial$FIELD;
	public static final Type.Field randomX$FIELD;
	public static final Type.Field randomY$FIELD;
	public static final Type.Field randomXY$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (RandomBase representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 4;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((RandomBase) o).initial = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((RandomBase) o).getInitial ();
			}
			return super.getInt (o, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					((RandomBase) o).randomX = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 2:
					((RandomBase) o).randomY = (float) value;
					return;
				case Type.SUPER_FIELD_COUNT + 3:
					((RandomBase) o).randomXY = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					return ((RandomBase) o).getRandomX ();
				case Type.SUPER_FIELD_COUNT + 2:
					return ((RandomBase) o).getRandomY ();
				case Type.SUPER_FIELD_COUNT + 3:
					return ((RandomBase) o).getRandomXY ();
			}
			return super.getFloat (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new RandomBase ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (RandomBase.class);
		initial$FIELD = Type._addManagedField ($TYPE, "initial", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 0);
		randomX$FIELD = Type._addManagedField ($TYPE, "randomX", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 1);
		randomY$FIELD = Type._addManagedField ($TYPE, "randomY", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 2);
		randomXY$FIELD = Type._addManagedField ($TYPE, "randomXY", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 3);
		initial$FIELD.setMinValue (new Integer (0));
		initial$FIELD.setMaxValue (new Integer (50));
		randomX$FIELD.setMinValue (new Float (0));
		randomX$FIELD.setMaxValue (new Float (20));
		randomY$FIELD.setMinValue (new Float (0));
		randomY$FIELD.setMaxValue (new Float (20));
		randomXY$FIELD.setMinValue (new Float (0));
		randomXY$FIELD.setMaxValue (new Float (20));
		$TYPE.validate ();
	}

	public int getInitial ()
	{
		return initial;
	}

	public void setInitial (int value)
	{
		this.initial = (int) value;
	}

	public float getRandomX ()
	{
		return randomX;
	}

	public void setRandomX (float value)
	{
		this.randomX = (float) value;
	}

	public float getRandomY ()
	{
		return randomY;
	}

	public void setRandomY (float value)
	{
		this.randomY = (float) value;
	}

	public float getRandomXY ()
	{
		return randomXY;
	}

	public void setRandomXY (float value)
	{
		this.randomXY = (float) value;
	}

//enh:end
	
}
