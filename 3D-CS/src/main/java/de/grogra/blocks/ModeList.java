/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;

import de.grogra.xl.lang.FloatToFloat;

public class ModeList {

	public static float function(int i, int n, float value1, float value2,
			FloatToFloat func) {
		float ii = i * value1 + (n - i) * value2;
		return func.evaluateFloat(ii);
	}
	
	public static float function(int i, int n, float value1, float value2,
			FloatToFloat func, Tuple2f ids, Tuple3f nutrientsValues, Tuple2f h, float d) {
		float ii = i * value1 + (n - i) * value2;
		if (func instanceof CustomFunction) {
			((CustomFunction) func).setValues(ids, nutrientsValues, h, d);
		}
		return func.evaluateFloat(ii);
	}
	
	// nur fuer hydra fan-mode
	public static float function(int i, float delta, FloatToFloat func, 
			Tuple2f ids, Tuple3f nutrientsValues, Tuple2f h, float d) {
		float ii = i * delta;
		if (func instanceof CustomFunction) {
			((CustomFunction) func).setValues(ids, nutrientsValues, h, d);
		}
		return func.evaluateFloat(ii);
	}	

}
