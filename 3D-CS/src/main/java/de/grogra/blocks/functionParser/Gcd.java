/**
 * Gcd.java
 *
 *
 * @author:	Michael Henke
 * @date:	10.09.2006
 */
package de.grogra.blocks.functionParser;

import java.math.BigInteger;

/*
 * eine Klasse für die Berechnung gcd(left,right)
 */
public class Gcd extends BinaryExpr {

	public Gcd(Expr left, Expr right) {
		super(left, right);
	}

	public double eval() {
		BigInteger n1 = new BigInteger("" + left.eval());
		BigInteger n2 = new BigInteger("" + right.eval());
		return (n1.gcd(n2)).doubleValue();
	}

	public String toString() {
		return "gcd(" + left.toString() + ", " + right.toString() + ")";
	}
}
