/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple2f;
import javax.vecmath.Tuple3f;

import de.grogra.blocks.functionParser.parser;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;
import de.grogra.xl.lang.FloatToFloat;

public class CustomFunction extends ShareableBase implements FloatToFloat 
{
	// enh:sco SCOType

	String function = "1";
	// enh:field getter setmethod=setFunction

	float p = 1;
	// enh:field getter min=-1 max=5
	
	private String oldFunc = "1";
	private Tuple2f id = new Point2f(0, 0);
	private Tuple3f nutrientsValues = new Point3f(0, 0, 0);
	private Tuple2f h = new Point2f(0, 0);
	private float d = 0;
	
	private parser parser1 = null;
	
	private float minFV = -1000;
	private float maxFV =  1000;
	
	public CustomFunction() {
		init();
	}

	public CustomFunction(String ss) {
		setFunction(ss);
	}

	public CustomFunction(double value) {
		setFunction(""+value);
	}
	
	public CustomFunction(String ss, double minFV, double maxFV) {
		setFunction(ss);
		this.minFV = (float)minFV; 
		this.maxFV = (float)maxFV;
	}

	public CustomFunction(double value, double minFV, double maxFV) {
		setFunction(""+value);
		this.minFV = (float)minFV; 
		this.maxFV = (float)maxFV;
	}
	
	private float checkBorders(double value) {
		if (value < minFV) {
			return minFV;
		}			
		if (value > maxFV) {
			return maxFV;
		}
		return (float) value;
	}
	
	private void init() {
		parser1 = new parser();
		parser1.initParser(function + ";");
		oldFunc = function;
	}	
	
	public void setValues(Tuple2f id, Tuple3f nutrientsValues, Tuple2f h, float d) {
		this.id = id;
		this.nutrientsValues = nutrientsValues;
		this.h = h;		
		this.d = d;		
	}
	
	public float evaluateFloat(float x) {
		checkFunction ();
		return checkBorders(parser1.eval(x, id, nutrientsValues, h, p, d));
	}
	
	public float evaluateFloat() {
		checkFunction ();
		return checkBorders(parser1.eval(0, id, nutrientsValues, h, p, d));
	}
	
	public float evaluateFloat(float x, Tuple2f id, Tuple3f nutrientsValues, Tuple2f h, float d) {
		checkFunction (); 
		return checkBorders(parser1.eval(x, id, nutrientsValues, h, p, d));
	}
	
	public float evaluateFloat(Tuple2f id, Tuple3f nutrientsValues, Tuple2f h, float d) {
		checkFunction (); 
		return checkBorders(parser1.eval(0, id, nutrientsValues, h, p, d));
	}

	public float evaluateZerro() {
		checkFunction ();
		return checkBorders(parser1.eval(0, new Point2f(0,0), new Point3f(0,0,0), new Point2f(0,0), 0, 0));
	}
	
	private void checkFunction ()
	{
		if (function == null || function.equals("")) 
		{
			function = "0";
		}
		if (!function.equals(oldFunc)) {
			init();
		}
	}
	
	public void setFunction (String value)
	{
		function = value;
		checkFunction ();
	}	

	public void setFunction (double value) 
	{
		function = ""+value;
		checkFunction ();
	}	
	
	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field function$FIELD;
	public static final Type.Field p$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (CustomFunction representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 2;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setFloat (Object o, int id, float value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					((CustomFunction) o).p = (float) value;
					return;
			}
			super.setFloat (o, id, value);
		}

		@Override
		protected float getFloat (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					return ((CustomFunction) o).getP ();
			}
			return super.getFloat (o, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((CustomFunction) o).setFunction ((String) value);
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((CustomFunction) o).getFunction ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new CustomFunction ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (CustomFunction.class);
		function$FIELD = Type._addManagedField ($TYPE, "function", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, Type.SUPER_FIELD_COUNT + 0);
		p$FIELD = Type._addManagedField ($TYPE, "p", 0 | Type.Field.SCO, de.grogra.reflect.Type.FLOAT, null, Type.SUPER_FIELD_COUNT + 1);
		p$FIELD.setMinValue (new Float (-1));
		p$FIELD.setMaxValue (new Float (5));
		$TYPE.validate ();
	}

	public float getP ()
	{
		return p;
	}

	public String getFunction ()
	{
		return function;
	}

//enh:end

}
