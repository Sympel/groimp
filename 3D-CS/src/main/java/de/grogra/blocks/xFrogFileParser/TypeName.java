/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import de.grogra.imp3d.objects.Box;
import de.grogra.imp3d.objects.Cone;
import de.grogra.imp3d.objects.Cylinder;
import de.grogra.imp3d.objects.NURBSSurface;
import de.grogra.imp3d.objects.Parallelogram;
import de.grogra.imp3d.objects.Polygon;
import de.grogra.imp3d.objects.Sphere;
import de.grogra.math.Circle;
import de.grogra.math.RegularPolygon;
import de.grogra.math.SwungSurface;

public class TypeName extends Expr {

	private final String[] typeList = { "\"Undefined\"", "\"Tube\"",
			"\"Area\"", "\"Square\"", "\"Circle\"", "\"Box\"", "\"Sphere\"",
			"\"Torus\"", "\"Cylinder\"", "\"Cone\"", "\"Attractor\"",
			"\"Triangle Up\"", "\"Triangle Down\"" };

	private final String[] sImports = { 
			"import de.grogra.imp3d.objects.Null;",
			"import de.grogra.math.RegularPolygon;", 
			"import de.grogra.imp3d.objects.Parallelogram;",
			"import de.grogra.imp3d.objects.Polygon;",
			"import de.grogra.imp3d.objects.Polygon;",
			"import de.grogra.imp3d.objects.Box;",
			"import de.grogra.imp3d.objects.Sphere;",
			"import de.grogra.imp3d.objects.NURBSSurface;",
			"import de.grogra.imp3d.objects.Cylinder;",
			"import de.grogra.imp3d.objects.Cone;", "",
			"import de.grogra.imp3d.objects.Polygon;",
			"import de.grogra.imp3d.objects.Polygon;" };

	private int type = 0;

	public TypeName(int type) {
		this.type = type;
		if (aktKeyFrame == 0) {
			RegularPolygon rp = new RegularPolygon();
			if (!aktStructName.equals("\"Root\"") && type != 0) {
				if (!imports.contains(sImports[type]))
					imports.add(sImports[type]);
			}
			switch (type) {
			case 2:
				aktPrimitive = new Parallelogram();
				break;
			case 3:
				rp.setSideCount(4);
				aktPrimitive = new Polygon(rp);
				break;
			case 4:
				rp.setSideCount(30);
				aktPrimitive = new Polygon(rp);
				break;
			case 5:
				aktPrimitive = new Box();
				break;
			case 6:
				aktPrimitive = new Sphere();
				break;
			case 7:
				SwungSurface ssf = new SwungSurface();
				ssf.setTrajectory(new Circle());
				ssf.setProfile(new Circle());
				ssf.setShift(2);
				aktPrimitive = new NURBSSurface(ssf);
				break;
			case 8:
				aktPrimitive = new Cylinder();
				break;
			case 9:
				aktPrimitive = new Cone();
				break;

			case 11:
				rp.setSideCount(3);
				aktPrimitive = new Polygon(rp);
				break;
			case 12:
				rp.setSideCount(3);
				aktPrimitive = new Polygon(rp);
				break;
			default:
				aktPrimitive = new Sphere();
				break;
			}
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: type = "
					+ type);
	}

	@Override
	public String toString() {
		if (debugS)
			System.out.println("TS  " + getClass().getSimpleName());
		return typeList[type];
	}

}
