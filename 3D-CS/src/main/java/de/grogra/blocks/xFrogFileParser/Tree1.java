/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import javax.vecmath.Point2f;

import de.grogra.blocks.Tree;
import de.grogra.graph.impl.Node;
import de.grogra.math.SplineFunction;

public class Tree1 extends Expr {

	private final String treeImport = "import de.grogra.blocks.Tree;";

	private String name = "";

	public Tree1(Expr a, Expr b, Expr c, Expr d, Expr e, Expr f, Expr g,
			Expr h, Expr i, Expr j, Expr k, Expr l, Expr m, Expr n, Expr o,
			Expr p, Expr q, Expr r, Expr s, Expr t, Expr u, Expr v, Expr w, Expr x) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
		this.g = g;
		this.h = h;
		this.i = i;
		this.j = j;
		this.k = k;
		this.l = l;
		this.m = m;
		this.n = n;
		this.o = o;
		this.p = p;
		this.q = q;
		this.r = r;
		this.s = s;
		this.t = t;
		this.u = u;
		this.v = v;
		this.w = w;
		this.x = x;

		if (aktKeyFrame == 0) {
			name = new String(aktStructName);
			blocks.put(name, toXL());
			blocksGraphNodes.put(name, toGraph());
			if (!imports.contains(treeImport))
				imports.add(treeImport);
			use_GFXColor = false;
			aktTexture = null;			
		}
		
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	private SplineFunction toSplineFunction(Expr exp)
	{
		float[] xdata = ((SizeFloatTripleRow) exp).getXdata();  
		float[] ydata = ((SizeFloatTripleRow) exp).getYdata();
			
		Point2f[] data = new Point2f[xdata.length];
		for (int i = 0; i < xdata.length; i++)
		{
			data[i] = new Point2f (xdata[i], ydata[i]);
		}
		return new SplineFunction(data, SplineFunction.HERMITE);
	}
	
	private String toSplineFunctionString(Expr exp)
	{
		float[] xdata = ((SizeFloatTripleRow) exp).getXdata();  
		float[] ydata = ((SizeFloatTripleRow) exp).getYdata();

		String dataString = "new SplineFunction(new Point2f[] {";
		for (int i = 0; i < xdata.length; i++)
		{
			dataString += "new Point2f("+xdata[i]+", "+ydata[i]+")";
			if (i+1<xdata.length) {
				dataString += ", ";
			}
		}
		return dataString+"}, SplineFunction.HERMITE)";
	}
	
	private String toXL() {
		Tree obj = new Tree();
		String ss = "";
		ss += "setName(" + name + "), ";

		// anz Segmente
//		if (!((FLOAT) (a.a.a.a)).equalsF5(obj.getStemSegments()))
//			ss += "setStemSegments(" + a.a.a.a + "), ";
		
		// laenge
		if (!((FLOAT) (d.a.a.a)).equalsF5(obj.getStemLength()))
			ss += "setStemLength(" + d.a.a.a + "), ";
		// Trunk_scale
		if (!((FLOAT) (g.a.a.a)).equalsF5(obj.getTrunkScale()))
			ss += "setTrunkScale(" + g.a.a.a + "), ";
		// shape
		if (!((SplineFunction)obj.getShape()).equals(toSplineFunction(m))) {		
			ss += "setShape("+toSplineFunctionString(m)+"), ";
		}
		
		// boolean Top
		if (((Bool) (h.a)).getValue() != obj.isTop()) {
			ss += "setCrookednessAmount(" + h.a + "), ";
		}
		
		// crookednessAmount
		if (!((FLOAT) (f.a.a.a)).equalsF5(obj.getCrookednessAmount())) {
			ss += "setCrookednessAmount(" + ((FLOAT) f.a.a.a).getValue() + "), ";
		}
		
		// crookednessIntensity
		if (!((SplineFunction)obj.getCrookednessIntensity()).equals(toSplineFunction(v))) {
			ss += "setCrookednessIntensity("+toSplineFunctionString(v)+"), ";
		}
		
		// Deviate
		if (!((SplineFunction)obj.getDeviate()).equals(toSplineFunction(p))) {	
			ss += "setDeviate("+toSplineFunctionString(p)+"), ";
		}
		
		// screw
		if (!((SplineFunction)obj.getScrew()).equals(toSplineFunction(q))) {
			ss += "setScrew("+toSplineFunctionString(q)+"), ";
		}
		
		// Branches_Number
		if (!((FLOAT) (b.a.a.a)).equalsF5(obj.getBranchesNumber())) {
			ss += "setBranchesNumber(" + b.a.a.a + "), ";
		}
		// Arrangement
		if (i!=null) { 
			ss += "setArrangement(" + i.a + "), ";
		}
		// Branches_Distribution
		if (!((SplineFunction)obj.getBranchesDistribution()).equals(toSplineFunction(k))) {
			ss += "setBranchesDistribution("+toSplineFunctionString(k)+"), ";
		}

		// Branches_Growth
		if (!((SplineFunction)obj.getBranchesGrowthScale()).equals(toSplineFunction(n))) {
			ss += "setBranchesGrowthScale("+toSplineFunctionString(n)+"), ";
		}

		// Branches_Geometric
		if (!((SplineFunction)obj.getBranchesGeometricScale()).equals(toSplineFunction(o))) {
			ss += "setBranchesGeometricScale("+toSplineFunctionString(o)+"), ";
		}

		// Branches_Angle
		if (!((SplineFunction)obj.getBranchesAngle()).equals(toSplineFunction(r))) {
			ss += "setBranchesAngle("+toSplineFunctionString(r)+"), ";
		}
		
		// Branches_Dense
		if (!((SplineFunction)obj.getBranchesDense()).equals(toSplineFunction(w))) {
			ss += "setBranchesDense("+toSplineFunctionString(w)+"), ";
		}

		// Phototropism
//		if (!((SplineFunction)obj.getPhototropism()).equals(toSplineFunction(t))) {
//			ss += "setPhototropism("+toSplineFunctionString(t)+"), ";		
//		}
		
		// Gravitropism
//		if (!((SplineFunction)obj.getGravitropism()).equals(toSplineFunction(u))) {
//			ss += "setGravitropism("+toSplineFunctionString(u)+"), ";
//		}

		// farbe setzen
		if (use_GFXColor) ss += color;		
		
		// letzte komma entfernen
		if (ss.lastIndexOf(',') > 0) {
			ss = ss.substring(0, ss.length() - 2);
		}
		return "Tree().(" + ss + ")";
	}

	private Node toGraph() {
		Tree obj = new Tree();
		obj.setName(name);
//		obj.setStemSegments(((FLOAT) a.a.a.a).getValue());
		obj.setStemLength  (((FLOAT) d.a.a.a).getValue());

		// float trunkScale;
		obj.setTrunkScale(((FLOAT) g.a.a.a).getValue());
		// boolean spline
		// obj.setSpline(((Bool).a.a).getValue()); ueber w
		// FloatFunction shape
		if (!((SplineFunction)obj.getShape()).equals(toSplineFunction(m))) {		
			obj.setShape(toSplineFunction(m));
		}
		// boolean Top
		obj.setTop(((Bool) h.a).getValue());
		// float crookednessAmount
		obj.setCrookednessAmount(((FLOAT) f.a.a.a).getValue());
		// FloatFunction crookednessIntensity
		if (!((SplineFunction)obj.getCrookednessIntensity()).equals(toSplineFunction(v))) {		
			obj.setCrookednessIntensity(toSplineFunction(v));
		}
		// FloatFunction Deviate
		if (!((SplineFunction)obj.getDeviate()).equals(toSplineFunction(p))) {		
			obj.setDeviate(toSplineFunction(p));
		}
		// FloatFunction Screw
		if (!((SplineFunction)obj.getScrew()).equals(toSplineFunction(q))) {		
			obj.setScrew(toSplineFunction(q));
		}
		// float Branches_Number
		obj.setBranchesNumber(((FLOAT) b.a.a.a).getValue());
		// int Arrangement
		if (i!=null) 
			obj.setArrangement(((INT) i.a).getValue());
		// FloatFunction Branches_Distribution
		if (!((SplineFunction)obj.getBranchesDistribution()).equals(toSplineFunction(k))) {		
			obj.setBranchesDistribution(toSplineFunction(k));
		}
		// FloatFunction Branches_GrowthScale
		if (!((SplineFunction)obj.getBranchesGrowthScale()).equals(toSplineFunction(n))) {
			obj.setBranchesGrowthScale(toSplineFunction(n));
		}
		// FloatFunction Branches_GeometricScale
		if (!((SplineFunction)obj.getBranchesGeometricScale()).equals(toSplineFunction(o))) {
			obj.setBranchesGeometricScale(toSplineFunction(o));
		}
		// FloatFunction Branches_Angle
		if (!((SplineFunction)obj.getBranchesAngle()).equals(toSplineFunction(r))) {		
			obj.setBranchesAngle(toSplineFunction(r));
		}
		// FloatFunction Branches_Dense
		if (!((SplineFunction)obj.getBranchesDense()).equals(toSplineFunction(w))) {		
			obj.setBranchesDense(toSplineFunction(w));
		}
		// FloatFunction Phototropism
//		if (!((SplineFunction)obj.getPhototropism()).equals(toSplineFunction(t))) {		
//		obj.setPhototropism(toSplineFunction(t));
//		}
		// FloatFunction Gravitropism
//		if (!((SplineFunction)obj.getGravitropism()).equals(toSplineFunction(u))) {		
//		obj.setGravitropism(toSplineFunction(u));
//		}
		
		if (phong!=null && aktTexture!=null) phong.setDiffuse(aktTexture);
		if (use_GFXColor) obj.setMaterial(phong);
		return obj;
	}
	
	@Override
	public String toString() {
		return "Tree {\n" + a.toString() + "\n" + b.toString() + "\n"
				+ c.toString() + "\n" + d.toString() + "\n" + e.toString()
				+ "\n" + f.toString() + "\n" + g.toString() + "\n"
				+ h.toString() + "\n" + ((i!=null)?i.toString()+"\n":"") + j.toString()
				+ "\n" + k.toString() + "\n" + l.toString() + "\n"
				+ m.toString() + "\n" + n.toString() + "\n" + o.toString()
				+ "\n" + p.toString() + "\n" + q.toString() + "\n"
				+ r.toString() + "\n" + s.toString() + "\n" + t.toString()
				+ "\n" + u.toString() + "\n" + v.toString() + "\n"
				+ w.toString() + "\n" + "}\n";
	}

}
