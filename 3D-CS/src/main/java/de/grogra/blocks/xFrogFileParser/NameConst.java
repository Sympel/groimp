/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

public class NameConst extends Expr {

	private final String[] typeList = { "TG_NONE", "TG_SPHEREMAP", "TG_LINEAR",
			"TG_UVW", "ApplyShape", "ApplyProfil", "ApplyProfil3D", "NewAlphaDef", "Culling On", "Culling Off", 
			"Vector", "Plane", "Photo_Dir X", "Photo_Dir Y", "Photo_Dir Z",  "Reference", "Hide", "AccessInfo {\n}",
			"Simple_Triangulation", "Smooth", "spline", "Group", "" };

	private int type = 0;

	public NameConst(int type) {
		this.type = type;
		if (type>=typeList.length) {
			type = typeList.length-1;
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + type + " " + typeList[type]);		
	}

	@Override
	public String toString() {
		if (debugS)
			System.out.println("TS  " + getClass().getSimpleName());
		return typeList[type];
	}

}
