/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import java.util.StringTokenizer;

public class Branches extends Expr {

	public Branches(Expr a) {
		this.a = a;
		if (aktKeyFrame == 0) {
			String ss = a.toString().replaceAll("\n", " ");
			StringTokenizer sn = new StringTokenizer(ss);
			int anz = 0;
			while (sn.hasMoreTokens()) {
				anz++;
				sn.nextElement();
			}
			branches.put(aktStructName, anz + " " + ss);

			if (!children.containsKey(aktStructName)) {
				children.put(aktStructName, anz + " " + ss);
			} else {
				sn = new StringTokenizer((String) children.get(aktStructName));
				String ss3 = "";
				int anzA = Integer.parseInt((String) sn.nextElement());
				while (sn.hasMoreTokens()) {
					ss3 += " " + sn.nextElement();
				}
				children.remove(aktStructName);
				children.put(aktStructName, (anz + anzA) + " " + ss + ss3);
			}
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	@Override
	public String toString() {
		if (debugS)
			System.out.println("TS  " + getClass().getSimpleName());
		return "Branches {\n" + a.toString() + "\n" + "}\n";
	}

}
