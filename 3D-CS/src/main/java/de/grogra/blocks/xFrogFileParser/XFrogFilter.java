/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.xFrogFileParser;

import java.io.File;
import java.io.IOException;
import java.io.Reader;

import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ReaderSource;

public class XFrogFilter extends FilterBase implements ObjectSource {

	public XFrogFilter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(IOFlavor.NODE);
	}

	public Object getObject() throws IOException {
		Reader input = ((ReaderSource) source).getReader();
		parser p1 = new parser();		
		File f = ((FileSource) source).getInputFile ();
		p1.parseFile(input, f.getParentFile (), getRegistry());		
		return p1.toGraph();
	}

}
