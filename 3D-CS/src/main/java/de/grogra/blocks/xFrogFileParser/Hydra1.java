/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import javax.vecmath.Point2f;

import de.grogra.blocks.Hydra;
import de.grogra.graph.impl.Node;
import de.grogra.math.BSplineCurve;
import de.grogra.math.Helix;
import de.grogra.math.SplineFunction;

public class Hydra1 extends Expr {

	private final String hydraImport = "import de.grogra.blocks.Hydra;";

	private String name = "";

	public Hydra1(Expr a, Expr b, Expr c, Expr d, Expr e, Expr f, Expr g) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
		this.g = g;

		if (aktKeyFrame == 0) {
			name = new String(aktStructName);
			blocks.put(name, toXL());
			blocksGraphNodes.put(name, toGraph());
			if (!imports.contains(hydraImport))
				imports.add(hydraImport);
			use_GFXColor = false;
			aktTexture = null;
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	private String toXL() {
		Hydra obj = new Hydra();
		String ss = "";

		ss += "setName(" + name + "), ";

		// anz Segmente
		if (!((FLOAT) (a.a.a.a)).equalsF5(obj.getNumber()))
			ss += "setNumber(" + a.a.a.a + "), ";

		// trajectory
		ss += "setTrajectory(" + makeTrajectory(((FLOAT) c.a.a).getValue(), ((FLOAT) c.a.a).getValue()) + "), ";

		// Twist
		if (!((FLOAT) (d.a.a)).equalsF5(obj.getTwist2()))
			ss += "setTwist2(" + d.a.a + "), ";
		if (!((FLOAT) (d.a.b)).equalsF5(obj.getTwist1()))
			ss += "setTwist1(" + d.a.b + "), ";
		if (!obj.getTwistMode().getClass().getSimpleName().equals(
				((Functions) d.a.c).getFunction().getClass().getSimpleName()))
			ss += "setTwistMode(" + ((Functions) d.a.c).toXL() + "), ";

		// Spin
		if (!((FLOAT) (e.a.a)).equalsF5(obj.getSpin2()))
			ss += "setSpin2(" + e.a.a + "), ";
		if (!((FLOAT) (e.a.b)).equalsF5(obj.getSpin1()))
			ss += "setSpin1(" + e.a.b + "), ";
		if (!obj.getSpinMode().getClass().getSimpleName().equals(
				((Functions) e.a.c).getFunction().getClass().getSimpleName()))
			ss += "setSpinMode(" + ((Functions) e.a.c).toXL() + "), ";

		// Scale
		if (!((FLOAT) (b.a.a)).equalsF5(obj.getScale2()))
			ss += "setScale2(" + b.a.a + "), ";
		if (!((FLOAT) (b.a.b)).equalsF5(obj.getScale1()))
			ss += "setScale1(" + b.a.b + "), ";
		if (!obj.getScaleMode().getClass().getSimpleName().equals(
				((Functions) b.a.c).getFunction().getClass().getSimpleName()))
			ss += "setScaleMode(" + ((Functions) b.a.c).toXL() + "), ";

		ss += "setSlope("+toSplineFunctionString()+")), ";
		
		// farbe setzen
		if (use_GFXColor) ss += color;
		if (aktTexture!=null) ss += "setMaterial(" + aktTextureName + "), ";

		// letzte komma entfernen
		if (ss.lastIndexOf(',') > 0) {
			ss = ss.substring(0, ss.length() - 2);
		}

		return "Hydra().(" + ss + ")";
	}

	private String makeTrajectory(float a, float b) {
		String ss = "";
		String name = "trajectory"+trajectory.size();
		ss += "\tBSplineCurve "+name+" = new Helix();\n";
		ss += "\t((Helix)"+name+").setStartRadius(0);\n";
		ss += "\t((Helix)"+name+").setEndRadius(0);\n";
		ss += "\t((Helix)"+name+").setHeight(0);\n";
		ss += "\t((Helix)"+name+").setStartAngle("+Math.round(a+180)+");\n";
		ss += "\t((Helix)"+name+").setEndAngle("+Math.round(b+180)+");\n";
		trajectory.add(ss);
		return name;
	}
	
	private Node toGraph() {
		Hydra obj = new Hydra(true);
		obj.setName(name);
		obj.setNumber(((FLOAT) a.a.a.a).getValue());

		BSplineCurve trajectory = new Helix();
		((Helix)trajectory).setStartRadius(0);
		((Helix)trajectory).setEndRadius(0);
		((Helix)trajectory).setHeight(0);
		((Helix)trajectory).setStartAngle(((FLOAT) c.a.a).getValue());
		((Helix)trajectory).setEndAngle(((FLOAT) c.a.b).getValue());		
		obj.setTrajectory(trajectory);
		
		obj.setTwist2(((FLOAT) d.a.a).getValue());
		obj.setTwist1(((FLOAT) d.a.b).getValue());
		obj.setTwistMode(((Functions) d.a.c).getFunction());
		obj.setSpin2(((FLOAT) e.a.a).getValue());
		obj.setSpin1(((FLOAT) e.a.b).getValue());
		obj.setSpinMode(((Functions) e.a.c).getFunction());
		obj.setScale2(((FLOAT) b.a.a).getValue());
		obj.setScale1(((FLOAT) b.a.b).getValue());
		obj.setScaleMode(((Functions) b.a.c).getFunction());
		obj.setSlopeFunction(toSplineFunction());
		obj.setRadius(0);
		
		if (phong!=null && aktTexture!=null) phong.setDiffuse(aktTexture);
		if (use_GFXColor) obj.setMaterial(phong);
		return obj;
	}

	private SplineFunction toSplineFunction()
	{
		float[] xdata = {0, 0.5f, 1};
		float[] ydata = {0, 0, 0};
		Point2f[] data = new Point2f[xdata.length];
		for (int i = 0; i < xdata.length; i++)
		{
			data[i] = new Point2f (xdata[i], ydata[i]);
		}
		return new SplineFunction(data, SplineFunction.HERMITE);
	}	
	
	private String toSplineFunctionString()
	{
		float[] xdata = {0, 0.5f, 1};
		float[] ydata = {0, 0, 0};
		String dataString = "new SplineFunction(new Point2f[] {";
		for (int i = 0; i < xdata.length; i++)
		{
			dataString += "new Point2f("+xdata[i]+", "+ydata[i]+")";
			if (i+1<xdata.length) {
				dataString += ", ";
			}
		}
		return dataString+"})";
	}	
	
	@Override
	public String toString() {
		return "Hydra {\n" + a.toString() + "\n" + b.toString() + "\n"
				+ c.toString() + "\n" + d.toString() + "\n" + e.toString()
				+ "\n" + f.toString() + "\n" + g.toString() + "\n" + "}\n";
	}

}
