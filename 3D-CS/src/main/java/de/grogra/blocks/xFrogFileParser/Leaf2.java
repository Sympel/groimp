/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Parallelogram;

public class Leaf2 extends Expr {

	private String name = "";

	private final String l1Import = "import de.grogra.imp3d.objects.Parallelogram;";

	public Leaf2(Expr a, Expr b, Expr c, Expr d, Expr e, Expr f, Expr g,
			Expr h, Expr i, Expr j, Expr k, Expr l, Expr m, Expr n, Expr o) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
		this.g = g;
		this.h = h;
		this.i = i;
		this.j = j;
		this.k = k;
		this.l = l;
		this.m = m;
		this.n = n;
		this.o = o;
		if (aktKeyFrame == 0) {
			name = new String(aktStructName);
			blocks.put(name, toXL());
			blocksGraphNodes.put(name, toGraph());
			if (!imports.contains(l1Import))
				imports.add(l1Import);
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	private String toXL() {
		String ss = "";
		ss += "setName(" + name + "), ";

		// letzte komma entfernen
		if (ss.lastIndexOf(',') > 0) {
			ss = ss.substring(0, ss.length() - 2);
		}
		return "Parallelogram().(" + ss + ")";
	}

	private Node toGraph() {
		Parallelogram obj = new Parallelogram();
		obj.setName(name);
		
		if (phong!=null && aktTexture!=null) phong.setDiffuse(aktTexture);
		if (use_GFXColor) obj.setMaterial(phong);
		return obj;
	}

	@Override
	public String toString() {
		return "Leaf {\n" + a.toString() + "\n" + b.toString() + "\n"
				+ c.toString() + "\n" + d.toString() + "\n" + e.toString()
				+ "\n" + f.toString() + "\n" + g.toString() + "\n"
				+ h.toString() + "\n" + i.toString() + "\n" + j.toString()
				+ "\n" + k.toString() + "\n" + l.toString() + "\n"
				+ m.toString() + "\n" + n.toString() + "\n" + o.toString()
				+ "\n" + "}\n";
	}

}
