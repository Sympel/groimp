/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import java.util.Random;

import javax.vecmath.Tuple3f;

import de.grogra.graph.Instantiator;
import de.grogra.graph.impl.Node;
import de.grogra.persistence.PersistenceField;
import de.grogra.persistence.Transaction;
import de.grogra.rgg.model.Instantiation;
import de.grogra.util.EnumerationType;

public class Variation extends Node implements de.grogra.xl.modules.Instantiator<Instantiation> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5L;

	private static final EnumerationType VARIATION_TYPE = new EnumerationType(
			"variationType", Attributes.I18N, 4);	
	
	int type = 0;
	// enh:field getter type=VARIATION_TYPE

	long seed = hashCode();
	// enh:field getter setter  

	int exceptionValue = 0;
	// enh:field getter setter min=0
	
	LocationParameterBase locationParameter = new LocationParameterBase();
	// enh:field getter setter
	
	boolean initAll = false;
	// enh:field getter setter	
	
	// interne variablen, die von vater uebergeben werden
	private Tuple3f nutrientsValues;
	
	private int aktObj = 0;
	private int i = 0;
	private int jj = 1;
	private int number = -1;
	private int numberS = -1;
	private int objProSequens = 0;
	private int sumTargets = 0;
	private int[] randomValue; 

	private Random rand = null;
	
	public Variation() {
		initAttributes();
	}
		
	public Variation(int type) {
		setType(type);
		initAttributes();
	}
	
	public Variation(int type, int numberS) {
		setType(type);
		initAttributes();
		this.numberS = numberS;
	}
	
	private void initAttributes() {
		type = 0;
		seed = hashCode();
		exceptionValue = 0;
		locationParameter = new LocationParameterBase();
		aktObj = 0;
		i = 0;
		jj = 1;
		number = -1;
		numberS = -1;
		objProSequens = 0;
		sumTargets = 0;	
		
		initRandom();
	}
	
	public Instantiator getInstantiator() {
		return de.grogra.rgg.model.Instantiation.INSTANTIATOR;
	}

	public void instantiate(Instantiation state) {
		Instantiation inst = (Instantiation) state;
		
		// locationParameter-werte holen und neu berechnen
		nutrientsValues = (Tuple3f)inst.getGraphState().getObjectDefault(this, true, Attributes.LOCATIONPARAMETER, null);
		Tuple3f nutrientsSetValues = locationParameter.setLocationParameter(nutrientsValues);		
		// neue nutrientswerte setzen
		inst.getGraphState().setInstanceAttribute (Attributes.NUTRIENTS_TUPLE3F, nutrientsSetValues);
		
		if (number==-1) {			
			// anzahl der kinder
			sumTargets = getChildCount(this, BlockConst.NEXT);
			if (sumTargets==0) return;
			
			if (numberS==-1) {
				// uebergenenen werte holen
				number = (Integer)inst.getGraphState().getObjectDefault(this, true, Attributes.NUMBER, 1);
			} else {
				// ansonsten wurde die Anzahl gesetzt
				number = numberS;
			}
			
			randomValue = new int[number];
			for (int j=0; j<number; j++)
			{
				randomValue[j] = ((int)(rand.nextFloat()*100)) % sumTargets;
			}			
			objProSequens = 0;			
			objProSequens = number / sumTargets;
		}
		if (i >= number) 
		{
			i = 0;
			aktObj = 0;			
		}
		
		int randi = 0;
		if (sumTargets==1) {
			inst.producer$push();
			inst.instantiate(get(this, 0, BlockConst.NEXT));
			inst.producer$pop(null);				
		} else {
			switch (type) {
				case 0: // zufall (zufaellig: z.b. 12132131221)						
					randi = randomValue[i];
				break;
				case 1: // sequentiell (abwechselnd hintereinander: z.b. 123123123)
					randi = i % sumTargets;
				break;
				case 2: // verteilt (blockweise hintereinander: z.b. 111222333)
					randi = aktObj;
					if (jj == objProSequens) 
					{
						aktObj++;
						jj = 0;
					}
					if (randi>objProSequens) 
					{
						randi = objProSequens+1;
					}
					jj++;
				break;
				case 3: // exceptionValue (immer erstes obj, wenn exceptionValue getroffen, dann zweites: z.b. 111112111111)
					randi = ((exceptionValue % number) == i) ? 1 : 0;
				break;
			}
			if (randi<0) {				
				randi = 0;
			}
			i++;
			inst.producer$push();
			inst.instantiate(get(this, randi, BlockConst.NEXT));
			inst.producer$pop(null);	
		}
	}
	
	private void initRandom ()
	{		
		rand = new Random(seed); // zufallsgenerator
	}

	public void setNumber (int value)
	{
		numberS = value;
	}
	
	public void setType (int value)
	{
		if (value > 3) {
			type = 0;
		} else {
			type = (int) value;
		}
		
	}	
	
    // liefert das ite kind von n, das ueber eine edgeBit-kante mit n verbunden ist (kinder muessen nacheinander an n haengen)
	public static Node get (Node n, int index, int edgeBits)
	{
		if (index < 0)
		{
			return null;
		}
		Node c = getFirstChild (n, edgeBits);
		while ((c != null) && (index-- > 0))
		{
			c = getNextSibling (c, edgeBits);
		}
		return c;
	}	
	
	public static Node getFirstChild (Node n, int edgeBits)
	{
		return n.findAdjacent (false, true, edgeBits);
	}
	
	public static Node getNextSibling (Node n, int edgeBits)
	{
		return n.findAdjacent (false, true, edgeBits);
	}	
	
	// liefert die anzal kinder von n, die ueber eine edgeBit-kante mit n verbunden sind (kinder muessen nacheinander an n haengen)
	public static int getChildCount (Node n, int edgeBits)
	{
		int i = 0;
		for (Node c = getFirstChild (n, edgeBits); c != null; c = getNextSibling (c, edgeBits))
		{
			i++;
		}
		return i;
	}	
	
	public void fieldModified (PersistenceField field, int[] indices, Transaction t)
	{
		super.fieldModified (field, indices, t);
		if (!Transaction.isApplying(t))
		{
			if (field.overlaps (indices, initAll$FIELD, null))
			{
				if (isInitAll()) 
				{
					initAttributes();
					setInitAll(false);
				}
			}
			if (field.overlaps (indices, seed$FIELD, null))
			{
					initRandom();
					number = -1;
			}				
		}
	}
		
	public float getN1() {
		return nutrientsValues.x;
	}	

	public float getN2() {
		return nutrientsValues.y;
	}
	
	public float getN3() {
		return nutrientsValues.z;
	}	

	
	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field type$FIELD;
	public static final NType.Field seed$FIELD;
	public static final NType.Field exceptionValue$FIELD;
	public static final NType.Field locationParameter$FIELD;
	public static final NType.Field initAll$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (Variation.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setBoolean (Object o, boolean value)
		{
			switch (id)
			{
				case 4:
					((Variation) o).initAll = (boolean) value;
					return;
			}
			super.setBoolean (o, value);
		}

		@Override
		public boolean getBoolean (Object o)
		{
			switch (id)
			{
				case 4:
					return ((Variation) o).isInitAll ();
			}
			return super.getBoolean (o);
		}

		@Override
		public void setInt (Object o, int value)
		{
			switch (id)
			{
				case 0:
					((Variation) o).type = (int) value;
					return;
				case 2:
					((Variation) o).exceptionValue = (int) value;
					return;
			}
			super.setInt (o, value);
		}

		@Override
		public int getInt (Object o)
		{
			switch (id)
			{
				case 0:
					return ((Variation) o).getType ();
				case 2:
					return ((Variation) o).getExceptionValue ();
			}
			return super.getInt (o);
		}

		@Override
		public void setLong (Object o, long value)
		{
			switch (id)
			{
				case 1:
					((Variation) o).seed = (long) value;
					return;
			}
			super.setLong (o, value);
		}

		@Override
		public long getLong (Object o)
		{
			switch (id)
			{
				case 1:
					return ((Variation) o).getSeed ();
			}
			return super.getLong (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 3:
					((Variation) o).locationParameter = (LocationParameterBase) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 3:
					return ((Variation) o).getLocationParameter ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new Variation ());
		$TYPE.addManagedField (type$FIELD = new _Field ("type", 0 | _Field.SCO, VARIATION_TYPE, null, 0));
		$TYPE.addManagedField (seed$FIELD = new _Field ("seed", 0 | _Field.SCO, de.grogra.reflect.Type.LONG, null, 1));
		$TYPE.addManagedField (exceptionValue$FIELD = new _Field ("exceptionValue", 0 | _Field.SCO, de.grogra.reflect.Type.INT, null, 2));
		$TYPE.addManagedField (locationParameter$FIELD = new _Field ("locationParameter", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (LocationParameterBase.class), null, 3));
		$TYPE.addManagedField (initAll$FIELD = new _Field ("initAll", 0 | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 4));
		exceptionValue$FIELD.setMinValue (new Integer (0));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new Variation ();
	}

	public boolean isInitAll ()
	{
		return initAll;
	}

	public void setInitAll (boolean value)
	{
		this.initAll = (boolean) value;
	}

	public int getType ()
	{
		return type;
	}

	public int getExceptionValue ()
	{
		return exceptionValue;
	}

	public void setExceptionValue (int value)
	{
		this.exceptionValue = (int) value;
	}

	public long getSeed ()
	{
		return seed;
	}

	public void setSeed (long value)
	{
		this.seed = (long) value;
	}

	public LocationParameterBase getLocationParameter ()
	{
		return locationParameter;
	}

	public void setLocationParameter (LocationParameterBase value)
	{
		locationParameter$FIELD.setObject (this, value);
	}

//enh:end

}
