
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.NURBSSurface;
import de.grogra.math.BSplineCurve;
import de.grogra.math.BSplineSurface;
import de.grogra.math.BezierCurve;
import de.grogra.math.Circle;
import de.grogra.math.SwungSurface;
import de.grogra.rgg.Library;

/*
 * sammelklasse mit methoden, die von xl-dateien benutzt werden
 * 
 */
public final class BlockConst {

	public static final int MULTIPLY = Library.EDGE_0;
	public static final int CHILD = Library.EDGE_1;
	public static final int NEXT = Library.EDGE_2;
	
	public static final int multiply = MULTIPLY;
	public static final int child = CHILD;
	public static final int next = NEXT;
	
	public static Node createRevo(float[] xdata, float[] ydata, float[] zdata) {
		NURBSSurface obj = new NURBSSurface();
		Circle cr = new Circle();
		cr.setIntermediateArcs(3);
		BSplineSurface sf = new SwungSurface(toBSplineCurve(xdata, ydata, zdata), cr, 0, 1);
		obj.setSurface(sf);
		obj.setVisibleSides(2);
		return obj;		
	}
	
	private static BSplineCurve toBSplineCurve(float[] xdata, float[] ydata, float[] zdata)
	{
		int flength = xdata.length;
		float[] data = new float[3*flength];
		int j = 0;
		for (int i = 0; i < flength; i++)
		{			
			data[j] = xdata[i];
			j++;
			data[j] = ydata[i];
			j++;
			data[j] = zdata[i];
			j++;
		}
		return new BezierCurve (data, 3);
	}	
	
	
}
