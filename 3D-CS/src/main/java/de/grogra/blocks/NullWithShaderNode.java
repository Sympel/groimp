/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;

import de.grogra.imp3d.PickRayVisitor;
import de.grogra.imp3d.Pickable;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.Renderable;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.ShadedNull;

public class NullWithShaderNode extends ShadedNull implements Renderable, Pickable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 134275612L;

	public void pick(Object object, boolean asNode, Point3d origin,
			Vector3d direction, Matrix4d transformation,
			de.grogra.imp.PickList list) {
		PickRayVisitor.pickPoint(origin, direction, transformation, list, 8);
	}

	private static final Tuple3f ZERO = new Point3f();

	private static final Color3f COLOR = new Color3f(1, 1, 0);

	public void draw(Object object, boolean asNode, RenderState rs) {
		rs.drawPoint(ZERO, 10, COLOR, RenderState.CURRENT_HIGHLIGHT, null);
	}

	private static void initType() {
		$TYPE.addIdentityAccessor(Attributes.SHAPE);
	}

	// enh:insert initType ();
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new NullWithShaderNode ());
		initType ();
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new NullWithShaderNode ();
	}

//enh:end

}
