module m3DCS {
	exports de.grogra.blocks.xFrogFileParser;
	exports de.grogra.blocks.arrangeBlock;
	exports de.grogra.blocks;
	exports de.grogra.blocks.functionParser;
	
	requires graph;
	requires imp;
	requires imp3d;
	requires math;
	requires platform;
	requires platform.core;
	requires rgg;
	requires raytracer;
	requires utilities;
	requires vecmath;
	requires xl;
	requires xl.core;
	requires java.datatransfer;
	requires java.desktop;
	requires raskob;
	requires java.cup11b;

	opens de.grogra.blocks to utilities;
}