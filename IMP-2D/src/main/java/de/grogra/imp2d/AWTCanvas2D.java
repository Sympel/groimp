
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.imp2d;

import java.awt.*;
import java.awt.geom.*;
import javax.vecmath.*;
import de.grogra.graph.*;
import de.grogra.graph.impl.GraphManager;
import de.grogra.imp.awt.*;
import de.grogra.imp.edit.ViewSelection;
import de.grogra.math.*;
import de.grogra.vecmath.*;

public class AWTCanvas2D extends CanvasAdapter
{
	public static final Stroke STROKE_1 = new BasicStroke ();
	public static final Stroke STROKE_3 = new BasicStroke (3);

	public final Pool pool = new Pool ();

	private DrawVisitor visitor = new DrawVisitor ();


	private class DrawVisitor extends Visitor2D
	{
		private int selectionState;
		private int minPathLength;

		void init (GraphState gs, int selectionState, int minPathLength)
		{
			init (gs, gs.getGraph ().getTreePattern (),
				  ((View2D) getView ()).getCanvasTransformation ());
			this.selectionState = selectionState;
			this.minPathLength = minPathLength;
		}


		@Override
		protected void visitImpl (Object object, boolean asNode, Path path)
		{
			if (path.getNodeAndEdgeCount () - (asNode ? 0 : 1) >= minPathLength)
			{
				Object d = state.getObjectDefault
					(object, asNode, de.grogra.imp2d.objects.Attributes.SHAPE, null);
				if (d instanceof AWTDrawable)
				{
					((AWTDrawable) d).draw
						(object, asNode, AWTCanvas2D.this, transformation, selectionState);
				}
			}
		}
	}


	public AWTCanvas2D ()
	{
		initCanvas (new CanvasComponent (640, 480));
	}


	public View2D getView2D ()
	{
		return (View2D) getView ();
	}


	private int canvasWidth, canvasHeight;


	@Override
	protected void initPaint (int flags, int width, int height)
	{
		this.canvasWidth = width;
		this.canvasHeight = height;
	}


	@Override
	protected void paintScene (int flags, Graphics2D g)
	{
		g.setColor (Color.BLACK);
		g.setTransform (IDENTITY);
		g.fillRect (0, 0, canvasWidth, canvasHeight);
		visitor.init (getRenderGraphState (), 0, 0);
		for (currentLayer = getMaxLayer (); currentLayer >= getMinLayer ();
			 currentLayer--)
		{
			getView ().getGraph ().accept (null, visitor, null);
		}
	}


	@Override
	protected void paintHighlight (int flags, Graphics2D g)
	{
		if (ViewSelection.get (getView ()) == null)
		{
			System.err.println ("NULL");
			return;
		}
		ArrayPath path = new ArrayPath (getView ().getGraph ());
		ViewSelection.Entry[] s = ViewSelection.get (getView ()).getAll (-1);
		for (currentLayer = getMaxLayer (); currentLayer >= getMinLayer ();
			 currentLayer--)
		{
			for (int i = 0; i < s.length; i++)
			{
				Path p = s[i].getPath ();
				visitor.init (getRenderGraphState (), s[i].getValue (),
							  p.getNodeAndEdgeCount ());
				GraphUtils.acceptPath (p, visitor, path);
			}
		}

		de.grogra.imp.edit.Tool tool = getView ().getActiveTool ();
		if (tool != null)
		{
			visitor.init (GraphManager.STATIC_STATE, 0, 0);
			path.clear (GraphManager.STATIC);
			for (int i = 0; i < tool.getToolCount (); i++)
			{
				de.grogra.graph.impl.GraphManager
					.acceptGraph (tool.getRoot (i), visitor, path);
			}
		}
	}


	protected void finalizePaint ()
	{
	}


	public int getWidth ()
	{
		return canvasWidth;
	}


	public int getHeight ()
	{
		return canvasHeight;
	}


	private int currentLayer, minLayer = 0, maxLayer = 2;

	public int getCurrentLayer ()
	{
		return currentLayer;
	}


	public int getMinLayer ()
	{
		return minLayer;
	}


	public int getMaxLayer ()
	{
		return maxLayer;
	}


	public boolean isCurrentLayer (int layer)
	{
		return (layer == currentLayer)
			|| ((currentLayer == minLayer) && (layer < minLayer))
			|| ((currentLayer == maxLayer) && (layer > maxLayer));
	}


	private final AffineTransform transform = new AffineTransform ();

	public void setGraphicsTransform (Matrix3d t)
	{
		Math2.setAffineTransform (transform, t);
		getGraphics ().setTransform (transform);
	}


	private final GeneralPath drawPath = new GeneralPath ();

	public void draw (Shape s, Matrix3d t, Stroke stroke)
	{
		Math2.setAffineTransform (transform, t);
		PathIterator i = s.getPathIterator (transform);

		drawPath.reset ();
		drawPath.setWindingRule (i.getWindingRule ());
		drawPath.append (i, false);

		resetGraphicsTransform ();
		Graphics2D g = getGraphics ();
		g.setStroke (stroke);
		g.draw (drawPath);
	}
	
	
	@Override
	public void setColor (Color color)
	{
		getGraphics ().setColor (color);
	}
}
