package uk.ac.soton.grophysics;

import javax.vecmath.Matrix4d;
import de.grogra.graph.GraphState;
import de.grogra.graph.Path;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.Visitor3D;
import de.grogra.rgg.Library;

/**
 * Updates the GroIMP position of any objects moved by the physics engine
 * @author Paul Masters
 *
 */
public class PhysicsVisitor extends Visitor3D {

	private boolean debug = false;
	private PhysicsModel physicsModel;
	
	public void init(GraphState gs, Matrix4d transform, PhysicsModel physicsModel) {
		this.physicsModel = physicsModel;
		init(gs, gs.getGraph().getTreePattern(), transform);
	}
	
	/**
	 * Hierarchical tree walk calls this when it enters the node
	 */
	@Override
	protected void visitEnterImpl(Object object, boolean asNode, Path path) {
		//Library.println("visitEnterImpl asNode "+asNode);
		if (asNode && (object instanceof Node) ) {
//			Node node = (Node) object;
//			Library.println("Node "+node.getId()+" "+node.getName() +" has "+node.getDirectChildCount()+" children");
			MassObject massObject = physicsModel.getMassObject(object);
			if (massObject != null) {
				if (massObject.isInitialised()) {
					if (debug) Library.println("\nPhysicsVisitor: Move MassObject "+massObject.getName());
					physicsModel.updatePosition(massObject);
				} else {
					// new object
					if (debug) Library.println("\nPhysicsVisitor: New MassObject "+massObject.getName());
					physicsModel.addDeferred(massObject);
					
				}
			}
		}
	}

	/**
	 * Hierarchical tree walk calls this when it leaves the node
	 */
	@Override
	protected void visitLeaveImpl(Object object, boolean asNode, Path path) {
//		if (asNode && (object instanceof Node) ) {
//			MassObject massObject = physicsModel.getMassObject(object);
//			if (massObject != null) {
//				Library.println("Local on leave "+	getCurrentTransformation());
//			}
//		}
	}

	
    /**
     * enable debug text output
     * @param debug
     */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	
}
