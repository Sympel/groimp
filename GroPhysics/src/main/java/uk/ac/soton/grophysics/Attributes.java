package uk.ac.soton.grophysics;

import java.util.ArrayList;

import de.grogra.graph.ObjectAttribute;
import de.grogra.imp3d.IMP3D;

public class Attributes extends de.grogra.imp3d.objects.Attributes {

	public static final ObjectAttribute MASSOBJECTS
	= init (new ObjectAttribute(ArrayList.class, false, null),
			"massObjects", IMP3D.I18N);

}
