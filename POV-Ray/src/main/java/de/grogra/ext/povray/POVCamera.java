
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import java.io.*;
import javax.vecmath.*;
import de.grogra.imp3d.*;
import de.grogra.vecmath.*;

public class POVCamera extends POVAttribute
{
	public POVCamera ()
	{
		super ("camera");
	}


	@Override
	protected void exportImpl (Object object, de.grogra.imp3d.objects.SceneTreeWithShader.Leaf leaf,
							   POVExport export)
		throws IOException
	{
		Camera c = (Camera) object;
		Matrix4d m = export.pool.m4d0;
		Math2.invertAffine (c.getWorldToViewTransformation (), m);
		Vector3d right = export.pool.v3d0, up = export.pool.v3d1,
			direction = export.pool.v3d2;
		right.set (1, 0, 0);
		up.set (0, 1, 0);
		direction.set (0, 0, -1);
		m.transform (right);
		m.transform (up);
		m.transform (direction);
		POVWriter out = export.out;

		Projection p;
		if (c instanceof Camera)
		{
			p = ((Camera) c).getProjection ();
			right.scale (2 / p.getScaleX ());
			up.scale (2 / p.getScaleY ());
			if (p instanceof ParallelProjection)
			{
				out.print ("orthographic");
				out.println ();
			}
			else if (p instanceof PerspectiveProjection)
			{
				out.print ("perspective");
				out.println ();
			}
		}
		else
		{
			p = null;
		}
		out.print ("location ");
		out.print ((float) m.m03, (float) m.m13, (float) m.m23);
		out.println ();
		out.print ("right ");
		out.print (right);
		out.println ();
		out.print ("up ");
		float aspect = export.getMetaData (POVExport.ASPECT, -1f);
		if (aspect > 0)
		{
			out.print (1 / aspect);
		}
		else
		{
			out.write ("(image_height/image_width)");
		}
		out.write ('*');
		out.print (up);
		out.println ();
		out.print ("direction ");
		out.print (direction);
		out.println ();
		if (p instanceof PerspectiveProjection)
		{
			out.print ("angle ");
			out.print (((PerspectiveProjection) p).getFieldOfView ()
					   * (180 / (float) Math.PI));
			out.println ();
		}
	}

}
