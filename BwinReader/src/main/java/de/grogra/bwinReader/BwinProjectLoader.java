/*
 * Copyright (C) 2020 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.bwinReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ProjectLoader;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.vfs.MemoryFileSystem;

public class BwinProjectLoader extends FilterBase implements ObjectSource, ProjectLoader {
	public BwinProjectLoader(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(IOFlavor.PROJECT_LOADER);
	}

	private MemoryFileSystem fs;
	
	@Override
	public Object getObject() {
		fs = new MemoryFileSystem (IO.PROJECT_FS);
		return this;
	}

	@Override
	public void loadRegistry(Registry r) {
		r.initFileSystem (fs);

		try {
			File f = ((FileSource) source).getInputFile();
			String newFileName = IO.toSimpleName(f.getName()) + ".rgg";
			Object file = fs.create (fs.getRoot (), newFileName, false);
			Writer out = fs.getWriter (file, false);
			Reader in = new BufferedReader (new FileReader(f));
			
			Parser.parseToTempFile(in, out);

			SourceFile sf = new SourceFile(newFileName, 
					IO.getMimeType(newFileName),
					IO.toSystemId(fs, file));
			r.getDirectory("/project/objects/files", null).addUserItem(sf);
			Workbench w = Workbench.get(r);
			if (w != null) {
				Item i = item.getItem("layout");
				if (i != null) {
					i = i.resolveLink(r);
				}
				if (i != null) {
					w.setProperty(Workbench.INITIAL_LAYOUT, i.getAbsoluteName());
					sf.showLater(w);
				}
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void loadGraph(Registry r) {
		r.setEmptyGraph();
	}
	
}