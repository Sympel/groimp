{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = { self, nixpkgs }: 
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        config.allowUnfree = true;
      };

      version = "2.0";

      buildInputs = with pkgs; [ jdk11 libxslt xorg.libXxf86vm steam-run ];

      nativeBuildInputs = with pkgs; [ makeWrapper maven ];

      runGroIMP = pkgs.writeShellScriptBin "run"
        ''
        steam-run java -Xverify:none -jar app/platform.core-*-jar-with-dependencies.jar
        '';

    in {
      devShells.x86_64-linux.default = pkgs.mkShell {
        inherit buildInputs;

        nativeBuildInputs = nativeBuildInputs ++ [
          runGroIMP
        ];

        shellHook = ''
          export LD_LIBRARY_PATH=${pkgs.xorg.libXxf86vm}/lib:$LD_LIBRARY_PATH
        '';
      };
    };
}
