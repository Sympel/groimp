module xl.core {
	exports org.objectweb.asm.signature;
	exports de.grogra.reflect;
	exports de.grogra.xl.lang;
	exports org.objectweb.asm;
	exports de.grogra.xl.util;

	requires java.desktop;
}