module texGen {
	exports de.grogra.texgen;

	requires graph;
	requires imp;
	requires utilities;
	requires xl.core;
	requires platform;
	requires java.desktop;
}