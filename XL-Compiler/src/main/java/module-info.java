module xl.compiler {
	exports de.grogra.xl.compiler;
	exports de.grogra.xl.compiler.scope;
	exports de.grogra.xl.parser;
	exports de.grogra.xl.expr;
	exports de.grogra.xl.compiler.pattern;

	requires grammar;
	requires graph;
	requires utilities;
	requires xl;
	requires xl.core;
	requires xl.vmx;
	requires antlr;
	requires java.desktop;

	opens de.grogra.xl.compiler to xl.core, utilities;
	opens de.grogra.xl.expr to utilities;
}
