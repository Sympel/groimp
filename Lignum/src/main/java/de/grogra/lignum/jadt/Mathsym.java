/*
 * Copyright (C) 2016 GroIMP Developer Team
 *
 * Department Ecoinformatics, Biometrics and Forest Growth,
 * University of Göttingen, Germany
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package de.grogra.lignum.jadt;

import static java.lang.Math.pow;

/**
 * Translated from orignal C++ Lignum code.
 *
 * @author Alexander Brinkmann
 */
public interface Mathsym {

	static double R_EPSILON = 1 * pow(10, -20);
	static double K_EPSILON = 1 * pow(10, -15);
	static double PI_DIV_2 = 1.57079632679489661923;
	static int HIT_THE_FOLIAGE = 1;
	static int NO_HIT = 0;
	static int HIT_THE_WOOD = -1;
	static double R_HUGE = 1.0 * pow(10, 20);
}
