/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ray2.tracing;

import java.util.ArrayList;

import de.grogra.ray.physics.Interior;
import de.grogra.ray.physics.Spectrum;
import de.grogra.ray2.Scene;
import de.grogra.vecmath.geom.Intersection;
import de.grogra.vecmath.geom.Volume;

public class EnteredSolidsList extends ArrayList<Volume>
{
	public EnteredSolidsList()
	{
	}

	public EnteredSolidsList(int capacity)
	{
		super(capacity);
	}

	public static double getIOR (Scene scene, Volume v, Spectrum spectrum)
	{
		Interior i = scene.getInterior (v);
		return (i != null) ? i.getIndexOfRefraction (spectrum) : 1;
	}

	public double getIOR (Scene scene, Intersection is, Spectrum spec)
	{
		switch (is.type)
		{
			case Intersection.PASSING:
				return 1;
			case Intersection.ENTERING:
				double outer = isEmpty () ? 1 : getIOR (scene, get (size () - 1), spec);
				double inner = getIOR (scene, is.solid, spec);
				return outer / inner;
			case Intersection.LEAVING:
				int s = size ();
				if (s == 0)
				{
					return 1;
				}
				if (get (s - 1) != is.solid)
				{
					return 1;
				}
				outer = (s == 1) ? 1 : getIOR (scene, get (s - 2), spec);
				inner = getIOR (scene, is.solid, spec);
				return outer / inner;
			default:
				throw new AssertionError ();
		}
	}

	public int record (Intersection is, boolean reflected)
	{
		switch (is.type)
		{
			case Intersection.PASSING:
				return -2;
			case Intersection.ENTERING:
				if (reflected)
				{
					return -2;
				}
				add (is.solid);
				return -1;
			case Intersection.LEAVING:
				if (reflected)
				{
					return -2;
				}
				int i = lastIndexOf (is.solid);
				if (i >= 0)
				{
					remove (i);
					return i;
				}
//				System.err.println ("Volume " + is.solid + " not found");
				return -2;
			default:
				throw new AssertionError ();
		}
	}

	public void unrecord (Intersection is, int index)
	{
		if (index >= 0)
		{
			add (index, is.solid);
		}
		else if (index == -1)
		{
			remove (size () - 1);
		}
	}
}
