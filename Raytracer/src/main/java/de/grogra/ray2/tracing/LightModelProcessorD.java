/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ray2.tracing;

import java.util.Random;

import javax.vecmath.Color3f;
import javax.vecmath.Vector3f;

import de.grogra.ray.physics.Collector;
import de.grogra.ray.physics.Environment;
import de.grogra.ray.physics.Light;
import de.grogra.ray.physics.Sensor;
import de.grogra.ray.physics.Shader;
import de.grogra.ray.physics.Spectrum;
import de.grogra.ray.util.Ray;
import de.grogra.ray.util.RayList;
import de.grogra.ray2.ProgressMonitor;
import de.grogra.ray2.Scene;
import de.grogra.vecmath.geom.Intersection;
import de.grogra.vecmath.geom.IntersectionList;
import de.grogra.vecmath.geom.Line;
import de.grogra.xl.util.IntList;
import de.grogra.xl.util.ObjectList;
import net.goui.util.MTRandom;

/**
 * Same as LightModelProcessor but in addition allows to compute values for each
 * recursion depth individually.
 * 
 * @author MH
 *
 */
public class LightModelProcessorD extends RadiationModel implements Cloneable {
	private int depth = 0;
	
	private Scene scene;

	private RayList rays;
	private Environment env;
	private Vector3f out;
	private Line line;

	private ObjectList<ObjectList<Spectrum>> reflectedPower;
	private ObjectList<ObjectList<Spectrum>> absorbedPower;
	private ObjectList<ObjectList<Spectrum>> transmittedPower;
	private ObjectList<ObjectList<Spectrum>> radiantPower;
	private ObjectList<ObjectList<Spectrum>> sensedIrradiance;
	private ObjectList<IntList> hitCounter;

	private Spectrum tmpSpectrum0;
	private Spectrum tmpSpectrum1;
	private Spectrum tmpSpectrum2;
	private Spectrum tmpSpectrum3;

	private IntersectionList ilist;
	private EnteredSolidsList enteredSolids;

	private double minPower;
	private int maxDepth;

	/**
	 * Implementation groups rays into bundles of <code>bundleSize</code> rays which
	 * are computed at once.
	 */
	private final int bundleSize;

	/**
	 * Create a new light model processor that uses the given spectrum factory.
	 * 
	 * @param scene
	 */
	public LightModelProcessorD(Scene scene, int[] idToGroup) {
		this(scene, 0, new ObjectList<ObjectList<Spectrum>>(), new ObjectList<ObjectList<Spectrum>>(), new ObjectList<ObjectList<Spectrum>>(),
				new ObjectList<ObjectList<Spectrum>>(), new ObjectList<ObjectList<Spectrum>>(), idToGroup, 1000, null, new ObjectList<IntList>());
	}

	/**
	 * Create a new light model processor that uses the given spectrum factory and
	 * adds collected radiation values to the lists.
	 * 
	 * @param scene               a scene
	 * @param radiantPowerSum
	 * @param absorbedPowerSum
	 * @param reflectedPowerSum
	 * @param transmittedPowerSum
	 * @param sensedIrradianceSum
	 * @param rays                for debugging purposes, the traced rays will be
	 *                            added to the list. Use null if this is not needed
	 */
	public LightModelProcessorD(Scene scene, int depth, ObjectList<ObjectList<Spectrum>> radiantPowerSum,
			ObjectList<ObjectList<Spectrum>> absorbedPowerSum, ObjectList<ObjectList<Spectrum>> reflectedPowerSum,
			ObjectList<ObjectList<Spectrum>> transmittedPowerSum, ObjectList<ObjectList<Spectrum>> sensedIrridiancSum, int[] idToGroup,
			int bundleSize, ObjectList<ObjectList<RayPoint>> rays, ObjectList<IntList> hitCounterSum) {
		super(scene.createSpectrum(), radiantPowerSum, absorbedPowerSum, reflectedPowerSum, transmittedPowerSum,
				sensedIrridiancSum, idToGroup, rays, hitCounterSum);
		this.depth = depth;
		this.scene = scene;
		this.bundleSize = bundleSize;
		initLocals();
	}

	protected void initLocals() {
		rays = new RayList(black);
		tmpSpectrum0 = black.newInstance();
		tmpSpectrum1 = black.newInstance();
		tmpSpectrum2 = black.newInstance();
		tmpSpectrum3 = black.newInstance();
		out = new Vector3f();
		line = new Line();
		env = new Environment(scene.getBoundingBox(), black, Environment.RADIATION_MODEL);
		reflectedPower = new ObjectList<ObjectList<Spectrum>>();
		absorbedPower = new ObjectList<ObjectList<Spectrum>>();
		transmittedPower = new ObjectList<ObjectList<Spectrum>>();
		radiantPower = new ObjectList<ObjectList<Spectrum>>();
		sensedIrradiance = new ObjectList<ObjectList<Spectrum>>();
		hitCounter = new ObjectList<IntList>();

		ilist = new IntersectionList();
		enteredSolids = new EnteredSolidsList();
	}

	public LightModelProcessorD dup(Scene scene) {
		try {
			LightModelProcessorD p = (LightModelProcessorD) super.clone();
			p.scene = scene;
			p.initLocals();
			return p;
		} catch (CloneNotSupportedException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public void compute(long rayCount, long seed, ProgressMonitor progress, int maxDepth, double minPower) {
		final Light[] lights = scene.getLights();
		final int lightCount = lights.length;

		if (lightCount == 0) {
			return;
		}

		this.maxDepth = maxDepth;

		// continue if there is light in the scene
		Environment e = new Environment(scene.getBoundingBox(), black, Environment.RADIATION_MODEL);

		// collect the power of the light sources
		final double[] powerOfLight = new double[lightCount];
		double totalPower = 0;
		for (int i = 0; i < lightCount; i++) {
			powerOfLight[i] = lights[i].getTotalPower(e);
			totalPower += powerOfLight[i];
		}
		if (!(totalPower > 0)) {
			return;
		}

		rayCount = (rayCount + bundleSize - 1) / bundleSize;

		// calculate how many rays originate from each light source
		final long[] raysFromLight = new long[lightCount];
		long totalRays = 0;
		for (int i = 0; i < lightCount; i++) {
			raysFromLight[i] = (long) (rayCount * powerOfLight[i] / totalPower);
			totalRays += raysFromLight[i];
			// System.err.println ("light: " + i + " rays: "
			// + raysFromLight[i]);
		}
		// System.err.println ("totalRays: " + totalRays + " rayCount: "
		// + rayCount);

		MTRandom rnd = new MTRandom(seed);
		// randomly distribute remaining rays (because of rounding)
		while (rayCount > totalRays) {
			int light = rnd.nextInt(lightCount);
			if (powerOfLight[light] > 0) {
				raysFromLight[light]++;
				totalRays++;
			}
		}
		// System.err.println ("totalRays: " + totalRays + " rayCount: "
		// + rayCount);

		double[] lightFactors = new double[lightCount];
		long[] seedOffsets = new long[lightCount];
		for (int i = 0; i < lightCount; i++) {
			lightFactors[i] = 1.0 / raysFromLight[i];
			seedOffsets[i] = (seed * lightCount + i) * rayCount;
		}
		computeImpl(raysFromLight, seedOffsets, lightFactors, rnd, progress, maxDepth, minPower);
	}

	void computeImpl(long[] raysFromLight, long[] seedOffsets, double[] lightFactors, Random rnd,
			ProgressMonitor progress, int maxDepth, double minPower) {
		this.minPower = minPower;
		this.maxDepth = maxDepth;

		final Light[] lights = scene.getLights();
		final int lightCount = lights.length;

		// these rays will be created by light.generateRandomOrigins
		final RayList orays = new RayList(black);
		orays.setSize(bundleSize);

		// these rays (in fact a single ray) will be created by light.generateRandomRays
		final RayList drays = new RayList(black);
		drays.setSize(1);

		Environment e = new Environment(scene.getBoundingBox(), black, Environment.RADIATION_MODEL);

		long rayCount = 0;
		for (int l = 0; l < lightCount; l++) {
			rayCount += raysFromLight[l];
		}

		// create lots of rays through the scene
		long time0 = System.currentTimeMillis();
		long r = 0;
		for (int l = 0; l < lightCount; l++) {
			// number of pending rays for light source #l
			long raysToCreate = raysFromLight[l];
			// create rays for light source #l
			for (long lr = 0; lr < raysToCreate; lr++) {
				r++;

				rnd.setSeed(seedOffsets[l] + lr);

				// generate a random ray from that light source
				e.localToGlobal.set(scene.getLightTransformation(l));
				e.globalToLocal.set(scene.getInverseLightTransformation(l));
				Light light = lights[l];

				// create the set of initial rays (in fact just their origins)
				light.generateRandomOrigins(e, orays, rnd);

				// for each initial origin, complete the ray by choosing a direction
				for (int j = 0; j < bundleSize; j++) {
					drays.rays[0].origin.set(orays.rays[j].origin);
					light.generateRandomRays(e, null, orays.rays[j].spectrum, drays, true, rnd);

					// trace where the ray goes and update lighting conditions
					// for each object it hits on its way
					ilist.clear();
					ObjectList<RayPoint> rayInfo = null;
					if (tracedRays != null) {
						RayPoint rp = new RayPoint();
						rp.point = new Vector3f(drays.rays[0].origin);
						rp.color = new Color3f();
						drays.rays[0].spectrum.get(rp.color);
						rayInfo = new ObjectList<RayPoint>();
						rayInfo.add(rp);
						synchronized (tracedRays) {
							tracedRays.add(rayInfo);
						}
					}
					traceRay(0, drays.rays[0], null, rnd, lightFactors[l], rayInfo);
				}

				if (progress != null) {
					// display the progress if:
					// - it was the last ray
					// - 500ms passed since the last display
					long time1 = System.currentTimeMillis();
					if (r >= rayCount || (time1 - time0 > 500)) {
						progress.setProgress("Ray " + (r * bundleSize) + " of " + (rayCount * bundleSize),
								(float) r / (float) rayCount);
						time0 = time1;
					}
				}

			}

			if (raysFromLight[l] > 0) {
				// collect the light for light source #l
				addAndClearD(lightFactors[l] * (1.0 / bundleSize), radiantPower, radiantPowerSumD);
				addAndClearD(lightFactors[l] * (1.0 / bundleSize), absorbedPower, absorbedPowerSumD);
				addAndClearD(lightFactors[l] * (1.0 / bundleSize), transmittedPower, transmittedPowerSumD);
				addAndClearD(lightFactors[l] * (1.0 / bundleSize), reflectedPower, reflectedPowerSumD);
				addAndClearD(lightFactors[l] * (1.0 / bundleSize), sensedIrradiance, sensedIrradianceSumD);
				addAndClearD(hitCounter, hitCounterSumD);

			}
		}
	}

	/**
	 * Perform recursive trace of the path of the light ray. The recursion ends when
	 * depth reaches this.maxDepth or when the light energy of the ray falls below a
	 * given threshold.
	 * 
	 * @param depth
	 * @param ray
	 * @param is
	 */
	void traceRay(int depth, Ray ray, Intersection is, Random rnd, double lightFactor, ObjectList<RayPoint> rayInfo) {
		// System.err.println ("origin: " + ray.origin + " direction: "
		// + ray.direction + " spectrum: " + ray.spectrum);

		// convert the ray into a line for intersection calculation
		// conversion between Tuple3d and Tuple3f is needed
		line.origin.set(ray.getOrigin());
		line.direction.set(ray.getDirection());
		line.start = 0.00001; // TODO
		line.end = java.lang.Double.POSITIVE_INFINITY;
		Spectrum spectrum = tmpSpectrum0;
		spectrum.set(ray.spectrum);

		tmpSpectrum3.set(ray.spectrum);
		tmpSpectrum3.clampMinZero();

		int ilistSize = ilist.size;
		// compute first intersection between ray and scene
		scene.computeIntersections(line, Intersection.CLOSEST, ilist, is, null);

		// check if an intersection was found
		if (ilist.size > ilistSize) {
			// there should be just one intersection
			Intersection intersection = ilist.elements[ilistSize];
			// check if no sky object was hit (parameter < INF)
			if (intersection.parameter < Double.POSITIVE_INFINITY) {
				// obtain the volume index
				int volumeIndex = idToGroup[intersection.volume.getId()];

				// prepare the direction of the outgoing ray
				out.set(line.direction);
				out.negate();

				double invMult = 1d / intersection.multiplicity;

				int sensorID = scene.getSensor(intersection.volume);
				if (sensorID >= 0) {
					Sensor s = scene.getSensors()[sensorID];
					rays.setSize(1);
					env.set(intersection, s.getFlags(), scene);
					s.computeExitance(env, tmpSpectrum1);
					s.computeBSDF(env, null, tmpSpectrum1, out, true, tmpSpectrum2);
					tmpSpectrum2.mul(spectrum);
					tmpSpectrum2.scale(invMult);
					ObjectList<Spectrum> sColD = sensedIrradiance.get(volumeIndex);
					Spectrum sCol;
					if (sColD == null) {
						sColD = new ObjectList<Spectrum>();
					}
					sCol = sColD.get(depth);
					if (sCol == null) {
						// Creates a new Collector-instance for a usual object.
						sCol = tmpSpectrum2.clone ();
						sColD.set(depth, sCol);
						sensedIrradiance.set(volumeIndex, sColD);
					} else {
						// Add the spectrum to an already existing spectrum
						sCol.add(tmpSpectrum2);
					}
					
					if (sCol instanceof Collector) {
						((Collector) sCol).setAsCollector();
						((Collector) sCol).addToStatistic(line.direction, tmpSpectrum2, lightFactor, depth == 0);
					}
				}

				// obtain the shader of the volume that was intersected
				Shader shader = scene.getShader(intersection.volume);

				if (shader == null) {
					// no shader: continue ray without change
					rays.setSize(1);
					Ray newRay = rays.rays[0];
					newRay.direction.set(line.direction);
					newRay.spectrum.set(spectrum);
				} else {
					// set the local environment
					env.set(intersection, shader.getFlags(), scene);
					env.iorRatio = (float) enteredSolids.getIOR(scene, intersection, spectrum);

					// calculate the new ray direction
					rays.setSize(1);
					shader.generateRandomRays(env, out, spectrum, rays, true, rnd);
				}

				tmpSpectrum2.set(spectrum);
				tmpSpectrum2.scale(invMult);

				// Add the received light to the volume that was hit, but only if this is the
				// first time that the current ray hits the volume.
				// Otherwise a ray could contribute more than once to the same volume, e.g. in
				// case of transparency or mirrors. So the idea
				// is that radiantPower collects the power which would be absorbed by the volume
				// if it was completely black (so no secondary
				// rays would be created).
				boolean firstHit = true;
				for (int i = 0; i < ilistSize; ++i) {
					if (idToGroup[ilist.elements[i].volume.getId()] == volumeIndex) {
						firstHit = false;
						break;
					}
				}
				ObjectList<Spectrum> colD;
				Spectrum col;
				if (firstHit) {
					colD = radiantPower.get(volumeIndex);
					if (colD == null) {
						colD = new ObjectList<Spectrum>();
					}
					col = colD.get(depth);
					if (col == null) {
						// Creates a new Collector-instance for a usual object.
						col = tmpSpectrum2.clone ();
						colD.set(depth, col);
						radiantPower.set(volumeIndex, colD);
					} else {
						// Add the spectrum to an already existing spectrum
						col.add(tmpSpectrum2);
					}
					
					if (col instanceof Collector) {
						((Collector) col).setAsCollector();
						((Collector) col).addToStatistic(line.direction, tmpSpectrum2, lightFactor, depth == 0);
					}
				}

				// obtain the new ray and set its origin (not done by generateRandomRays)
				Ray newRay = rays.lastRay();
				newRay.origin.set(intersection.getPoint());

				spectrum.sub(newRay.spectrum);

				tmpSpectrum2.set(spectrum);
				tmpSpectrum2.scale(invMult);

				// add the absorbed part of the light to the volume that was hit
				colD = absorbedPower.get(volumeIndex);
				if (colD == null) {
					colD = new ObjectList<Spectrum>();
				}
				col = colD.get(depth);
				if (col == null) {
					// Creates a new Collector-instance for a usual object.
					col = tmpSpectrum2.clone ();
					colD.set(depth, col);
					absorbedPower.set(volumeIndex, colD);
				} else {
					// Add the spectrum to an already existing spectrum
					col.add(tmpSpectrum2);
				}
				
				if (col instanceof Collector) {
					((Collector) col).setAsCollector();
					((Collector) col).addToStatistic(line.direction, tmpSpectrum2, lightFactor, depth == 0);
				}

				// count the ray to the volume that was hit
				IntList hc = hitCounter.get(volumeIndex);
				if(hc==null) {
					hc = new IntList();
				}
				int counter = hc.get(depth);
				// increase the hit counter for an existing objects
				hc.set(depth, counter + 1);
				hitCounter.set (volumeIndex , hc);

				if (newRay.reflected) {
					tmpSpectrum2.set(newRay.spectrum);
					tmpSpectrum2.scale(invMult);
					// add the reflected part of the light to the volume that was hit					
					colD = reflectedPower.get(volumeIndex);
					if (colD == null) {
						colD = new ObjectList<Spectrum>();
					}
					col = colD.get(depth);
					if (col == null) {
						// Creates a new Collector-instance for a usual object.
						col = tmpSpectrum2.clone ();
						colD.set(depth, col);
						reflectedPower.set(volumeIndex, colD);
					} else {
						// Add the spectrum to an already existing spectrum
						col.add(tmpSpectrum2);
					}
					
					if (col instanceof Collector) {
						((Collector) col).setAsCollector();
						((Collector) col).addToStatistic(line.direction, tmpSpectrum2, lightFactor, depth == 0);
					}
				} else {
					// add the transmitted part of the light to the volume that was hit
					colD = transmittedPower.get(volumeIndex);
					if (colD == null) {
						colD = new ObjectList<Spectrum>();
					} 
					col = colD.get(depth);
					if (col == null) {
						// Creates a new Collector-instance for a usual object.
						col = tmpSpectrum2.clone ();
						colD.set(depth, col);
						transmittedPower.set(volumeIndex, colD);
					} else {
						// Add the spectrum to an already existing spectrum
						col.add(tmpSpectrum2);
					}
					
					if (col instanceof Collector) {
						((Collector) col).setAsCollector();
						((Collector) col).addToStatistic(line.direction, tmpSpectrum2, lightFactor, depth == 0);
					}
				}

				if (rayInfo != null) {
					RayPoint rp = new RayPoint();
					rp.point = new Vector3f(newRay.origin);
					rp.color = new Color3f();
					newRay.spectrum.get(rp.color);
					rayInfo.add(rp);
				}

				// check if radiance power is big enough for recursion
				if ((depth < this.maxDepth) && newRay.spectrum.integrate() > minPower) {
					// prevent the ray from staying at the same position
					// e.g. if two spheres of the same size are at the same position
					// intersection.line.start += 0.00001;

					// recursive descent
					int i = enteredSolids.record(intersection, newRay.reflected);
					traceRay(depth + 1, newRay, intersection, rnd, lightFactor, rayInfo);
					enteredSolids.unrecord(intersection, i);
				}
			}
			ilist.setSize(ilistSize);
		}
	}

	public int getBundleSize() {
		return bundleSize;
	}
}
