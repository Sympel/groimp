/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * The Growth Panel including 3 sub-panels: source panel,sink panel, allometry
 * panel
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabGrowthPanel extends JPanel {

	private static final long serialVersionUID = -2073432007086110708L;
	private JTabbedPane jtp;
	private GreenLabGrowthAllometryPanel gap;
	private GreenLabGrowthSinkPanel gsp;
	private GreenLabGrowthSourcePanel grp;

	GreenLabGrowthPanel() {
		jtp = new JTabbedPane();
		gap = new GreenLabGrowthAllometryPanel();
		gsp = new GreenLabGrowthSinkPanel();
		grp = new GreenLabGrowthSourcePanel();
		jtp.add(grp, GreenLabGUI.getString("Source"));
		jtp.add(gsp, GreenLabGUI.getString("Sink"));
		jtp.add(gap, GreenLabGUI.getString("Allometry"));

		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(jtp, BorderLayout.NORTH);
		JPanel mainmPanel = new JPanel(new BorderLayout());
		mainmPanel.add(mainPanel, BorderLayout.WEST);
		this.setLayout(new BorderLayout());
		this.add(mainmPanel, BorderLayout.NORTH);
	}

	public void activeTab(int n) {
		gap.activeTab(n);
		gsp.activeTab(n);
		grp.activeTab(n);
	}

	public void updateValue() {
		gap.updateValue();
		gsp.updateValue();
		grp.updateValue();
	}
}
