/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab.ui.elements;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComboBox;

import de.grogra.greenlab.GreenLabGUI;

/**
 * The JComboBox for choosing constant or variable
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabConstComboBox extends JComboBox implements FocusListener {

	private static final long serialVersionUID = -8362511955311051986L;
	private int startPosition = 0;
	private String keyString = "defaultTooltip";

	public GreenLabConstComboBox(String keyString, String[] strs) {
		super(strs);
		this.keyString = keyString;
		addFocusListener(this);
		final String key = this.keyString;
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				GreenLabGUI.tooltipPanel.updateValue(key);
			}
		});
	}

	public GreenLabConstComboBox(String keyString, String[] strs, int n) {
		super(strs);
		this.keyString = keyString;
		startPosition = n;
		addFocusListener(this);
		final String key = this.keyString;
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				GreenLabGUI.tooltipPanel.updateValue(key);
			}
		});
	}

	public void setText(int i) {
		this.setSelectedIndex(i - startPosition);
	}

	public int getPos() {
		return this.getSelectedIndex() + startPosition;
	}

	public String getText() {
		return (this.getSelectedIndex() + startPosition) + "";
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		GreenLabGUI.tooltipPanel.updateValue(keyString);
	}

	@Override
	public void focusLost(FocusEvent arg0) {
		// GreenLabGUI.tooltipPanel.updateValue("emptyTooltip");
	}

}
