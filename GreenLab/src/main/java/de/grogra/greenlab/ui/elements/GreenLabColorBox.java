/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab.ui.elements;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.grogra.greenlab.GreenLabGUI;
import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;

/**
 * The Color Chooser including a label to display color and a button to open a
 * color chooser to select color
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabColorBox extends JPanel {

	private static final long serialVersionUID = -5954356931969610472L;
	private JLabel colorLabel;
	private JButton colorButton;
	private String key;
	private int pos, r, g, b;
	private JColorChooser colorChooser;
	private JFrame colorFrame;
	private String keyString = "defaultTooltip";

	public GreenLabColorBox(String keyString, String p, int n) {
		this.keyString = keyString;
		key = p;
		pos = n;
		colorButton = new JButton(GreenLabGUI.getString("Change"));
		colorLabel = new JLabel("oo");
		colorLabel.setOpaque(true);
		colorChooser();
		final String ks = this.keyString;
		colorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				GreenLabGUI.tooltipPanel.updateValue(ks);
				colorFrame.setVisible(true);
			}
		});
		this.add(colorLabel);
		this.add(colorButton);

		final String key = this.keyString;
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				GreenLabGUI.tooltipPanel.updateValue(key);
			}
		});
		update();
	}

	private void colorChooser() {
		colorChooser = new JColorChooser(new Color(r, g, b));
		colorFrame = new JFrame(GreenLabGUI.getString("Color_Chooser"));
		colorFrame.setVisible(false);
		JPanel colorChooserMainPanel = new JPanel(new BorderLayout());
		colorFrame.add(colorChooserMainPanel);
		colorChooserMainPanel.add(colorChooser, BorderLayout.CENTER);
		JPanel optionPanel = new JPanel();
		JButton confirm = new JButton(GreenLabGUI.getString("Confirm"));
		JButton cancel = new JButton(GreenLabGUI.getString("Cancel"));
		cancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				colorFrame.setVisible(false);

			}
		});
		confirm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				colorFrame.setVisible(false);
				colorLabel.setBackground(colorChooser.getColor());
				colorLabel.setForeground(colorChooser.getColor());
				r = colorChooser.getColor().getRed();
				g = colorChooser.getColor().getGreen();
				b = colorChooser.getColor().getBlue();
				String[] v = GreenLabPropertyFileReader.get().getProperty(key)
						.split("\\s+");
				String value = "";
				DecimalFormat df = new DecimalFormat("0.0");
				df.setMaximumFractionDigits(GreenLabAtributes.DOUBLE_DIGITS);
				df.setMinimumFractionDigits(0);
				for (int i = 0; i < v.length; i++) {
					// String d = v[i];
					double d;
					try {
						d = NumberFormat.getInstance(Locale.US).parse(v[i])
								.doubleValue();
						if (i == pos * 3) {
							d = (double) r / GreenLabAtributes.MAX_RGB_VALUE;
						} else if (i == pos * 3 + 1) {
							d = (double) g / GreenLabAtributes.MAX_RGB_VALUE;
						} else if (i == pos * 3 + 2) {
							d = (double) b / GreenLabAtributes.MAX_RGB_VALUE;
						}
						value += " " + df.format(d).toString();
					} catch (ParseException e1) {
					}
				}
				GreenLabPropertyFileReader.get().setProperty(key,
						value.substring(1));
			}
		});
		optionPanel.add(confirm);
		optionPanel.add(cancel);
		colorChooserMainPanel.add(optionPanel, BorderLayout.SOUTH);
		colorFrame.pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = colorFrame.getSize();
		if (frameSize.height > screenSize.height)
			frameSize.height = screenSize.height;
		if (frameSize.width > screenSize.width)
			frameSize.width = screenSize.width;
		colorFrame.setLocation((screenSize.width - frameSize.width) / 2,
				(screenSize.height - frameSize.height) / 2);
	}

	public void setText(String p, int n) {
		key = p;
		pos = n;
		String v[] = GreenLabPropertyFileReader.get().getProperty(key)
				.split("\\s+");
		try {
			r = (int) (NumberFormat.getInstance(Locale.US).parse(v[pos * 3])
					.doubleValue() * GreenLabAtributes.MAX_RGB_VALUE);
			g = (int) (NumberFormat.getInstance(Locale.US)
					.parse(v[pos * 3 + 1]).doubleValue() * GreenLabAtributes.MAX_RGB_VALUE);
			b = (int) (NumberFormat.getInstance(Locale.US)
					.parse(v[pos * 3 + 2]).doubleValue() * GreenLabAtributes.MAX_RGB_VALUE);
		} catch (ParseException e) {
		}
		colorLabel.setBackground(new Color(r, g, b));
	}

	public void update() {
		String v[] = GreenLabPropertyFileReader.get().getProperty(key)
				.split("\\s+");
		try {
			r = (int) (NumberFormat.getInstance(Locale.US).parse(v[pos * 3])
					.doubleValue() * GreenLabAtributes.MAX_RGB_VALUE);
			g = (int) (NumberFormat.getInstance(Locale.US)
					.parse(v[pos * 3 + 1]).doubleValue() * GreenLabAtributes.MAX_RGB_VALUE);
			b = (int) (NumberFormat.getInstance(Locale.US)
					.parse(v[pos * 3 + 2]).doubleValue() * GreenLabAtributes.MAX_RGB_VALUE);
			colorLabel.setBackground(new Color(r, g, b));
			colorLabel.setForeground(new Color(r, g, b));
			colorChooser.setColor(new Color(r, g, b));
		} catch (ParseException e) {
		}
	}

	public void setEnabled(boolean b) {
		colorButton.setEnabled(b);	
	}
}
