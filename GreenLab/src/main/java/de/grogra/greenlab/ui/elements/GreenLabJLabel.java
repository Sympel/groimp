/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab.ui.elements;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.JLabel;

import de.grogra.greenlab.GreenLabGUI;

public class GreenLabJLabel extends JLabel {

	/**
	 * The TextField
	 * 
	 * @author Cong Ding
	 * 
	 */
	private static final long serialVersionUID = -4605848212192093833L;
	private String keyString = "defaultTooltip";

	public GreenLabJLabel(String text) {
		super(GreenLabGUI.getString(text) + " ");
		init(text);
	}

	public GreenLabJLabel(String text, int horizontalAlignment) {
		super(GreenLabGUI.getString(text) + " ", horizontalAlignment);
		init(text);
	}

	public GreenLabJLabel(String text, String key) {
		super(text + " ");
		init(key);
	}

	public GreenLabJLabel(Icon image) {
		super(image);
	}

	public GreenLabJLabel(Icon image, int horizontalAlignment) {
		super(image, horizontalAlignment);
	}

	private void init(String text) {
		this.keyString = text;
		final String key = this.keyString;
		this.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				GreenLabGUI.tooltipPanel.updateValue(key);
			}
		});
	}
}
