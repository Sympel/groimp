/* Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab.amapsymbol;

/**
 * Provides functionality for reading and interpreting bytes from SMB object's
 * byte array.
 * 
 * @author yongzhi ong
 * 
 */
public class SMBByteReader {

	private int firstByte = 0;
	private int secondByte = 0;
	private int thirdByte = 0;
	private int fourthByte = 0;

	/**
	 * Reads 32 bits from the byteArray. Inverts the byte order.
	 * 
	 * @return unsigned 32 bit int as a Java long
	 */
	long readUnsignedInt32(byte[] byteArray, int index) {
		long anUnsignedInt = 0;

		firstByte = (0x000000FF & ((int) byteArray[index]));
		secondByte = (0x000000FF & ((int) byteArray[index + 1]));
		thirdByte = (0x000000FF & ((int) byteArray[index + 2]));
		fourthByte = (0x000000FF & ((int) byteArray[index + 3]));

		anUnsignedInt = ((long) (fourthByte << 24 | thirdByte << 16
				| secondByte << 8 | firstByte)) & 0xFFFFFFFFL;
		return anUnsignedInt;
	}

	/**
	 * Reads 16 bits from the byteArray. Inverts the byte order.
	 * 
	 * @return unsigned 16 bit int as a Java int
	 */
	int readUnsignedInt16(byte[] byteArray, int index) {
		long anUnsignedInt = 0;

		firstByte = (0x000000FF & ((int) byteArray[index]));
		secondByte = (0x000000FF & ((int) byteArray[index + 1]));

		anUnsignedInt = ((int) (secondByte << 8 | firstByte)) & 0xFFFFFFFFL;
		return (int) anUnsignedInt;
	}

	/**
	 * Reads 32 bits from the byteArray. Inverts the byte order.
	 * 
	 * @return float as a Java float
	 */
	float readFloat32(byte[] byteArray, int index) {
		firstByte = (0x000000FF & ((int) byteArray[index]));
		secondByte = (0x000000FF & ((int) byteArray[index + 1]));
		thirdByte = (0x000000FF & ((int) byteArray[index + 2]));
		fourthByte = (0x000000FF & ((int) byteArray[index + 3]));

		int asInt = (firstByte & 0xFF) | ((secondByte & 0xFF) << 8)
				| ((thirdByte & 0xFF) << 16) | ((fourthByte & 0xFF) << 24);

		float asFloat = Float.intBitsToFloat(asInt);

		return asFloat;
	}
}
