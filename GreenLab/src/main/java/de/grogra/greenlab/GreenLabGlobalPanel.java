/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;
import de.grogra.greenlab.ui.elements.GreenLabAgeComboBox;
import de.grogra.greenlab.ui.elements.GreenLabBooleanCheckBox;
import de.grogra.greenlab.ui.elements.GreenLabJLabel;
import de.grogra.greenlab.ui.elements.GreenLabNumberField;

/**
 * The Global Panel
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabGlobalPanel extends JPanel {

	private static final long serialVersionUID = 8233686427720094332L;
	private GreenLabNumberField field_Chr_Age;
	private GreenLabAgeComboBox field_Phy_Age;
	private GreenLabJLabel paLabel;

	private JTabbedPane jtp;
	private JPanel pap[] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private GreenLabNumberField sampleSize[] = new GreenLabNumberField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private String str = "Sample_Size(Tr)";
	private String comp = "Flag_hydro";
	private GreenLabBooleanCheckBox comPhy;

	GreenLabGlobalPanel(final GreenLabTopologyPanel topologyPanel,
			final GreenLabGrowthPanel growthPanel,
			final GreenLabGeometryPanel geomatryPanel,
			GreenLabEnvironmentPanel environmentPanel,
			final GreenLabOutputPanel outputPanel) {

		comPhy = new GreenLabBooleanCheckBox(comp);

		comPhy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; j++) {
					GreenLabPropertyFileReader.get().setProperty(comp,
							comPhy.getTextValue());
				}
			}
		});

		jtp = new JTabbedPane();
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			pap[i] = new JPanel(new GridLayout(3, 6));
			jtp.add(GreenLabGUI.getString("PA") + (i + 1), pap[i]);
			sampleSize[i] = new GreenLabNumberField(str.replaceAll("[\\(\\)]",
					"_"));
			for (int j = 0; j < 6; j++) {
				pap[i].add(new GreenLabJLabel(""));
			}
			pap[i].add(new GreenLabJLabel((str.replaceAll("[\\(\\)]", "_"))));
			pap[i].add(sampleSize[i]);
			for (int j = 0; j < 10; j++) {
				pap[i].add(new GreenLabJLabel(""));
			}
			final int ii = i;
			sampleSize[i].getDocument().addDocumentListener(
					new DocumentListener() {
						public void actionPerformed() {
							GreenLabPropertyFileReader.get().setProperty(str,
									sampleSize[ii].getText(), ii + 1);
						}

						@Override
						public void changedUpdate(DocumentEvent arg0) {
							actionPerformed();
						}

						@Override
						public void insertUpdate(DocumentEvent arg0) {
							actionPerformed();
						}

						@Override
						public void removeUpdate(DocumentEvent arg0) {
							actionPerformed();
						}
					});
		}

		field_Chr_Age = new GreenLabNumberField("Chr_Age_N_");
		field_Phy_Age = new GreenLabAgeComboBox("Phy_Age_Maxp_");
		JPanel mainPanel;
		mainPanel = new JPanel(new GridLayout(4, 3));

		GreenLabJLabel label_Chr_Age = new GreenLabJLabel(("Chr_Age_N_"));
		GreenLabJLabel label_Phy_Age = new GreenLabJLabel(("Phy_Age_Maxp_"));

		mainPanel.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "ca_trans"), GreenLabJLabel.RIGHT));
		JPanel tmpPanel = new JPanel(new BorderLayout());
		tmpPanel.add(label_Chr_Age, BorderLayout.SOUTH);
		mainPanel.add(tmpPanel);
		tmpPanel = new JPanel(new BorderLayout());
		tmpPanel.add(field_Chr_Age, BorderLayout.SOUTH);
		mainPanel.add(tmpPanel);
		paLabel = new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "pa1_trans"), GreenLabJLabel.RIGHT);
		mainPanel.add(paLabel);
		tmpPanel = new JPanel(new BorderLayout());
		tmpPanel.add(label_Phy_Age, BorderLayout.SOUTH);
		mainPanel.add(tmpPanel);
		tmpPanel = new JPanel(new BorderLayout());
		tmpPanel.add(field_Phy_Age, BorderLayout.SOUTH);
		mainPanel.add(tmpPanel);
		mainPanel.add(new GreenLabJLabel(""));
		JPanel comPanel = new JPanel();
		comPanel.add(comPhy);
		comPanel.add(new GreenLabJLabel(comp));
		tmpPanel = new JPanel(new BorderLayout());
		tmpPanel.add(comPanel, BorderLayout.SOUTH);
		mainPanel.add(tmpPanel);
		mainPanel.add(new GreenLabJLabel(""));
		for (int i = 0; i < 3; i++) {
			mainPanel.add(new GreenLabJLabel(""));
		}
		field_Chr_Age.getDocument().addDocumentListener(new DocumentListener() {
			public void actionPerformed() {
				GreenLabPropertyFileReader.get().setProperty("Chr_Age(N)",
						field_Chr_Age.getText());
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				actionPerformed();
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				actionPerformed();
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				actionPerformed();
			}
		});
		final GreenLabGlobalPanel tt = this;
		field_Phy_Age.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				GreenLabPropertyFileReader.get().setProperty("Phy_Age(Maxp)",
						field_Phy_Age.getText());
				tt.activeTab(field_Phy_Age.getPos());
				topologyPanel.activeTab(field_Phy_Age.getPos());
				growthPanel.activeTab(field_Phy_Age.getPos());
				geomatryPanel.activeTab(field_Phy_Age.getPos());
				outputPanel.activeTab(field_Phy_Age.getPos());
				paLabel.setIcon(GreenLabAtributes.getImageIcon(this.getClass(), "pa"
						+ (field_Phy_Age.getPos() + 1) + "_trans"));
			}
		});
		JPanel mainmPanel = new JPanel(new BorderLayout());
		mainmPanel.add(mainPanel, BorderLayout.WEST);
		mainmPanel.setBorder(new TitledBorder(""));
		Box box = Box.createVerticalBox();
		box.add(mainmPanel);
//		box.add(jtp); //TODO: activate this line to insert sample size
		JPanel mPanel = new JPanel(new BorderLayout());
		mPanel.add(box, BorderLayout.NORTH);
		this.setLayout(new BorderLayout());
		this.add(mPanel, BorderLayout.WEST);
		updateValue();
		this.activeTab(field_Phy_Age.getPos());
		topologyPanel.activeTab(field_Phy_Age.getPos());
		growthPanel.activeTab(field_Phy_Age.getPos());
		geomatryPanel.activeTab(field_Phy_Age.getPos());
		outputPanel.activeTab(field_Phy_Age.getPos());
	}

	public void updateValue() {
		field_Chr_Age.setValue(GreenLabPropertyFileReader.get().getProperty(
				"Chr_Age(N)"));
		field_Phy_Age.setText(GreenLabPropertyFileReader.get().getProperty(
				"Phy_Age(Maxp)"));
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			sampleSize[i].setValue(GreenLabPropertyFileReader.get()
					.getProperty(str, i + 1));
		}
		comPhy.setTextValue(GreenLabPropertyFileReader.get().getProperty(comp));
	}

	public void activeTab(int n) {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			if (i <= n) {
				jtp.setEnabledAt(i, true);
			} else {
				jtp.setEnabledAt(i, false);
			}
		}
		// jtp.setSelectedIndex(n);
	}

}
