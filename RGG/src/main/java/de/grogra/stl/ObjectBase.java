/*
  * Copyright (C) 2021 GroIMP Developer Team
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 3
  * of the License, or any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 
02111-1307, USA.
  */

package de.grogra.stl;

import java.io.IOException;
import java.io.PrintWriter;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.imp3d.PolygonArray;

/**
 * Wrapper class. Part of STLExport
 * 
 * http://en.wikipedia.org/wiki/STL_(file_format)
 * 
 * MH 2015-04-28
 */

public abstract class ObjectBase implements SceneGraphExport.NodeExport {

	Matrix4d transformation;
	PrintWriter out;
	
	protected void writeFace(Vector3d a, Vector3d b, Vector3d c) {
		if (!checkArea(a, b, c)) {
			return;
		}
		Vector3d normal = new Vector3d();
		Vector3d h1 = new Vector3d();
		Vector3d h2 = new Vector3d();
		h1.sub(b,a);
		h2.sub(c,a);
		normal.cross(h1, h2);
		normal.normalize();
		out.println("facet normal "+round(normal.x)+" "+round(normal.y)+" "+round(normal.z));
		out.println("  outer loop");
		out.println("    vertex "+round(a.x)+" "+round(a.y)+" "+round(a.z));
		out.println("    vertex "+round(b.x)+" "+round(b.y)+" "+round(b.z));
		out.println("    vertex "+round(c.x)+" "+round(c.y)+" "+round(c.z));
		out.println("  endloop");
		out.println("endfacet");
	}

	@Override
	public void export (Leaf node, InnerNode transform, SceneGraphExport sge) throws IOException {
		// convert to STLExport
		STLExport export = (STLExport) sge;

		// obtain output writer
		out = export.out;

		// obtain transformation matrix for this node
		Matrix4d m = export.matrixStack.peek ();
		Matrix4d n = new Matrix4d ();
		if (transform != null) {
			transform.transform (m, n);
		} else {
			n.set (m);
		}
		transformation = n;

		// call implementation export function
		exportImpl (node, transform, export);
	}

	abstract void exportImpl (Leaf node, InnerNode transform, STLExport export) throws IOException;

	Matrix4d getTransformation () {
		return transformation;
	}

	// round to 5 decimal digits
	static double round (double d) {
		return (double) Math.round (d * 100000) / 100000;
	}

	void mesh1 (PolygonArray p) {
		if (p.polygons.size == 0) return;
		int v1,v2,v3;
		// output indices
		for (int i = 0; i < p.polygons.size (); i += p.edgeCount) {
			v1 = p.polygons.get (i + 0);
			v2 = p.polygons.get (i + 1);
			v3 = p.polygons.get (i + 2);
			writeFace(
				new Vector3d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2)),
				new Vector3d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2)),
				new Vector3d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2))
			);
		}
	}

	void mesh2 (PolygonArray p) {
		if (p.polygons.size == 0) return;
		int v1,v2,v3;
		if(p.edgeCount==3) {
			// output indices
			for (int i = 0; i < p.polygons.size (); i += p.edgeCount) {
				v1 = p.polygons.get (i + 0);
				v2 = p.polygons.get (i + 1);
				v3 = p.polygons.get (i + 2);
				writeFace(
					new Vector3d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2)),
					new Vector3d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2)),
					new Vector3d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2))
				);
				writeFace(
					new Vector3d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2)),
					new Vector3d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2)),
					new Vector3d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2))
				);
			}
		}
		if(p.edgeCount==4) {
			int v4;
			// output indices
			for (int i = 0; i < p.polygons.size (); i += p.edgeCount) {
				v1 = p.polygons.get (i + 0);
				v2 = p.polygons.get (i + 1);
				v3 = p.polygons.get (i + 2);
				v4 = p.polygons.get (i + 3);
				writeFace(
					new Vector3d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2)),
					new Vector3d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2)),
					new Vector3d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2))
				);
				writeFace(
					new Vector3d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2)),
					new Vector3d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2)),
					new Vector3d(p.vertices.get(v4*p.dimension+0),p.vertices.get(v4*p.dimension+1),p.vertices.get(v4*p.dimension+2))
				);
			}
		}
	}
	

	private boolean checkArea(Vector3d a, Vector3d b, Vector3d c) {
		return calcTrArea(a,b,c)>0.0000001;
	}
	
	private double calcTrArea(Vector3d va, Vector3d vb, Vector3d vc) {
		double x1 = va.x;
		double y1 = va.y;
		double z1 = va.z;
		double x2 = vb.x;
		double y2 = vb.y;
		double z2 = vb.z;
		double x3 = vc.x;
		double y3 = vc.y;
		double z3 = vc.z;
		double a = Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1));
		double b = Math.sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1)+(z3-z1)*(z3-z1));
		double c = Math.sqrt((x3-x2)*(x3-x2)+(y3-y2)*(y3-y2)+(z3-z2)*(z3-z2));
		double p = 0.5f*(a+b+c);
		double d = p*(p-a)*(p-b)*(p-c);
		if (d<0) { // bad value
			d=0;
		}
		return Math.sqrt(d);
	}
}
