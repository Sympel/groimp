package de.grogra.imp3d.gl;

abstract class DisplayListRenderable
{
	abstract void render (float lod);
}
