module imp3d {
	exports de.grogra.imp3d.ray;
	exports de.grogra.imp3d;
	exports de.grogra.imp3d.gl;
	exports de.grogra.imp3d.glsl.utility;
	exports de.grogra.imp3d.glsl.renderpass;
	exports de.grogra.imp3d.io;
	exports de.grogra.imp3d.objects;
	exports de.grogra.imp3d.glsl;
	exports de.grogra.imp3d.glsl.light;
	exports de.grogra.imp3d.glsl.renderable;
	exports de.grogra.imp3d.glsl.material;
	exports de.grogra.imp3d.glsl.renderpass.nostencil;
	exports de.grogra.imp3d.ray2;
	exports de.grogra.imp3d.math.delaunay;
	exports de.grogra.imp3d.gl20;
	exports de.grogra.imp3d.glsl.renderable.vbo;
	exports de.grogra.imp3d.shading;
	exports de.grogra.imp3d.anaglyph;
	exports de.grogra.imp3d.glsl.light.shadow;
	exports de.grogra.imp3d.msml;
	exports de.grogra.imp3d.edit;
	exports de.grogra.imp3d.glsl.material.channel;

	requires graph;
	requires imp;
	requires math;
	requires platform;
	requires platform.core;
	requires raytracer;
	requires utilities;
	requires vecmath;
	requires xl;
	requires xl.core;
	requires xl.impl;
	requires java.datatransfer;
	requires java.desktop;
	requires java.logging;
	requires java.xml;
	requires jogl;
	
	opens de.grogra.imp3d to utilities;
	opens de.grogra.imp3d.gl to utilities;
	opens de.grogra.imp3d.glsl to utilities;
	opens de.grogra.imp3d.shading to utilities;
	
	provides javax.imageio.spi.ImageReaderSpi with de.grogra.imp3d.io.HGTImageReaderSpi;
}