package de.grogra.ext.x3d;

import java.util.HashMap;
import de.grogra.ext.x3d.importation.ProtoDeclareImport;
import de.grogra.ext.x3d.xmlbeans.X3DNode;
import de.grogra.ext.x3d.xmlbeans.ConnectDocument.Connect;
import de.grogra.ext.x3d.xmlbeans.FieldValueDocument.FieldValue;
import de.grogra.ext.x3d.xmlbeans.ProtoInstanceDocument.ProtoInstance;
import de.grogra.graph.impl.Node;

public class ProtoInstanceImport {

	private static ProtoInstanceImport lastProtoInstanceImport;
	private HashMap<String, String> valueMap;
	
	public static Node createInstance(ProtoInstance protoInstance) {
		
		ProtoInstanceImport pii = new ProtoInstanceImport();
		lastProtoInstanceImport = pii;

		pii.valueMap = new HashMap<String, String>();
		FieldValue[] fieldValues = protoInstance.getFieldValueArray();
		for (FieldValue fieldValue : fieldValues) {
			pii.valueMap.put(fieldValue.getName(), fieldValue.getValue());
		}
		
		X3DImport importer = X3DImport.getTheImport();
		
		String protoInstanceName = protoInstance.getName();
		ProtoDeclareImport pdi = X3DImport.getTheImport().getCurrentParser().getProtoMap().get(protoInstanceName);
		X3DParser parser = new X3DParser(pdi.getProtoBody(), importer.getCurrentParser().getFile());

		Node protoRoot = new Node();
		protoRoot.setName("X3DProtoInstance");
		
		importer.addNewParser(parser);
		parser.parseScene(protoRoot);
		importer.removeCurrentParser();
		
		return protoRoot;
	}

	public static ProtoInstanceImport getLastProtoInstanceImport() {
		return lastProtoInstanceImport;
	}

	public HashMap<String, String> getValueMap() {
		return valueMap;
	}
	
	public static void calcFinalValueMap(HashMap<String, String> finalValueMap, X3DNode x3dnode) {
		ProtoInstanceImport pii = getLastProtoInstanceImport();
		HashMap<String, String> valueMap = pii.getValueMap();
		Connect[] connects = x3dnode.getIS().getConnectArray();
		for (Connect connect : connects) {
			String field = connect.getNodeField();
			String protoField = connect.getProtoField();
			String value = valueMap.get(protoField);
			finalValueMap.put(field, value);
		}		
	}
		
}
