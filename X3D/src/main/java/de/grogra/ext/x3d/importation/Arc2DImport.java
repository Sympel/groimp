package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.Arc2DDocument.Arc2D;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.ColoredNull;
import de.grogra.imp3d.objects.NURBSCurve;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.math.Arc;
import de.grogra.math.ChannelMap;
import de.grogra.math.RGBColor;

public class Arc2DImport {

	public static ColoredNull createInstance(Arc2D arc, Node parent) {
		NURBSCurve n = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (arc.isSetUSE()) {
			try {
				n = (NURBSCurve) ((Node) referenceMap.get(arc.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new NURBSCurve();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (arc.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, arc);
			}
			
			setValues(n, arc, finalValueMap, parent);
			if (arc.isSetDEF()) {
				referenceMap.put(arc.getDEF(), n);
			}
		}
		
		n.setName("X3DArc2D");
		return n;
	}
	
	private static void setValues(NURBSCurve node, Arc2D arc, HashMap<String, String> valueMap, Node parent) {
		// startAngle
		float startAngle;
		startAngle = valueMap.containsKey("startAngle") ?
				Float.valueOf(valueMap.get("startAngle")) :
					arc.isSetStartAngle() ?
						arc.getStartAngle() :
							0;
		
		// endAngle
		float endAngle;
		endAngle = valueMap.containsKey("endAngle") ?
				Float.valueOf(valueMap.get("endAngle")) :
					arc.isSetEndAngle() ?
						arc.getEndAngle() :
							(float) (Math.PI/2.0);

		// radius
		float radius;
		radius = valueMap.containsKey("radius") ?
				Float.valueOf(valueMap.get("radius")) :
					arc.isSetRadius() ?
						arc.getRadius() :
							1f;
						
		// use color from material node (if exists)
		RGBColor c = new RGBColor(0, 0, 0);
		if (parent instanceof ShadedNull) {
			Shader s = ((ShadedNull) parent).getShader();
			if (s instanceof Phong) {
				ChannelMap cm = ((Phong) s).getEmissive();
				if (cm instanceof RGBColor) {
					c = (RGBColor) cm;
				}
			}
		}
						
		Arc a = new Arc(startAngle, endAngle, radius);
		a.setPlane(1);
		node.setCurve(a);
		node.setColor(c);
	}
	
}
