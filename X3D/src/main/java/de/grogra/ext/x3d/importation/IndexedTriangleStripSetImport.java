package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.IndexedTriangleStripSetDocument.IndexedTriangleStripSet;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class IndexedTriangleStripSetImport {

	public static ShadedNull createInstance(IndexedTriangleStripSet triangleStripSet, Node parent) {
		MeshNode m = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (triangleStripSet.isSetUSE()) {
			try {
				m = (MeshNode) ((Node) referenceMap.get(triangleStripSet.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			m = new MeshNode();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (triangleStripSet.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, triangleStripSet);
			}
			
			setValues(m, triangleStripSet, finalValueMap, parent);
			if (triangleStripSet.isSetDEF()) {
				referenceMap.put(triangleStripSet.getDEF(), m);
			}
		}
		
		m.setName("X3DIndexedTriangleStripSet");
		return m;
	}
	
	private static void setValues(MeshNode node, IndexedTriangleStripSet triangleStripSet, HashMap<String, String> valueMap, Node parent) {
		
		// coords
		float[] coords = null;
		if (triangleStripSet.getCoordinateArray().length > 0)
			coords = CoordinateImport.createInstance(triangleStripSet.getCoordinateArray(0));
		
		// normals
		float[] normals = null;
		if (triangleStripSet.getNormalArray().length > 0)
			normals = NormalImport.createInstance(triangleStripSet.getNormalArray(0));
		
		// texCoords
		float[] texCoords = null;
		if (triangleStripSet.getTextureCoordinateArray().length > 0)
			texCoords = TextureCoordinateImport.createInstance(triangleStripSet.getTextureCoordinateArray(0));

		// index
		int[] index;
		String indexString = valueMap.containsKey("index") ? valueMap.get("index") : triangleStripSet.getIndex();
		index = Util.splitStringToArrayOfInt(indexString, new int[]{});		
		
		// ccw
		boolean ccw;
		ccw = valueMap.containsKey("ccw") ?
				Boolean.valueOf(valueMap.get("ccw")) :
					triangleStripSet.isSetCcw() ?
							triangleStripSet.getCcw() :
								true;
		
		// normalPerVertex
		boolean normalPerVertex;
		normalPerVertex = valueMap.containsKey("normalPerVertex") ?
				Boolean.valueOf(valueMap.get("normalPerVertex")) :
					triangleStripSet.isSetNormalPerVertex() ?
							triangleStripSet.getNormalPerVertex() :
								true;
		
		// solid
		boolean solid;
		solid = valueMap.containsKey("solid") ?
				Boolean.valueOf(valueMap.get("solid")) :
					triangleStripSet.isSetSolid() ?
							triangleStripSet.getSolid() :
								true;

		// if no coordinates exist, no mesh is created
//		if (coords == null)
//			return;
		
		PolygonMesh p = new PolygonMesh();
		
		int coordCount = index.length;
		
		IntList indexData = new IntList();
		FloatList vertexData = new FloatList();
		FloatList normalData = new FloatList();
		FloatList textureData = new FloatList();
		
		int vertexCount = 0;
		int startVertex = 0;
		
		for (int i = 0; i < coordCount - 2; i++) {
			// loop over strips
			
			boolean odd;
			
			if (index[i+2] != -1) {
				// create triangle
				
				odd = ((i - startVertex) % 2 == 1);
				
				// set vertices
				indexData.add(vertexCount);
				vertexData.add( coords[3*index[i+0]+0]);
				vertexData.add(-coords[3*index[i+0]+2]);
				vertexData.add( coords[3*index[i+0]+1]);

				indexData.add(vertexCount + (ccw ^ odd ? 1 : 2));
				vertexData.add( coords[3*index[i+1]+0]);
				vertexData.add(-coords[3*index[i+1]+2]);
				vertexData.add( coords[3*index[i+1]+1]);

				indexData.add(vertexCount + (ccw ^ odd ? 2 : 1));
				vertexData.add( coords[3*index[i+2]+0]);
				vertexData.add(-coords[3*index[i+2]+2]);
				vertexData.add( coords[3*index[i+2]+1]);

				// set normals
				if ((normals != null) && normalPerVertex) {
					normalData.add( normals[3*index[i+0]+0]);
					normalData.add(-normals[3*index[i+0]+2]);
					normalData.add( normals[3*index[i+0]+1]);
					
					normalData.add( normals[3*index[i+1]+0]);
					normalData.add(-normals[3*index[i+1]+2]);
					normalData.add( normals[3*index[i+1]+1]);

					normalData.add( normals[3*index[i+2]+0]);
					normalData.add(-normals[3*index[i+2]+2]);
					normalData.add( normals[3*index[i+2]+1]);
				}
				
				// set texture coordinates
				if (texCoords != null) {
					textureData.add(texCoords[2*index[i+0]+0]);
					textureData.add(texCoords[2*index[i+0]+1]);
					
					textureData.add(texCoords[2*index[i+1]+0]);
					textureData.add(texCoords[2*index[i+1]+1]);

					textureData.add(texCoords[2*index[i+2]+0]);
					textureData.add(texCoords[2*index[i+2]+1]);
				}
				
				vertexCount += 3;
			}
			else {
				// end of strip
				i += 2;
				startVertex = i + 1;
			}
		}
		
		p.setIndexData(indexData);
		p.setVertexData(vertexData);
		if ((normals != null) && normalPerVertex)
			p.setNormalData(normalData.elements);
		if (texCoords != null)
			p.setTextureData(textureData.elements);
		
		node.setVisibleSides(solid ? 0 : 2);
		
		node.setPolygons(p);
	}
}
