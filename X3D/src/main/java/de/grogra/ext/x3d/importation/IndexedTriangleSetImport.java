package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.IndexedTriangleSetDocument.IndexedTriangleSet;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class IndexedTriangleSetImport {

	public static ShadedNull createInstance(IndexedTriangleSet triangleSet, Node parent) {
		MeshNode m = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (triangleSet.isSetUSE()) {
			try {
				m = (MeshNode) ((Node) referenceMap.get(triangleSet.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			m = new MeshNode();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (triangleSet.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, triangleSet);
			}
			
			setValues(m, triangleSet, finalValueMap, parent);
			if (triangleSet.isSetDEF()) {
				referenceMap.put(triangleSet.getDEF(), m);
			}
		}
		
		m.setName("X3DTriangleSet");
		return m;
	}
	
	private static void setValues(MeshNode node, IndexedTriangleSet triangleSet, HashMap<String, String> valueMap, Node parent) {
		
		// coords
		float[] coords = null;
		if (triangleSet.getCoordinateArray().length > 0)
			coords = CoordinateImport.createInstance(triangleSet.getCoordinateArray(0));
		
		// normals
		float[] normals = null;
		if (triangleSet.getNormalArray().length > 0)
			normals = NormalImport.createInstance(triangleSet.getNormalArray(0));
		
		// texCoords
		float[] texCoords = null;
		if (triangleSet.getTextureCoordinateArray().length > 0)
			texCoords = TextureCoordinateImport.createInstance(triangleSet.getTextureCoordinateArray(0));

		// ccw
		boolean ccw;
		ccw = valueMap.containsKey("ccw") ?
				Boolean.valueOf(valueMap.get("ccw")) :
					triangleSet.isSetCcw() ?
							triangleSet.getCcw() :
								true;
		
		// normalPerVertex
		boolean normalPerVertex;
		normalPerVertex = valueMap.containsKey("normalPerVertex") ?
				Boolean.valueOf(valueMap.get("normalPerVertex")) :
					triangleSet.isSetNormalPerVertex() ?
							triangleSet.getNormalPerVertex() :
								true;
		
		// solid
		boolean solid;
		solid = valueMap.containsKey("solid") ?
				Boolean.valueOf(valueMap.get("solid")) :
					triangleSet.isSetSolid() ?
							triangleSet.getSolid() :
								true;
		
		// index
		int[] index;
		String indexString = valueMap.containsKey("index") ? valueMap.get("index") : triangleSet.getIndex();
		index = Util.splitStringToArrayOfInt(indexString, new int[]{});

		// if no coordinates exist, no mesh is created
//		if (coords == null)
//			return;
		
		PolygonMesh p = new PolygonMesh();
		
		int coordCount = index.length;

		IntList indexData = new IntList();
		FloatList vertexData = new FloatList();
		FloatList normalData = new FloatList();
		FloatList textureData = new FloatList();
		
		// loop over coordinates and put them into mesh
		for (int i = 0; i < coordCount; i++) {
			// if ccw then swap second and third vertex
			int j = i;
			if ((i % 3 == 1) && !ccw)
				j++;
			if ((i % 3 == 2) && !ccw)
				j--;
			indexData.add(j);
			
			vertexData.add( coords[3*index[i]+0]);
			vertexData.add(-coords[3*index[i]+2]);
			vertexData.add( coords[3*index[i]+1]);
			
			if ((normals != null) && normalPerVertex) {
				normalData.add( normals[3*index[i]+0]);
				normalData.add(-normals[3*index[i]+2]);
				normalData.add( normals[3*index[i]+1]);
			}
			
			if (texCoords != null) {
				textureData.add(texCoords[2*index[i]+0]);
				textureData.add(texCoords[2*index[i]+1]);
			}
		}
		
		p.setIndexData(indexData);
		p.setVertexData(vertexData);
		if ((normals != null) && normalPerVertex)
			p.setNormalData(normalData.elements);
		if (texCoords != null)
			p.setTextureData(texCoords);
		
		node.setVisibleSides(solid ? 0 : 2);
		
		node.setPolygons(p);
	}
}
