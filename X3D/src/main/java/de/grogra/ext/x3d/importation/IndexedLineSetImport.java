package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import javax.vecmath.Point3f;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.IndexedLineSetDocument.IndexedLineSet;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Line;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.math.ChannelMap;
import de.grogra.math.RGBColor;

public class IndexedLineSetImport {

	public static Null createInstance(IndexedLineSet lineSet, Node parent) {
		Null n = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (lineSet.isSetUSE()) {
			try {
				n = (Null) ((Node) referenceMap.get(lineSet.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new Null();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (lineSet.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, lineSet);
			}
			
			setValues(n, lineSet, finalValueMap, parent);
			if (lineSet.isSetDEF()) {
				referenceMap.put(lineSet.getDEF(), n);
			}			
		}
		
		n.setName("X3DIndexedLineSet");
		return n;
	}
	
	private static void setValues(Null node, IndexedLineSet lineSet, HashMap<String, String> valueMap, Node parent) {
		// coord
		float[] coord = null;
		if (lineSet.getCoordinate() != null)
			coord = CoordinateImport.createInstance(lineSet.getCoordinate());
		
		// color
		float[] color = null;
		if (lineSet.getColor() != null)
			color = ColorImport.createInstance(lineSet.getColor());
		
		// colorIndex
		int[] colorIndex;
		String colorIndexString = valueMap.containsKey("colorIndex") ? valueMap.get("colorIndex") : lineSet.getColorIndex();
		colorIndex = Util.splitStringToArrayOfInt(colorIndexString, new int[]{});
		
		// colorPerVertex
		boolean colorPerVertex;
		colorPerVertex = valueMap.containsKey("colorPerVertex") ? 
				Boolean.valueOf(valueMap.get("colorPerVertex")) :
					lineSet.isSetColorPerVertex() ?
							lineSet.getColorPerVertex() :
								true;
		
		// coordIndex
		int[] coordIndex;
		String coordIndexString = valueMap.containsKey("coordIndex") ? valueMap.get("coordIndex") : lineSet.getCoordIndex();
		coordIndex = Util.splitStringToArrayOfInt(coordIndexString, new int[]{});
		
		
		// create lines
		int pointCount = (int) (coord.length / 3.0f);
		// convert coordinates to GroIMP space
		Point3f[] coords = new Point3f[pointCount];
		for (int i = 0; i < pointCount; i++) {
			coords[i] = new Point3f( coord[3*i+0],
									-coord[3*i+2],
									 coord[3*i+1]);
		}
		
		int lineCount = 0;
		for (int i = 1; i < coordIndex.length; i++) {
			// loop over polylines in coord index
			// split every polyline to single lines of two vertices
			if ((coordIndex[i] != -1) && (coordIndex[i-1] != -1)) {
				float x = coords[coordIndex[i-1]].x;
				float y = coords[coordIndex[i-1]].y;
				float z = coords[coordIndex[i-1]].z;
				float dx = coords[coordIndex[i]].x - coords[coordIndex[i-1]].x;
				float dy = coords[coordIndex[i]].y - coords[coordIndex[i-1]].y;
				float dz = coords[coordIndex[i]].z - coords[coordIndex[i-1]].z;
				Line newLine = new Line(x, y, z, dx, dy, dz);
				
				if ((color != null) && (colorPerVertex == false)) {
					// if exists a color node and color per vertex is false use colors from
					// indexedlineset node
					if (colorIndex.length == 0) {
						// if color index empty, the colours from the color node
						// are applied to each polyline  in order
						newLine.setColor(color[3*lineCount+0],
										 color[3*lineCount+1],
										 color[3*lineCount+2]);
					} else {
						// use the color index to apply colors
						float r = color[3*colorIndex[lineCount]+0];
						float g = color[3*colorIndex[lineCount]+1];
						float b = color[3*colorIndex[lineCount]+2];
						newLine.setColor(r, g, b);
					}
				} else {
					// use color from material node (if exists)
					if (parent instanceof ShadedNull) {
						Shader s = ((ShadedNull) parent).getShader();
						if (s instanceof Phong) {
							ChannelMap cm = ((Phong) s).getEmissive();
							if (cm instanceof RGBColor) {
								newLine.setColor((RGBColor) cm);
							}
						}
					}
				}
				newLine.setName("X3DLine");
				node.addEdgeBitsTo(newLine, Graph.BRANCH_EDGE, null);
				lineCount++;
			}
		}
	}
	
}
