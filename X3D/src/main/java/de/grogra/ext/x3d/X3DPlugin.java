package de.grogra.ext.x3d;

import de.grogra.pf.registry.Plugin;
import de.grogra.util.I18NBundle;

/**
 * This class describes a x3d import and export groimp plugin.
 * It also contains internationalization informations.
 * 
 * @author Udo Bischof, Uwe Mannl
 *
 */
public class X3DPlugin extends Plugin {

	public static final I18NBundle I18N = I18NBundle.getInstance (X3DPlugin.class); 
	
}
