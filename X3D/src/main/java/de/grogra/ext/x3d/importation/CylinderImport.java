package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.CylinderDocument.Cylinder;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.ShadedNull;

public class CylinderImport {

	public static ShadedNull createInstance(Cylinder cylinder) {
		de.grogra.imp3d.objects.Cylinder c = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (cylinder.isSetUSE()) {
			try {
				c = (de.grogra.imp3d.objects.Cylinder) ((Node) referenceMap.get(cylinder.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			c = new de.grogra.imp3d.objects.Cylinder();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (cylinder.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, cylinder);
			}
			
			setValues(c, cylinder, finalValueMap);
			if (cylinder.isSetDEF()) {
				referenceMap.put(cylinder.getDEF(), c);
			}				
		}
	
		c.setName("X3DCylinder");
		return c;
	}
	
	private static void setValues(de.grogra.imp3d.objects.Cylinder node, Cylinder cylinder, HashMap<String, String> valueMap) {
		// radius
		float radius;
		radius = (valueMap.get("radius") != null) ?
				Float.valueOf(valueMap.get("radius")) :
					cylinder.isSetRadius() ?
						cylinder.getRadius() :
							1f;
		node.setRadius(radius);
		
		// height
		float height;
		height = (valueMap.get("height") != null) ?
				Float.valueOf(valueMap.get("height")) :
					cylinder.isSetHeight() ?
						cylinder.getHeight() :
							2f;
		node.setLength(height);
		
		// top
		boolean top;
		top = (valueMap.get("top") != null) ?
				Boolean.valueOf(valueMap.get("top")) :
					cylinder.isSetTop() ?
							cylinder.getTop() :
								true;
		node.setTopOpen(!top);
		
		// bottom
		boolean bottom;
		bottom = (valueMap.get("bottom") != null) ?
				Boolean.valueOf(valueMap.get("bottom")) :
					cylinder.isSetBottom() ?
							cylinder.getBottom() :
								true;
		node.setBaseOpen(!bottom);
		
		// groimp-specific attributes
		node.setStartPosition(-0.5f);
		node.setEndPosition(0);
	}
	
}
