package de.grogra.ext.x3d;

import java.io.File;
import java.util.HashMap;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlTokenSource;
import de.grogra.ext.x3d.importation.AppearanceImport;
import de.grogra.ext.x3d.importation.Arc2DImport;
import de.grogra.ext.x3d.importation.BackgroundImport;
import de.grogra.ext.x3d.importation.BillboardImport;
import de.grogra.ext.x3d.importation.BoxImport;
import de.grogra.ext.x3d.importation.Circle2DImport;
import de.grogra.ext.x3d.importation.ConeImport;
import de.grogra.ext.x3d.importation.CylinderImport;
import de.grogra.ext.x3d.importation.DirectionalLightImport;
import de.grogra.ext.x3d.importation.ElevationGridImport;
import de.grogra.ext.x3d.importation.ExternProtoDeclareImport;
import de.grogra.ext.x3d.importation.ExtrusionImport;
import de.grogra.ext.x3d.importation.GroupImport;
import de.grogra.ext.x3d.importation.IndexedFaceSetImport;
import de.grogra.ext.x3d.importation.IndexedLineSetImport;
import de.grogra.ext.x3d.importation.IndexedTriangleFanSetImport;
import de.grogra.ext.x3d.importation.IndexedTriangleSetImport;
import de.grogra.ext.x3d.importation.IndexedTriangleStripSetImport;
import de.grogra.ext.x3d.importation.LODImport;
import de.grogra.ext.x3d.importation.LineSetImport;
import de.grogra.ext.x3d.importation.PointLightImport;
import de.grogra.ext.x3d.importation.PointSetImport;
import de.grogra.ext.x3d.importation.Polyline2DImport;
import de.grogra.ext.x3d.importation.Polypoint2DImport;
import de.grogra.ext.x3d.importation.ProtoDeclareImport;
import de.grogra.ext.x3d.importation.Rectangle2DImport;
import de.grogra.ext.x3d.importation.ShapeImport;
import de.grogra.ext.x3d.importation.SphereImport;
import de.grogra.ext.x3d.importation.SpotLightImport;
import de.grogra.ext.x3d.importation.TransformImport;
import de.grogra.ext.x3d.importation.TriangleFanSetImport;
import de.grogra.ext.x3d.importation.TriangleSet2DImport;
import de.grogra.ext.x3d.importation.TriangleSetImport;
import de.grogra.ext.x3d.importation.TriangleStripSetImport;
import de.grogra.ext.x3d.xmlbeans.SceneGraphStructureNodeType;
import de.grogra.ext.x3d.xmlbeans.X3DAppearanceNode;
import de.grogra.ext.x3d.xmlbeans.X3DBackgroundNode;
import de.grogra.ext.x3d.xmlbeans.X3DBindableNode;
import de.grogra.ext.x3d.xmlbeans.X3DChildNode;
import de.grogra.ext.x3d.xmlbeans.X3DComposedGeometryNode;
import de.grogra.ext.x3d.xmlbeans.X3DGeometryNode;
import de.grogra.ext.x3d.xmlbeans.X3DGroupingNode;
import de.grogra.ext.x3d.xmlbeans.X3DLightNode;
import de.grogra.ext.x3d.xmlbeans.X3DNode;
import de.grogra.ext.x3d.xmlbeans.X3DPrototype;
import de.grogra.ext.x3d.xmlbeans.X3DShapeNode;
import de.grogra.ext.x3d.xmlbeans.AppearanceDocument.Appearance;
import de.grogra.ext.x3d.xmlbeans.Arc2DDocument.Arc2D;
import de.grogra.ext.x3d.xmlbeans.BackgroundDocument.Background;
import de.grogra.ext.x3d.xmlbeans.BillboardDocument.Billboard;
import de.grogra.ext.x3d.xmlbeans.BoxDocument.Box;
import de.grogra.ext.x3d.xmlbeans.Circle2DDocument.Circle2D;
import de.grogra.ext.x3d.xmlbeans.ConeDocument.Cone;
import de.grogra.ext.x3d.xmlbeans.CylinderDocument.Cylinder;
import de.grogra.ext.x3d.xmlbeans.DirectionalLightDocument.DirectionalLight;
import de.grogra.ext.x3d.xmlbeans.ElevationGridDocument.ElevationGrid;
import de.grogra.ext.x3d.xmlbeans.ExternProtoDeclareDocument.ExternProtoDeclare;
import de.grogra.ext.x3d.xmlbeans.ExtrusionDocument.Extrusion;
import de.grogra.ext.x3d.xmlbeans.GroupDocument.Group;
import de.grogra.ext.x3d.xmlbeans.ISDocument.IS;
import de.grogra.ext.x3d.xmlbeans.IndexedFaceSetDocument.IndexedFaceSet;
import de.grogra.ext.x3d.xmlbeans.IndexedLineSetDocument.IndexedLineSet;
import de.grogra.ext.x3d.xmlbeans.IndexedTriangleFanSetDocument.IndexedTriangleFanSet;
import de.grogra.ext.x3d.xmlbeans.IndexedTriangleSetDocument.IndexedTriangleSet;
import de.grogra.ext.x3d.xmlbeans.IndexedTriangleStripSetDocument.IndexedTriangleStripSet;
import de.grogra.ext.x3d.xmlbeans.InlineDocument.Inline;
import de.grogra.ext.x3d.xmlbeans.LODDocument.LOD;
import de.grogra.ext.x3d.xmlbeans.LineSetDocument.LineSet;
import de.grogra.ext.x3d.xmlbeans.PointLightDocument.PointLight;
import de.grogra.ext.x3d.xmlbeans.PointSetDocument.PointSet;
import de.grogra.ext.x3d.xmlbeans.Polyline2DDocument.Polyline2D;
import de.grogra.ext.x3d.xmlbeans.Polypoint2DDocument.Polypoint2D;
import de.grogra.ext.x3d.xmlbeans.ProtoBodyDocument.ProtoBody;
import de.grogra.ext.x3d.xmlbeans.ProtoDeclareDocument.ProtoDeclare;
import de.grogra.ext.x3d.xmlbeans.ProtoInstanceDocument.ProtoInstance;
import de.grogra.ext.x3d.xmlbeans.Rectangle2DDocument.Rectangle2D;
import de.grogra.ext.x3d.xmlbeans.SceneDocument.Scene;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.SphereDocument.Sphere;
import de.grogra.ext.x3d.xmlbeans.SpotLightDocument.SpotLight;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.ext.x3d.xmlbeans.TriangleFanSetDocument.TriangleFanSet;
import de.grogra.ext.x3d.xmlbeans.TriangleSet2DDocument.TriangleSet2D;
import de.grogra.ext.x3d.xmlbeans.TriangleSetDocument.TriangleSet;
import de.grogra.ext.x3d.xmlbeans.TriangleStripSetDocument.TriangleStripSet;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.LightNode;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Phong;

public class X3DParser {

	/**
	 * Cursor for traverse x3d file.
	 */
	protected XmlCursor cursor = null;
	
	/**
	 * Currently parsed file.
	 */
	protected File file = null;
	
	/**
	 * Reference map, used for DEF and USE. Only used in
	 * current parser.
	 */
	private HashMap<String, Object> referenceMap = null;
	
	/** 
	 * Map of all proto declares.
	 */
	protected HashMap<String, ProtoDeclareImport> protoMap = null;
	
	public X3DParser(XmlTokenSource node, File file) {
		this.cursor = node.newCursor();
		this.file = file;
		this.referenceMap = new HashMap<String, Object>();
		this.protoMap = new HashMap<String, ProtoDeclareImport>();
	}
	
	public void parseScene(Object parent) {
		X3DImport.getTheImport().increaseProgress();
		
		if (parent == null)
			return;
		
		XmlObject obj = cursor.getObject();
		Object n = handleObject(parent, obj);

		if (cursor.toFirstChild()) {
			parseScene(n);
		}
		else
			return;
		
		while (cursor.toNextSibling()) {
			parseScene(n);
		}
		
		cursor.toParent();
	}
	
	protected Object handleObject(Object parent, XmlObject obj) {
		Object n = null;
		
		if (obj instanceof X3DNode)
			n = handleNode(parent, (X3DNode) obj);
		else if (obj instanceof SceneGraphStructureNodeType)
			n = handleSceneGraphStructureNodeType(parent, (SceneGraphStructureNodeType) obj);
		
		return n;
	}
	
	protected Object handleSceneGraphStructureNodeType(Object parent, SceneGraphStructureNodeType sgsnt) {
		Object n = null;
		
		if (sgsnt instanceof Scene)
			n = handleScene(parent, (Scene) sgsnt);
		else if (sgsnt instanceof X3DPrototype)
			n = handlePrototype(parent, (X3DPrototype) sgsnt);
		else if (sgsnt instanceof ProtoBody)
			n = handleProtobody(parent, (ProtoBody) sgsnt);
		else if (sgsnt instanceof IS)
			n = handleIS(parent, (IS) sgsnt);
		
		return n;
	}
	
	protected Object handleScene(Object parent, Scene scene) {
		return parent;
	}
	
	// return null, IS is handled in every node type
	protected Object handleIS(Object parent, IS is) {
		return null;
	}
	
	protected Object handlePrototype(Object parent, X3DPrototype x3dprototype) {
		Object n = null;
		
		if (x3dprototype instanceof ProtoDeclare)
			handleProtoDeclare(parent, (ProtoDeclare) x3dprototype);
		else if (x3dprototype instanceof ExternProtoDeclare)
			handleExternProtoDeclare(parent, (ExternProtoDeclare) x3dprototype);
		else if (x3dprototype instanceof ProtoInstance)
			handleProtoInstance(parent, (ProtoInstance) x3dprototype);
		
		return n;
	}
	
	protected Object handleProtobody(Object parent, ProtoBody protoBody) {
		return parent;
	}
	
	protected Object handleProtoDeclare(Object parent, ProtoDeclare protoDeclare) {
		ProtoDeclareImport.createInstance(protoDeclare);
		return null;
	}
	
	protected Object handleExternProtoDeclare(Object parent, ExternProtoDeclare externProtoDeclare) {
		ExternProtoDeclareImport.createInstance(externProtoDeclare);
		return null;
	}
	
	protected Object handleProtoInstance(Object parent, ProtoInstance protoInstance) {
		Node child = ProtoInstanceImport.createInstance(protoInstance);
		if (child != null)
			addChild(parent, child);
		return child;
	}
	
	protected Object handleNode(Object parent, X3DNode node) {
		Object n = null;
		
		if (node instanceof X3DChildNode)
			n = handleChildNode(parent, (X3DChildNode) node);
		else if (node instanceof X3DGeometryNode)
			n = handleGeometryNode(parent, (X3DGeometryNode) node);
		else if (node instanceof X3DAppearanceNode)
			n = handleAppearanceNode(parent, (X3DAppearanceNode) node);
		
		if (node.isSetUSE())
			// set to null so children of this node are not parsed
			n = null;
		
		return n;
	}
	
	protected Object handleGeometryNode(Object parent, X3DGeometryNode geometryNode) {
		Object n = null;

		if (geometryNode instanceof X3DComposedGeometryNode)
			n = handleComposedGeometryNode(parent, (X3DComposedGeometryNode) geometryNode);
		else if (geometryNode instanceof Box)
			n = handleBox(parent, (Box) geometryNode);
		else if (geometryNode instanceof Sphere)
			n = handleSphere(parent, (Sphere) geometryNode);
		else if (geometryNode instanceof Cylinder)
			n = handleCylinder(parent, (Cylinder) geometryNode);
		else if (geometryNode instanceof Cone)
			n = handleCone(parent, (Cone) geometryNode);
		else if (geometryNode instanceof ElevationGrid)
			n = handleElevationGrid(parent, (ElevationGrid) geometryNode);
		else if (geometryNode instanceof Extrusion)
			n = handleExtrusion(parent, (Extrusion) geometryNode);
		else if (geometryNode instanceof PointSet)
			n = handlePointSet(parent, (PointSet) geometryNode);
		else if (geometryNode instanceof IndexedLineSet)
			n = handleIndexedLineSet(parent, (IndexedLineSet) geometryNode);
		else if (geometryNode instanceof LineSet)
			n = handleLineSet(parent, (LineSet) geometryNode);
		else if (geometryNode instanceof Polypoint2D)
			n = handlePolypoint2D(parent, (Polypoint2D) geometryNode);
		else if (geometryNode instanceof Polyline2D)
			n = handlePolyline2D(parent, (Polyline2D) geometryNode);
		else if (geometryNode instanceof Rectangle2D)
			n = handleRectangle2D(parent, (Rectangle2D) geometryNode);
		else if (geometryNode instanceof TriangleSet2D)
			n = handleTriangleSet2D(parent, (TriangleSet2D) geometryNode);
		else if (geometryNode instanceof Arc2D)
			n = handleArc2D(parent, (Arc2D) geometryNode);
		else if (geometryNode instanceof Circle2D)
			n = handleCircle2D(parent, (Circle2D) geometryNode);
		
		return n;
	}
	
	protected Object handleComposedGeometryNode(Object parent, X3DComposedGeometryNode composedGeometryNode) {
		Object n = null;
		
		if (composedGeometryNode instanceof IndexedFaceSet)
			n = handleIndexedFaceSet(parent, (IndexedFaceSet) composedGeometryNode);
		else if (composedGeometryNode instanceof TriangleSet)
			n = handleTriangleSet(parent, (TriangleSet) composedGeometryNode);
		else if (composedGeometryNode instanceof TriangleFanSet)
			n = handleTriangleFanSet(parent, (TriangleFanSet) composedGeometryNode);
		else if (composedGeometryNode instanceof TriangleStripSet)
			n = handleTriangleStripSet(parent, (TriangleStripSet) composedGeometryNode);
		else if (composedGeometryNode instanceof IndexedTriangleSet)
			n = handleIndexedTriangleSet(parent, (IndexedTriangleSet) composedGeometryNode);
		else if (composedGeometryNode instanceof IndexedTriangleFanSet)
			n = handleIndexedTriangleFanSet(parent, (IndexedTriangleFanSet) composedGeometryNode);
		else if (composedGeometryNode instanceof IndexedTriangleStripSet)
			n = handleIndexedTriangleStripSet(parent, (IndexedTriangleStripSet) composedGeometryNode);
		
		return n;
	}
	
	protected Object handleBox(Object parent, Box box) {
		Node child = BoxImport.createInstance(box, parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleSphere(Object parent, Sphere sphere) {
		Node child = SphereImport.createInstance(sphere);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleCylinder(Object parent, Cylinder cylinder) {
		Node child = CylinderImport.createInstance(cylinder);
		addChild(parent, child);
		return child;
	}

	protected Object handleCone(Object parent, Cone cone) {
		Node child = ConeImport.createInstance(cone);
		addChild(parent, child);
		return child;
	}

	protected Object handleElevationGrid(Object parent, ElevationGrid elevationGrid) {
		Node child = ElevationGridImport.createInstance(elevationGrid);
		addChild(parent, child);
		return child;
	}

	protected Object handleIndexedFaceSet(Object parent, IndexedFaceSet indexedFaceSet) {
		Node child = IndexedFaceSetImport.createInstance(indexedFaceSet);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleExtrusion(Object parent, Extrusion extrusion) {
		Node child = ExtrusionImport.createInstance(extrusion);
		addChild(parent, child);
		return child;
	}
	
	protected Object handlePointSet(Object parent, PointSet pointSet) {
		Node child = PointSetImport.createInstance(pointSet, (Node) parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleIndexedLineSet(Object parent, IndexedLineSet lineSet) {
		Node child = IndexedLineSetImport.createInstance(lineSet, (Node) parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleLineSet(Object parent, LineSet lineSet) {
		Node child = LineSetImport.createInstance(lineSet, (Node) parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleTriangleSet(Object parent, TriangleSet triangleSet) {
		Node child = TriangleSetImport.createInstance(triangleSet, (Node) parent);
		addChild(parent, child);
		return child;
	}

	protected Object handleIndexedTriangleSet(Object parent, IndexedTriangleSet triangleSet) {
		Node child = IndexedTriangleSetImport.createInstance(triangleSet, (Node) parent);
		addChild(parent, child);
		return child;
	}

	protected Object handleTriangleFanSet(Object parent, TriangleFanSet triangleFanSet) {
		Node child = TriangleFanSetImport.createInstance(triangleFanSet, (Node) parent);
		addChild(parent, child);
		return child;
	}

	protected Object handleIndexedTriangleFanSet(Object parent, IndexedTriangleFanSet triangleFanSet) {
		Node child = IndexedTriangleFanSetImport.createInstance(triangleFanSet, (Node) parent);
		addChild(parent, child);
		return child;
	}

	protected Object handleTriangleStripSet(Object parent, TriangleStripSet triangleStripSet) {
		Node child = TriangleStripSetImport.createInstance(triangleStripSet, (Node) parent);
		addChild(parent, child);
		return child;
	}

	protected Object handleIndexedTriangleStripSet(Object parent, IndexedTriangleStripSet triangleStripSet) {
		Node child = IndexedTriangleStripSetImport.createInstance(triangleStripSet, (Node) parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handlePolypoint2D(Object parent, Polypoint2D polypoint) {
		Node child = Polypoint2DImport.createInstance(polypoint, (Node) parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handlePolyline2D(Object parent, Polyline2D polyline) {
		Node child = Polyline2DImport.createInstance(polyline, (Node) parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleRectangle2D(Object parent, Rectangle2D rectangle) {
		Node child = Rectangle2DImport.createInstance(rectangle, (Node) parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleTriangleSet2D(Object parent, TriangleSet2D triangleSet) {
		Node child = TriangleSet2DImport.createInstance(triangleSet, (Node) parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleArc2D(Object parent, Arc2D arc) {
		Node child = Arc2DImport.createInstance(arc, (Node) parent);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleCircle2D(Object parent, Circle2D circle) {
		Node child = Circle2DImport.createInstance(circle, (Node) parent);
		addChild(parent, child);
		return child;
	}

	protected Object handleAppearanceNode(Object parent, X3DAppearanceNode appearanceNode) {
		Object n = null;
		
		if (appearanceNode instanceof Appearance)
			n = handleAppearance(parent, (Appearance) appearanceNode);
		
		return n;
	}
	
	protected Object handleAppearance(Object parent, Appearance appearance) {
		Phong child = AppearanceImport.createInstance(appearance);
		((ShadedNull) parent).setShader(child);
		return null;
	}
	
	protected Object handleChildNode(Object parent, X3DChildNode childNode) {
		Object n = null;
		
		if (childNode instanceof X3DGroupingNode)
			n = handleGroupingNode(parent, (X3DGroupingNode) childNode);
		else if (childNode instanceof X3DShapeNode)
			n = handleShapeNode(parent, (X3DShapeNode) childNode);
		else if (childNode instanceof X3DLightNode)
			n = handleLightNode(parent, (X3DLightNode) childNode);
		else if (childNode instanceof Inline)
			n = handleInline(parent, (Inline) childNode);
		else if (childNode instanceof X3DBindableNode)
			n = handleBindableNode(parent, (X3DBindableNode) childNode);
		
		return n;
	}
	
	protected Object handleBindableNode(Object parent, X3DBindableNode bindableNode) {
		Object n = null;
		
		if (bindableNode instanceof X3DBackgroundNode)
			n = handleBackgroundNode(parent, (X3DBackgroundNode) bindableNode);
		
		return n;
	}
	
	protected Object handleBackgroundNode(Object parent, X3DBackgroundNode backgroundNode) {
		Object n = null;
		
		if (backgroundNode instanceof Background)
			handleBackground(parent, (Background) backgroundNode);
		
		return n;
	}
	
	protected Object handleBackground(Object parent, Background background) {
		Node child = BackgroundImport.createInstance(background);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleInline(Object parent, Inline inline) {
		Node child = InlineImport.createInstance(inline);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleLightNode(Object parent, X3DLightNode lightNode) {
		Object n = null;
		
		if (lightNode instanceof PointLight)
			n = handlePointLight(parent, (PointLight) lightNode);
		else if (lightNode instanceof DirectionalLight)
			n = handleDirectionalLight(parent, (DirectionalLight) lightNode);
		else if (lightNode instanceof SpotLight)
			n = handleSpotLight(parent, (SpotLight) lightNode);
		
		return n;
	}
	
	protected Object handlePointLight(Object parent, PointLight pointLight) {
		LightNode child = PointLightImport.createInstance(pointLight);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleDirectionalLight(Object parent, DirectionalLight directionalLight) {
		LightNode child = DirectionalLightImport.createInstance(directionalLight);
		addChild(parent, child);
		return child;
	}

	protected Object handleSpotLight(Object parent, SpotLight spotLight) {
		LightNode child = SpotLightImport.createInstance(spotLight);
		addChild(parent, child);
		return child;
	}

	protected Object handleGroupingNode(Object parent, X3DGroupingNode groupingNode) {
		Object n = null;
		
		if (groupingNode instanceof Transform)
			n = handleTransform(parent, (Transform) groupingNode);
		else if (groupingNode instanceof Group)
			n = handleGroup(parent, (Group) groupingNode);
		else if (groupingNode instanceof LOD)
			n = handleLOD(parent, (LOD) groupingNode);
		else if (groupingNode instanceof Billboard)
			n = handleBillboard(parent, (Billboard) groupingNode);
		
		return n;
	}
	
	protected Object handleShapeNode(Object parent, X3DShapeNode shapeNode) {
		Object n = null;
		
		if (shapeNode instanceof Shape)
			n = handleShape(parent, (Shape) shapeNode);
		
		return n;
	}
	
	protected Object handleTransform(Object parent, Transform transformNode) {
		Node child = TransformImport.createInstance(transformNode);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleGroup(Object parent, Group groupNode) {
		Node child = GroupImport.createInstance(groupNode);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleLOD(Object parent, LOD lod) {
		Node child = LODImport.createInstance(lod);
		addChild(parent, child);
		return null; // children are parsed in LODImport
	}
	
	protected Object handleBillboard(Object parent, Billboard billboard) {
		Node child = BillboardImport.createInstance(billboard);
		addChild(parent, child);
		return child;
	}
	
	protected Object handleShape(Object parent, Shape shape) {
		Node child = ShapeImport.createInstance(shape);
		addChild(parent, child);
		return child;		
	}
	
	protected void addChild(Object parent, Node child) {
		((Node) parent).addEdgeBitsTo(child, Graph.BRANCH_EDGE, null);
	}
	
	/**
	 * Returns the url of the x3d file to parse.
	 * @return
	 */
	public File getFile() {
		return file;
	}
	
	/**
	 * Returns the current reference map.
	 * @return
	 */
	public HashMap<String, Object> getReferenceMap() {
		return referenceMap;
	}
	
	/**
	 * Returns the current proto declare map.
	 * @return
	 */
	public HashMap<String, ProtoDeclareImport> getProtoMap() {
		return protoMap;
	}
}
