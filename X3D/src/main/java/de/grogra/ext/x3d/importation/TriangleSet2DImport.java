package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.TriangleSet2DDocument.TriangleSet2D;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class TriangleSet2DImport {

	public static ShadedNull createInstance(TriangleSet2D triangleSet, Node parent) {
		MeshNode n = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (triangleSet.isSetUSE()) {
			try {
				n = (MeshNode) ((Node) referenceMap.get(triangleSet.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new MeshNode();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (triangleSet.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, triangleSet);
			}
			
			setValues(n, triangleSet, finalValueMap);
			if (triangleSet.isSetDEF()) {
				referenceMap.put(triangleSet.getDEF(), n);
			}
		}
		
		n.setName("X3DTriangleSet2D");
		return n;
	}
	
	private static void setValues(MeshNode node, TriangleSet2D triangleSet, HashMap<String, String> valueMap) {
		// vertices
		float[] vertices = null;
		String verticesString = valueMap.containsKey("vertices") ? valueMap.get("vertices") : triangleSet.getVertices();
		vertices = Util.splitStringToArrayOfFloat(verticesString, new float[]{});
		
		// solid
		boolean solid;
		solid = valueMap.containsKey("solid") ?
				Boolean.valueOf(valueMap.get("solid")) :
					triangleSet.isSetSolid() ?
							triangleSet.getSolid() :
								false;

		PolygonMesh p = new PolygonMesh();
		
		IntList indexData = new IntList();
		FloatList vertexData = new FloatList();
		FloatList textureData = new FloatList();

		int vertexLength = vertices.length;
		int index = 0;
		
		float minX = 0;
		float maxX = 0;
		float minY = 0;
		float maxY = 0;
		
		// calc dimension for triangle set
		for (int i = 0; i < vertexLength; i++) {
			if (i % 2 == 0) {
				if (vertices[i] < minX)
					minX = vertices[i];
				else if (vertices[i] > maxX)
					maxX = vertices[i];
			}
			else {
				if (vertices[i] < minY)
					minY = vertices[i];
				else if (vertices[i] > maxY)
					maxY = vertices[i];
			}
		}
	
		for (int i = 0; i < vertexLength; i=i+2) {
			float x = vertices[i+0];
			float y = vertices[i+1];

			// set index data
			indexData.add(index++);
			
			// set vertices
			vertexData.add(x);
			vertexData.add(0);
			vertexData.add(y);
			
			// set texture coordinates
			float u = (x - minX) / (maxX - minX);
			float v = (y - minY) / (maxY - minY);
			textureData.add(u);
			textureData.add(v);
		}
		
		p.setIndexData(indexData);
		p.setVertexData(vertexData);
		p.setTextureData(textureData.elements);
		
		node.setVisibleSides(solid ? 0 : 2);
		
		node.setPolygons(p);

	}
	
}
