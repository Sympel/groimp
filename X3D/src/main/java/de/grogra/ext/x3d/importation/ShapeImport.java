package de.grogra.ext.x3d.importation;

import java.util.HashMap;

import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.ShadedNull;

public class ShapeImport {

	public static Node createInstance(Shape shape) {
		ShadedNull s = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (shape.isSetUSE()) {
			try {
				s = (ShadedNull) ((Node) referenceMap.get(shape.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			s = new ShadedNull();
			if (shape.isSetDEF()) {
				referenceMap.put(shape.getDEF(), s);
			}
		}
		s.setName("X3DShape");
		return s;
	}
	
}
