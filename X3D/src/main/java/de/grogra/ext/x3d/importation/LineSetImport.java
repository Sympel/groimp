package de.grogra.ext.x3d.importation;

import java.util.HashMap;

import javax.vecmath.Point3f;

import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.LineSetDocument.LineSet;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Line;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.math.ChannelMap;
import de.grogra.math.RGBColor;

public class LineSetImport {

	public static Null createInstance(LineSet lineSet, Node parent) {
		Null n = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (lineSet.isSetUSE()) {
			try {
				n = (Null) ((Node) referenceMap.get(lineSet.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new Null();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (lineSet.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, lineSet);
			}
			
			setValues(n, lineSet, finalValueMap, parent);
			if (lineSet.isSetDEF()) {
				referenceMap.put(lineSet.getDEF(), n);
			}
		}
		
		n.setName("X3DLineSet");
		return n;
	}
	
	private static void setValues(Null node, LineSet lineSet, HashMap<String, String> valueMap, Node parent) {
		// coord
		float[] coord = null;
		if (lineSet.getCoordinate() != null)
			coord = CoordinateImport.createInstance(lineSet.getCoordinate());
		
		// color
		// not usable because color effects vertices
//		float[] color = null;
//		if (lineSet.getColor() != null)
//			color = ColorImport.createInstance(lineSet.getColor());

		// vertexCount
		int[] vertexCount;
		String vertexCountString = valueMap.containsKey("vertexCount") ? valueMap.get("vertexCount") : lineSet.getVertexCount();
		vertexCount = Util.splitStringToArrayOfInt(vertexCountString, new int[]{});
		
		// create lines
		int pointCount = (int) (coord.length / 3.0f);
		// convert coordinates to GroIMP space
		Point3f[] coords = new Point3f[pointCount];
		for (int i = 0; i < pointCount; i++) {
			coords[i] = new Point3f( coord[3*i+0],
									-coord[3*i+2],
									 coord[3*i+1]);
		}
		
		RGBColor lineColor = new RGBColor(0.0f, 0.0f, 0.0f);
		
		// use color from material node (if exists)
		if (parent instanceof ShadedNull) {
			Shader s = ((ShadedNull) parent).getShader();
			if (s instanceof Phong) {
				ChannelMap cm = ((Phong) s).getEmissive();
				if (cm instanceof RGBColor) {
					lineColor = (RGBColor) cm;
				}
			}
		}
		
		pointCount = 0;
		for (int i = 0; i < vertexCount.length; i++) {
			// loop over vertexCount
			
			for (int j = 1; j < vertexCount[i]; j++) {
				// loop over vertices
				float x = coords[pointCount].x;
				float y = coords[pointCount].y;
				float z = coords[pointCount].z;
				float dx = coords[pointCount+1].x - coords[pointCount].x;
				float dy = coords[pointCount+1].y - coords[pointCount].y;
				float dz = coords[pointCount+1].z - coords[pointCount].z;
				Line newLine = new Line(x, y, z, dx, dy, dz);
				
				newLine.setName("X3DLine");
				newLine.setColor(lineColor);
				node.addEdgeBitsTo(newLine, Graph.BRANCH_EDGE, null);
				pointCount++;
			}
			pointCount++;
		}
	}
}
