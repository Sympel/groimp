package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import javax.vecmath.Color3f;
import javax.vecmath.Tuple3f;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.PointSetDocument.PointSet;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.math.ChannelMap;
import de.grogra.math.RGBColor;

public class PointSetImport {

	public static Null createInstance(PointSet pointSet, Node parent) {
		Null n = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (pointSet.isSetUSE()) {
			try {
				n = (Null) ((Node) referenceMap.get(pointSet.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new Null();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (pointSet.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, pointSet);
			}
			
			setValues(n, pointSet, finalValueMap, parent);
			if (pointSet.isSetDEF()) {
				referenceMap.put(pointSet.getDEF(), n);
			}
		}
		
		n.setName("X3DPointSet");
		return n;
	}
	
	private static void setValues(Null node, PointSet pointSet, HashMap<String, String> valueMap, Node parent) {
		// coord
		float[] coord = null;
		if (pointSet.getCoordinate() != null)
			coord = CoordinateImport.createInstance(pointSet.getCoordinate());
		
		// color
		float[] color = null;
		if (pointSet.getColor() != null)
			color = ColorImport.createInstance(pointSet.getColor());
		
		// if no colorNode is given create one with values of sibling appearance node
		// neccessary because Point in GroIMP doesn't inherit the color of parent
		if (color == null) {
			color = new float[coord.length];
			Tuple3f appColor = new Color3f(0, 0, 0);
			
			if (parent instanceof ShadedNull) {
				Shader s = ((ShadedNull) parent).getShader();
				if (s instanceof Phong) {
					ChannelMap cm = ((Phong) s).getEmissive();
					if (cm instanceof RGBColor) {
						appColor = (RGBColor) cm; 
					}
				}
			}
			
			for (int i = 0; i < coord.length; i=i+3) {
				color[i] = appColor.x;
				color[i+1] = appColor.y;
				color[i+2] = appColor.z;
			}
		}
		
		// create points, set translations
		int pointCount = coord.length;
		for (int i = 0; i < pointCount; i=i+3) {
			de.grogra.imp3d.objects.Point newPoint = new de.grogra.imp3d.objects.Point();
			newPoint.setTranslation(coord[i], -coord[i+2], coord[i+1]);
			newPoint.setColor(color[i], color[i+1], color[i+2]);
			newPoint.setName("X3DPoint");
			node.addEdgeBitsTo(newPoint, Graph.BRANCH_EDGE, null);
		}
	}
	
}
