package de.grogra.ext.x3d.util;

import java.util.ArrayList;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class ExtrusionComputation {
	
	public static class CrossSection {
		protected ArrayList<Point3f> vertices = null;
		
		public CrossSection() {
			vertices = new ArrayList<Point3f>();
		}

		public ArrayList<Point3f> getVertices() {
			return vertices;
		}

		public void setVertices(ArrayList<Point3f> vertices) {
			this.vertices = vertices;
		}
	}
		
	private final IntList coordIndexList;
	private final FloatList vertexList;
	private final FloatList normalList;
	private final FloatList uvList;

	private final float[] crossSection;
	private final boolean ccw;
	
	private final boolean isCrossSectionClosed;
	private final boolean isSpineClosed;
	private final int spineLength;
	private final int crossSectionLength;
	private final int spineCount;
	
	public ExtrusionComputation(IntList coordIndexList, FloatList vertexList,
			FloatList normalList, FloatList uvList, float[] crossSection, float[] spine, boolean ccw) {
		this.coordIndexList = coordIndexList;
		this.vertexList = vertexList;
		this.normalList = normalList;
		this.uvList = uvList;
		
		this.crossSection = crossSection;
		this.ccw = ccw;
	
		// calculate some variables
		crossSectionLength = crossSection.length;
		spineLength = spine.length;
		spineCount = spineLength / 3;
		
		if ((crossSection[0] == crossSection[crossSectionLength-2]) &&
				(crossSection[1] == crossSection[crossSectionLength-1]))
			isCrossSectionClosed = true;
		else
			isCrossSectionClosed = false;
		
		if ((spine[0] == spine[spineLength-3]) &&
				(spine[1] == spine[spineLength-2]) && 
				(spine[2] == spine[spineLength-1]))
			isSpineClosed = true;
		else
			isSpineClosed = false;
	}
	
	public void createCrossSections(float[] newScale, float[] newOrientation, float[] newSpine,
			ArrayList<CrossSection> cs, boolean pointsOnLine) {
		Vector3d previousZAxis = null;
		for (int j = 0; j < spineCount; j++) {
			CrossSection newCS = new CrossSection();
			
			ArrayList<Point3f> vertList = new ArrayList<Point3f>();
			for (int i = 0; i < crossSectionLength; i=i+2) {
				// create point in y=0 plane
				Point3f point = new Point3f(crossSection[i], crossSection[i+1], 0);
				
				// scale point
				point.x *= newScale[j*2+0];
				point.y *= newScale[j*2+1];
			
				// rotate point through orienting at spine
				Vector3d yAxis = null;
				Vector3d zAxis = null;

				// test if entire spline is collinear
				if (pointsOnLine) {
					// rotate point through orientation attribute
					AxisAngle4d rot = new AxisAngle4d(newOrientation[j*4 + 0], newOrientation[j*4 + 1],
							newOrientation[j*4 + 2], newOrientation[j*4 + 3]);
					Matrix4d orientMatrix = new Matrix4d();
					orientMatrix.setIdentity();
					orientMatrix.set(rot);
					orientMatrix.transform(point);
					
					// find yAxis
					yAxis = new Vector3d(newSpine[spineLength-3] - newSpine[0],
							newSpine[spineLength-2] - newSpine[1],
							newSpine[spineLength-1] - newSpine[2]);
				
					Matrix4d transMatrix = Util.vectorsToTransMatrix(new Vector3d(0, 0, 1), yAxis);
					transMatrix.transform(point);
					
				}
				else {
					// rotate point through orientation attribute
					AxisAngle4d rot = new AxisAngle4d(newOrientation[j*4 + 0], newOrientation[j*4 + 1],
							newOrientation[j*4 + 2], -newOrientation[j*4 + 3]);
					Matrix4d orientMatrix = new Matrix4d();
					orientMatrix.setIdentity();
					orientMatrix.set(rot);
					orientMatrix.transform(point);
					
					if (j == 0) {
						// first spine
						if (isSpineClosed) {
							// calculate y-axis from second spine point to spine point before last
							yAxis = new Vector3d(newSpine[3] - newSpine[spineLength-6],
									newSpine[4] - newSpine[spineLength-5],
									newSpine[5] - newSpine[spineLength-4]);
							zAxis = new Vector3d();
							Vector3d vec1 = new Vector3d(newSpine[3] - newSpine[0],
									newSpine[4] - newSpine[1],
									newSpine[5] - newSpine[2]);
							Vector3d vec2 = new Vector3d(newSpine[spineLength-6] - newSpine[0],
									newSpine[spineLength-5] - newSpine[1],
									newSpine[spineLength-4] - newSpine[2]);
							zAxis.cross(vec1, vec2);
						}
						else {
							// calculate y-axis from second spine point to first spine point
							yAxis = new Vector3d(newSpine[3] - newSpine[0],
									newSpine[4] - newSpine[1],
									newSpine[5] - newSpine[2]);
							zAxis = new Vector3d();
							Vector3d vec2 = new Vector3d(newSpine[0] - newSpine[3],
									newSpine[1] - newSpine[4],
									newSpine[2] - newSpine[5]);
							Vector3d vec1 = new Vector3d(newSpine[6] - newSpine[3],
									newSpine[7] - newSpine[4],
									newSpine[8] - newSpine[5]);
							zAxis.cross(vec1, vec2);
						}
						// if cross = 0 take z-axis from first spine point with z-axis
						int k = 0;
						while (zAxis.equals(new Vector3d(0, 0, 0))) {
							k++;
							
							Vector3d vec1 = new Vector3d(newSpine[k*3+3] - newSpine[k*3+0],
									newSpine[k*3+4] - newSpine[k*3+1],
									newSpine[k*3+5] - newSpine[k*3+2]);
							Vector3d vec2 = new Vector3d(newSpine[k*3-3] - newSpine[k*3+0],
									newSpine[k*3-2] - newSpine[k*3+1],
									newSpine[k*3-1] - newSpine[k*3+2]);
							zAxis.cross(vec1, vec2);
							
						}
						
					}
					else if (j == spineCount - 1) {
						// last spine
						if (isSpineClosed) {
							// calculate y-axis from second spine point to spine point before last
							yAxis = new Vector3d(newSpine[3] - newSpine[spineLength-6],
									newSpine[4] - newSpine[spineLength-5],
									newSpine[5] - newSpine[spineLength-4]);
							zAxis = new Vector3d();
							Vector3d vec1 = new Vector3d(newSpine[3] - newSpine[0],
									newSpine[4] - newSpine[1],
									newSpine[5] - newSpine[2]);
							Vector3d vec2 = new Vector3d(newSpine[spineLength-6] - newSpine[0],
									newSpine[spineLength-5] - newSpine[1],
									newSpine[spineLength-4] - newSpine[2]);
							zAxis.cross(vec1, vec2);
						}
						else {
							// calculate y-axis from second spine point to first spine point
							yAxis = new Vector3d(newSpine[spineLength-3] - newSpine[spineLength-6],
									newSpine[spineLength-2] - newSpine[spineLength-5],
									newSpine[spineLength-1] - newSpine[spineLength-4]);
							zAxis = new Vector3d();
							Vector3d vec1 = new Vector3d(newSpine[spineLength-3] - newSpine[spineLength-6],
									newSpine[spineLength-2] - newSpine[spineLength-5],
									newSpine[spineLength-1] - newSpine[spineLength-4]);
							Vector3d vec2 = new Vector3d(newSpine[spineLength-9] - newSpine[spineLength-6],
									newSpine[spineLength-8] - newSpine[spineLength-5],
									newSpine[spineLength-7] - newSpine[spineLength-4]);
							zAxis.cross(vec1, vec2);
						}
					}
					else {
						// spine in the middle
						yAxis = new Vector3d(newSpine[j*3+3] - newSpine[j*3-3],
								newSpine[j*3+4] - newSpine[j*3-2],
								newSpine[j*3+5] - newSpine[j*3-1]);					
						zAxis = new Vector3d();
						Vector3d vec1 = new Vector3d(newSpine[j*3+3] - newSpine[j*3+0],
								newSpine[j*3+4] - newSpine[j*3+1],
								newSpine[j*3+5] - newSpine[j*3+2]);
						Vector3d vec2 = new Vector3d(newSpine[j*3-3] - newSpine[j*3+0],
								newSpine[j*3-2] - newSpine[j*3+1],
								newSpine[j*3-1] - newSpine[j*3+2]);
						zAxis.cross(vec1, vec2);
					}
				
				
					yAxis.normalize();
					// if zAxis could not be calculated take zAxis from previous spine
					if (zAxis.equals(new Vector3d(0, 0, 0))) {
						zAxis = previousZAxis;
					}
					else
						zAxis.normalize();

					if (previousZAxis == null) {
						previousZAxis = new Vector3d();
					}
					else {
						double dot = zAxis.dot(previousZAxis);
						if (dot < 0)
							zAxis.negate();
					}
					previousZAxis = zAxis;
					
					Vector3d xAxis = new Vector3d();
					xAxis.cross(yAxis, zAxis);
					xAxis.normalize();
					
					Matrix4d transMatrix = new Matrix4d();
					transMatrix.setIdentity();
					transMatrix.setColumn(0, xAxis.x, xAxis.y, xAxis.z, 0);
					transMatrix.setColumn(1, zAxis.x, zAxis.y, zAxis.z, 0);
					transMatrix.setColumn(2, yAxis.x, yAxis.y, yAxis.z, 0);
					
					transMatrix.transform(point);
				}
				
				// translate point to spine point
				point.x += newSpine[j*3+0];
				point.y += newSpine[j*3+1];
				point.z += newSpine[j*3+2];
			
				vertList.add(point);
			}			
			newCS.setVertices(vertList);
			cs.add(newCS);
		}
	}
	
	public int createCap(int k, ArrayList<Point3f> vertList, boolean beginCap) {
	
		int end = vertList.size() - (isCrossSectionClosed ? 2 : 1);
		
		// calculate normal
		Vector3f vec1 = new Vector3f(vertList.get(0).x - vertList.get(1).x,
									 vertList.get(0).y - vertList.get(1).y,
									 vertList.get(0).z - vertList.get(1).z);
		
		Vector3f vec2 = new Vector3f(vertList.get(2).x - vertList.get(1).x,
				 					 vertList.get(2).y - vertList.get(1).y,
				 					 vertList.get(2).z - vertList.get(1).z);
		Vector3f normal = new Vector3f();
		if (beginCap ^ !ccw)
			normal.cross(vec1, vec2);
		else
			normal.cross(vec2, vec1);
		normal.normalize();
	
		// calculate uv's
		float minx = crossSection[0],
			  miny = crossSection[1],
			  maxx = crossSection[0],
			  maxy = crossSection[1];
		for (int i = 1; i < end; i++) {
			float x = crossSection[2*i];
			float y = crossSection[2*i+1];
			minx = Math.min(minx, x);
			miny = Math.min(miny, y);
			maxx = Math.max(maxx, x);
			maxy = Math.max(maxy, y);
		}
		
		// iterate over cross section points
		for (int i = 1; i <  end; i++) {
			coordIndexList.add(k + 0);
			coordIndexList.add(k + (ccw ? 1 : 2));
			coordIndexList.add(k + (ccw ? 2 : 1));
			k += 3;
			
			vertexList.add(vertList.get(0).x);
			vertexList.add(vertList.get(0).y);
			vertexList.add(vertList.get(0).z);
			normalList.add(normal.x);
			normalList.add(normal.y);
			normalList.add(normal.z);
			uvList.add((crossSection[0] - minx) / (maxx - minx));
			uvList.add((crossSection[1] - miny) / (maxy - miny));
			
			int j = i + (beginCap ? 1 : 0);
			vertexList.add(vertList.get(j).x);
			vertexList.add(vertList.get(j).y);
			vertexList.add(vertList.get(j).z);
			normalList.add(normal.x);
			normalList.add(normal.y);
			normalList.add(normal.z);
			uvList.add((crossSection[2*(j)+0] - minx) / (maxx - minx));
			uvList.add((crossSection[2*(j)+1] - miny) / (maxy - miny));
			
			j = i + (beginCap ? 0 : 1);
			vertexList.add(vertList.get(j).x);
			vertexList.add(vertList.get(j).y);
			vertexList.add(vertList.get(j).z);
			normalList.add(normal.x);
			normalList.add(normal.y);
			normalList.add(normal.z);
			uvList.add((crossSection[2*(j)+0] - minx) / (maxx - minx));
			uvList.add((crossSection[2*(j)+1] - miny) / (maxy - miny));
		}
		
		return k;
	}
	
	public int createSides(ArrayList<CrossSection> cs, int k) {
		
		for (int j = 0; j < spineCount - 1; j++) {
			ArrayList<Point3f> vert1List = cs.get(j+0).getVertices();
			ArrayList<Point3f> vert2List = cs.get(j+1).getVertices();
			
			int end = vert1List.size() - 1;
			
			for (int i = 0; i < end; i++) {
				Vector3f vec1 = new Vector3f(vert1List.get(i+1).x - vert1List.get(i).x,
											 vert1List.get(i+1).y - vert1List.get(i).y,
											 vert1List.get(i+1).z - vert1List.get(i).z);

				Vector3f vec2 = new Vector3f(vert2List.get(i).x - vert1List.get(i).x,
					 					 	 vert2List.get(i).y - vert1List.get(i).y,
					 					 	 vert2List.get(i).z - vert1List.get(i).z);
				
				Vector3f normal = new Vector3f();
				if (ccw)
					normal.cross(vec1, vec2);
				else
					normal.cross(vec2, vec1);
				normal.normalize();
				
				float endf = end;
				float spinef = spineCount - 1;
				
				coordIndexList.add(k + 0);
				coordIndexList.add(k + (ccw ? 1 : 2));
				coordIndexList.add(k + (ccw ? 2 : 1));				
				coordIndexList.add(k + 3);
				coordIndexList.add(k + (ccw ? 4 : 5));
				coordIndexList.add(k + (ccw ? 5 : 4));
				k += 6;
				
				vertexList.add(vert1List.get(i).x);
				vertexList.add(vert1List.get(i).y);
				vertexList.add(vert1List.get(i).z);
				normalList.add(normal.x);
				normalList.add(normal.y);
				normalList.add(normal.z);
				uvList.add((i + 0) / endf);
				uvList.add((j + 0) / spinef);
			

				vertexList.add(vert1List.get(i+1).x);
				vertexList.add(vert1List.get(i+1).y);
				vertexList.add(vert1List.get(i+1).z);
				normalList.add(normal.x);
				normalList.add(normal.y);
				normalList.add(normal.z);
				uvList.add((i + 1) / endf);
				uvList.add((j + 0) / spinef);
				

				vertexList.add(vert2List.get(i).x);
				vertexList.add(vert2List.get(i).y);
				vertexList.add(vert2List.get(i).z);
				normalList.add(normal.x);
				normalList.add(normal.y);
				normalList.add(normal.z);
				uvList.add((i + 0) / endf);
				uvList.add((j + 1) / spinef);
				

				vertexList.add(vert1List.get(i+1).x);
				vertexList.add(vert1List.get(i+1).y);
				vertexList.add(vert1List.get(i+1).z);
				normalList.add(normal.x);
				normalList.add(normal.y);
				normalList.add(normal.z);
				uvList.add((i + 1) / endf);
				uvList.add((j + 0) / spinef);
			

				vertexList.add(vert2List.get(i+1).x);
				vertexList.add(vert2List.get(i+1).y);
				vertexList.add(vert2List.get(i+1).z);
				normalList.add(normal.x);
				normalList.add(normal.y);
				normalList.add(normal.z);
				uvList.add((i + 1) / endf);
				uvList.add((j + 1) / spinef);
				

				vertexList.add(vert2List.get(i).x);
				vertexList.add(vert2List.get(i).y);
				vertexList.add(vert2List.get(i).z);
				normalList.add(normal.x);
				normalList.add(normal.y);
				normalList.add(normal.z);
				uvList.add((i + 0) / endf);
				uvList.add((j + 1) / spinef);
			}
		}
		
		return k;
	}

}
