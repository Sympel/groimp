package de.grogra.ext.x3d.importation;

import java.util.HashMap;

import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.GroupDocument.Group;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.impl.Node;

public class GroupImport {

	public static Node createInstance(Group group) {
		Node n = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (group.isSetUSE()) {
			try {
				n = ((Node) referenceMap.get(group.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new Node();
			if (group.isSetDEF()) {
				referenceMap.put(group.getDEF(), n);
			}
		}
		n.setName("X3DGroup");
		return n;
	}
}
