package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.ConeDocument.Cone;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.ShadedNull;

public class ConeImport {

	public static ShadedNull createInstance(Cone cone) {
		de.grogra.imp3d.objects.Cone c = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (cone.isSetUSE()) {
			try {
				c = (de.grogra.imp3d.objects.Cone) ((Node) referenceMap.get(cone.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			c = new de.grogra.imp3d.objects.Cone();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (cone.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, cone);
			}
			
			setValues(c, cone, finalValueMap);
			if (cone.isSetDEF()) {
				referenceMap.put(cone.getDEF(), c);
			}				
		}
	
		c.setName("X3DCone");
		return c;
	}
	
	private static void setValues(de.grogra.imp3d.objects.Cone node, Cone cone, HashMap<String, String> valueMap) {
		// bottomRadius
		float radius;
		radius = (valueMap.get("bottomRadius") != null) ?
				Float.valueOf(valueMap.get("bottomRadius")) :
					cone.isSetBottomRadius() ?
						cone.getBottomRadius() :
							1f;
		node.setRadius(radius);
		
		// height
		float height;
		height = (valueMap.get("height") != null) ?
				Float.valueOf(valueMap.get("height")) :
					cone.isSetHeight() ?
						cone.getHeight() :
							2f;
		node.setLength(height);
		
		// bottom
		boolean bottom;
		bottom = (valueMap.get("bottom") != null) ?
				Boolean.valueOf(valueMap.get("bottom")) :
					cone.isSetBottom() ? 
						cone.getBottom() :
							true;
		node.setOpen(!bottom);
		
		// groimp-specific attributes
		node.setStartPosition(-0.5f);
		node.setEndPosition(0);
	}
	
}
