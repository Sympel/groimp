package de.grogra.ext.x3d.exportation;

import java.io.IOException;
import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.xmlbeans.CoordinateDocument.Coordinate;
import de.grogra.ext.x3d.xmlbeans.IndexedFaceSetDocument.IndexedFaceSet;
import de.grogra.ext.x3d.xmlbeans.NormalDocument.Normal;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.TextureCoordinateDocument.TextureCoordinate;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.graph.ContextDependent;
import de.grogra.graph.GraphState;
import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.Polygonizable;
import de.grogra.imp3d.Polygonization;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.io.FilterSource.MetaDataKey;
import de.grogra.xl.util.ByteList;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class IndexedFaceSetExport extends BaseExport {

	public final PolygonArray polygons = new PolygonArray ();
	
	@Override
	protected void exportImpl(Leaf node, X3DExport export, Shape shapeNode, Transform transformNode)
			throws IOException {
		GraphState gs = export.getGraphState ();
		
		gs.setObjectContext (node.object, node.asNode);
		ContextDependent c = ((Polygonizable) node.getObject (de.grogra.imp3d.objects.Attributes.SHAPE))
			.getPolygonizableSource (gs);
		
		gs.setObjectContext (node.object, node.asNode);
		((Polygonizable) node.getObject
		 (de.grogra.imp3d.objects.Attributes.SHAPE)).getPolygonization ().polygonize
			(c, gs, polygons,
			 Polygonization.COMPUTE_NORMALS | Polygonization.COMPUTE_UV,
			 export.getMetaData (new MetaDataKey<Float> ("flatness"), 1f));
		
		IndexedFaceSet ifsNode = shapeNode.addNewIndexedFaceSet();
	
		// how many dimensions has a vertex, should be always 3
		int dim = polygons.dimension;
		// how many vertices has a polyon
		int ec = polygons.edgeCount;
		
		// index list for vertices, separated at ec
		IntList polyList = polygons.polygons;
		// list of float values for vertices, separated at dim
		FloatList vertList = polygons.vertices;
		// list of float values for texture coordinates, separated at 2
		FloatList uvList = polygons.uv;
		// list of float values for normals, separated at 3
		ByteList normalList = polygons.normals;
		
		// attribute for coordIndex
		StringBuilder coordIndex = new StringBuilder();
		// attribute for vertices
		StringBuilder coordPoint = new StringBuilder();
		// attribute for uv coordinates
		StringBuilder uvPoint = new StringBuilder();
		// attribute for normals
		StringBuilder normalVector = new StringBuilder();
		
		// node for vertices
		Coordinate coordNode = ifsNode.addNewCoordinate();
		// vertex coordinates
		float[] coordPoints = vertList.elements;
		
		// uv coordinates
		float[] uvPoints = uvList.size > 0 ? uvList.elements : null;
		
		// normal vectors
		byte[] normalVectors = normalList.size > 0 ? normalList.elements : null;
		
		// if ec == 4 transfer polyList to newPolyList with ec = 3
		if (ec == 4) {
			IntList newPolyList = new IntList((polyList.size - 2) * 3); // (polyList.size - 2) * 3
			for (int i = 0; i < polyList.size; i = i + ec) {
				newPolyList.add(polyList.get(i+0));
				newPolyList.add(polyList.get(i+1));
				newPolyList.add(polyList.get(i+2));
				newPolyList.add(polyList.get(i+0));
				newPolyList.add(polyList.get(i+2));
				newPolyList.add(polyList.get(i+3));
			}
			polyList = newPolyList;
		}
		
		// identify maximum coord index value, write coord index values
		int maxIndex = 0;
		for (int i = 0; i < polyList.size; i++) {
			int ci = polyList.get(i);
			maxIndex = Math.max(ci, maxIndex);
			coordIndex.append(ci + " ");
			if ((i+1) % 3 == 0) {
				coordIndex.append("-1 ");
			}
		}
		
		// loop over polygons (polygon indices for other array lists)
		for (int i = 0; i <= maxIndex; i++) {
			// write vertices
			coordPoint.append( coordPoints[i*dim + 0] + " ");
			coordPoint.append( coordPoints[i*dim + 2] + " ");
			coordPoint.append(-coordPoints[i*dim + 1] + " ");
			
			// write uv coordinates
			if (uvPoints != null) {
				uvPoint.append(uvPoints[i*2 + 0] + " ");
				uvPoint.append(uvPoints[i*2 + 1] + " ");
			}
			
			// write normal vectors
			if (normalVectors != null) {
				normalVector.append( normalVectors[i*3 + 0]/127.0f + " ");
				normalVector.append( normalVectors[i*3 + 2]/127.0f + " ");
				normalVector.append(-normalVectors[i*3 + 1]/127.0f + " ");
			}
		}
		
		// write coordIndex
		ifsNode.setCoordIndex(coordIndex.toString());
		
		// write coordPoint
		coordNode.setPoint(coordPoint.toString());
		
		// write uvPoint
		if (uvPoints != null) {
			// node for texture coordinates
			TextureCoordinate textureCoordNode = ifsNode.addNewTextureCoordinate();
			textureCoordNode.setPoint(uvPoint.toString());
		}
	
		// write normalVector
		if (normalVectors != null) {
			// node for normal vectors
			Normal normalVectorNode = ifsNode.addNewNormal();
			normalVectorNode.setVector(normalVector.toString());
		}
		
		// write other default attributes
		ifsNode.setColorPerVertex(false);
		ifsNode.setSolid(polygons.visibleSides != 2);
		
	}

}
