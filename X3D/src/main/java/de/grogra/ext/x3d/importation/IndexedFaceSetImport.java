package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.IndexedFaceSetDocument.IndexedFaceSet;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class IndexedFaceSetImport {

	public static ShadedNull createInstance(IndexedFaceSet indexedFaceSet) {
		MeshNode m = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (indexedFaceSet.isSetUSE()) {
			try {
				m = (MeshNode) ((Node) referenceMap.get(indexedFaceSet.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			m = new MeshNode();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (indexedFaceSet.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, indexedFaceSet);
			}
			
			setValues(m, indexedFaceSet, finalValueMap);
			if (indexedFaceSet.isSetDEF()) {
				referenceMap.put(indexedFaceSet.getDEF(), m);
			}				
		}
	
		m.setName("X3DIndexedFaceSet");
		return m;
	}
	
	private static void setValues(MeshNode node, IndexedFaceSet indexedFaceSet, HashMap<String, String> valueMap) {

		// ccw
		boolean ccw;
		ccw = valueMap.containsKey("ccw") ?
				Boolean.valueOf(valueMap.get("ccw")) :
					indexedFaceSet.isSetCcw() ?
							indexedFaceSet.getCcw() :
								true;
		
		// coordIndex
		int[] coordIndex;
		String coordIndexString = valueMap.containsKey("coordIndex") ? valueMap.get("coordIndex") : indexedFaceSet.getCoordIndex();
		coordIndex = Util.splitStringToArrayOfInt(coordIndexString, new int[]{});
		
		// normalIndex
		int[] normalIndex;
		String normalIndexString = valueMap.containsKey("normalIndex") ? valueMap.get("normalIndex") : indexedFaceSet.getNormalIndex();
		normalIndex = Util.splitStringToArrayOfInt(normalIndexString, new int[]{});
		
		// normalPerVertex
		boolean normalPerVertex;
		normalPerVertex = valueMap.containsKey("normalPerVertex") ?
				Boolean.valueOf(valueMap.get("normalPerVertex")) :
					indexedFaceSet.isSetNormalPerVertex() ?
							indexedFaceSet.getNormalPerVertex() :
								true;
		
		// solid
		boolean solid;
		solid = valueMap.containsKey("solid") ?
				Boolean.valueOf(valueMap.get("solid")) :
					indexedFaceSet.isSetSolid() ?
							indexedFaceSet.getSolid() :
								true;

		// texCoordIndex
		int[] texCoordIndex;
		String texCoordIndexString = valueMap.containsKey("texCoordIndex") ? valueMap.get("texCoordIndex") : indexedFaceSet.getTexCoordIndex();
		texCoordIndex = Util.splitStringToArrayOfInt(texCoordIndexString, new int[]{});
		
		// coords
		float[] coords = null;
		if (indexedFaceSet.getCoordinateArray().length > 0)
			coords = CoordinateImport.createInstance(indexedFaceSet.getCoordinateArray(0));
		
		// normals
		float[] normals = null;
		if (indexedFaceSet.getNormalArray().length > 0)
			normals = NormalImport.createInstance(indexedFaceSet.getNormalArray(0));
		
		// texCoords
		float[] texCoords = null;
		if (indexedFaceSet.getTextureCoordinateArray().length > 0)
			texCoords = TextureCoordinateImport.createInstance(indexedFaceSet.getTextureCoordinateArray(0));

		PolygonMesh p = new PolygonMesh();
		
		int coordIndexLength = coordIndex.length;
		
		int[] newCoordIndex = coordIndex.clone();
		float[] newCoord = coords.clone();
		float[] newTexCoord = null;
		float[] newNormal = null;
		
		int[] newTexCoordIndex = null;
		// if texture coordinates given but without texture coordinates index list
		// take coordinates index list
		if (texCoords != null) {
			if (texCoordIndex.length > 0)
				newTexCoordIndex = texCoordIndex.clone();
			else
				newTexCoordIndex = coordIndex.clone();
			newTexCoord = texCoords.clone();
		}
		else {
			// compute uvs
			FloatList tmpTexCoord = new FloatList();
			newTexCoordIndex = new int[coordIndex.length];
			
			// iterate of coords to find bounding box
			float xmin = newCoord[0], xmax = newCoord[0], ymin = newCoord[1], ymax = newCoord[1], zmin = newCoord[2], zmax = newCoord[2];
			for (int i = 0; i < newCoord.length; i=i+3) {
				if (newCoord[i] < xmin)
					xmin = newCoord[i];
				if (newCoord[i] > xmax)
					xmax = newCoord[i];
			}
			for (int i = 1; i < newCoord.length; i=i+3) {
				if (newCoord[i] < ymin)
					ymin = newCoord[i];
				if (newCoord[i] > ymax)
					ymax = newCoord[i];
			}
			for (int i = 2; i < newCoord.length; i=i+3) {
				if (newCoord[i] < zmin)
					zmin = newCoord[i];
				if (newCoord[i] > zmax)
					zmax = newCoord[i];
			}
			
			float xsize = xmax - xmin;
			float ysize = ymax - ymin;
			float zsize = zmax - zmin;
			
			// define s and t
			float ssize, s0, t0;
			int dims, dimt;
			if ((xsize > ysize) && (ysize > zsize)) {
				ssize = xsize; s0 = xmin; t0 = ymin;
				dims = 0; dimt = 1;
			}
			else if ((xsize > ysize) && (zsize > ysize)) {
				ssize = xsize; s0 = xmin; t0 = zmin;
				dims = 0; dimt = 2;
			}
			else if ((ysize > xsize) && (xsize > zsize)) {
				ssize = ysize; s0 = ymin; t0 = xmin;
				dims = 1; dimt = 0;
			}
			else if ((ysize > xsize) && (zsize > xsize)) {
				ssize = ysize; s0 = ymin; t0 = zmin;
				dims = 1; dimt = 2;
			}
			else if ((zsize > xsize) && (xsize > ysize)) {
				ssize = zsize; s0 = zmin; t0 = xmin;
				dims = 2; dimt = 0;
			}
			else {
				ssize = zsize; s0 = zmin; t0 = ymin;
				dims = 2; dimt = 1;
			}

			// calc uvs with formular from x3d spec
			float s, t;
			int indexCounter = 0;
			for (int i = 0; i < coordIndex.length; i++) {
				if (coordIndex[i] != -1) {
					newTexCoordIndex[i] = indexCounter++;
					s = (newCoord[3*coordIndex[i]+dims]-s0)/ssize;
					t = (newCoord[3*coordIndex[i]+dimt]-t0)/ssize;
					tmpTexCoord.add(s);
					tmpTexCoord.add(t);
				} else {
					newTexCoordIndex[i] = coordIndex[i];
				}
			}
			tmpTexCoord.trimToSize();
			newTexCoord = tmpTexCoord.toArray();
		}
		
		if ((normals != null) && normalPerVertex)
			newNormal = normals.clone();
		
		// texture coordinate indices or normal indices are given,
		// so transformation is needed because GroIMP has only one list
		// for indices of coordinates, texture coordinates and normals
		// best way: create new index list straight in ascending order
		// also convert coordinates and normals into GroIMP space
		newCoordIndex = new int[coordIndexLength];
		FloatList tmpCoord = new FloatList();
		FloatList tmpTexCoord = new FloatList();
		FloatList tmpNormal = null;
		if (normalPerVertex)
			tmpNormal = new FloatList();
		int indexCounter = 0;

		// if normals given but without normals index list
		// take coordinates index list
		int[] newNormalIndex = null;
		if (normalIndex.length > 0)
			newNormalIndex = normalIndex.clone();
		else
			newNormalIndex = coordIndex.clone();
		for (int i = 0; i < coordIndexLength; i++) {
			if (coordIndex[i] != -1) {
				newCoordIndex[i] = indexCounter++;
				tmpCoord.add( newCoord[3*coordIndex[i]+0]);
				tmpCoord.add(-newCoord[3*coordIndex[i]+2]);
				tmpCoord.add( newCoord[3*coordIndex[i]+1]);

				tmpTexCoord.add(newTexCoord[2*newTexCoordIndex[i]+0]);
				tmpTexCoord.add(newTexCoord[2*newTexCoordIndex[i]+1]);

				if ((normals != null) && normalPerVertex) {
					tmpNormal.add( newNormal[3*newNormalIndex[i]+0]);
					tmpNormal.add(-newNormal[3*newNormalIndex[i]+2]);
					tmpNormal.add( newNormal[3*newNormalIndex[i]+1]);
				}
			} else {
				newCoordIndex[i] = coordIndex[i];
			}
		}
		tmpCoord.trimToSize();
		newCoord = tmpCoord.toArray();
		tmpTexCoord.trimToSize();
		newTexCoord = tmpTexCoord.toArray();
		if (normalPerVertex) {
			tmpNormal.trimToSize();
			newNormal = tmpNormal.toArray();
		}
		
		// set the calculated data
		// if normalPerVertex is false, GroIMP has to calculate normals
		p.setVertexData(new FloatList(newCoord));
		p.setTextureData(newTexCoord);
		if ((normals != null) && normalPerVertex)
			p.setNormalData(newNormal);
		
		// transfer coordinate indices from coordIndex into int list for polygon mesh
		// polygons with more than three vertices are splitted into triangles
		// if ccw == false polygons are created in the opposite direction
		IntList coordIndexList = new IntList();
		for (int i = 0; i < coordIndexLength; i++) {
			int firstVertexIndex = i;
			coordIndexList.add(newCoordIndex[i]);
			if (ccw) {
				coordIndexList.add(newCoordIndex[i+1]);
				coordIndexList.add(newCoordIndex[i+2]);
			} else {
				coordIndexList.add(newCoordIndex[i+2]);
				coordIndexList.add(newCoordIndex[i+1]);
			}
			i+=3;
			while (( i < coordIndexLength ) && (newCoordIndex[i] != -1))  {
				coordIndexList.add(newCoordIndex[i-1]);
				if (ccw) {
					coordIndexList.add(newCoordIndex[i]);
					coordIndexList.add(newCoordIndex[firstVertexIndex]);
				} else {
					coordIndexList.add(newCoordIndex[firstVertexIndex]);
					coordIndexList.add(newCoordIndex[i]);
				}
				i++;
			}
		}
		p.setIndexData(coordIndexList);
		
		node.setVisibleSides(solid ? 0 : 2);
	      
		node.setPolygons(p);		
	}
	
}
