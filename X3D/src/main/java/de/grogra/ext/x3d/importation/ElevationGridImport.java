package de.grogra.ext.x3d.importation;

import java.util.HashMap;

import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.ElevationGridComputation;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.ElevationGridDocument.ElevationGrid;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class ElevationGridImport {
	
	public static ShadedNull createInstance(ElevationGrid elevationGrid) {
		de.grogra.imp3d.objects.MeshNode m = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (elevationGrid.isSetUSE()) {
			try {
				m = (de.grogra.imp3d.objects.MeshNode) ((Node) referenceMap.get(elevationGrid.getUSE())).clone(true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			m = new de.grogra.imp3d.objects.MeshNode();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (elevationGrid.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, elevationGrid);
			}
			
			setValues(m, elevationGrid, finalValueMap);
			if (elevationGrid.isSetDEF()) {
				referenceMap.put(elevationGrid.getDEF(), m);
			}				
		}
	
		m.setName("X3DElevationGrid");
		return m;
	}
	
	private static void setValues(de.grogra.imp3d.objects.MeshNode node, ElevationGrid elevationGrid, HashMap<String, String> valueMap) {
		// ccw
		boolean ccw;
		ccw = valueMap.containsKey("ccw") ?
				Boolean.valueOf(valueMap.get("ccw")) :
					elevationGrid.isSetCcw() ?
							elevationGrid.getCcw() : 
								true;
							
		// creaseAngle
	    float creaseAngle;
	    creaseAngle = valueMap.containsKey("creaseAngle") ?
	    		Float.valueOf(valueMap.get("creaseAngle")) :
	    			elevationGrid.isSetCreaseAngle() ?
	    					elevationGrid.getCreaseAngle() :
	    						0.0f;
		
		// height
		float[] height;
		String heightString = valueMap.containsKey("height") ? valueMap.get("height") : elevationGrid.getHeight();
		height = Util.splitStringToArrayOfFloat(heightString, new float[]{});
		
		// normalPerVertex
		boolean normalPerVertex;
		normalPerVertex = valueMap.containsKey("normalPerVertex") ?
				Boolean.valueOf(valueMap.get("normalPerVertex")) :
					elevationGrid.isSetNormalPerVertex() ?
							elevationGrid.getNormalPerVertex() :
								true;

		// solid
		boolean solid;
		solid = valueMap.containsKey("solid") ?
				Boolean.valueOf(valueMap.get("solid")) :
					elevationGrid.isSetSolid() ? 
							elevationGrid.getSolid() : 
								true;
		
		// dimensions - number of vertices in x and z
		int xDimension;
		xDimension = valueMap.containsKey("xDimension") ?
				Integer.valueOf(valueMap.get("xDimension")) :
					elevationGrid.getXDimension().intValue();
		int zDimension;
		zDimension = valueMap.containsKey("zDimension") ?
				Integer.valueOf(valueMap.get("zDimension")) :
					elevationGrid.getZDimension().intValue();
		
		// spacings - space between vertices in x and z
		float xSpacing;
		xSpacing = valueMap.containsKey("xSpacing") ?
				Float.valueOf(valueMap.get("xSpacing")) :
					elevationGrid.getXSpacing();
		float zSpacing;
		zSpacing = valueMap.containsKey("zSpacing") ?
				Float.valueOf(valueMap.get("zSpacing")) :
					elevationGrid.getZSpacing();
		
		// normals
		float[] normals = null;
		if (elevationGrid.getNormalArray().length > 0)
			normals = NormalImport.createInstance(elevationGrid.getNormalArray(0));
		
		// texCoords
		float[] texCoords = null;
		if (elevationGrid.getTextureCoordinateArray().length > 0)
			texCoords = TextureCoordinateImport.createInstance(elevationGrid.getTextureCoordinateArray(0));
		
		
		PolygonMesh p = new PolygonMesh();
		
		FloatList vertexList = new FloatList();
		IntList coordIndexList = new IntList();
		FloatList textureData = new FloatList();
		FloatList normalData = new FloatList();
		
		int vertexCount = 0;
		
		ElevationGridComputation egc = new ElevationGridComputation(xDimension, zDimension, xSpacing, zSpacing, height, creaseAngle);
		
		Vector3f vertex0 = new Vector3f();
		Vector3f vertex1 = new Vector3f();
		Vector3f vertex2 = new Vector3f();
		Vector3f vertex3 = new Vector3f();
		Vector3f vertex4 = new Vector3f();
		Vector3f vertex5 = new Vector3f();
		Vector3f normal0 = new Vector3f();
		Vector3f normal1 = new Vector3f();
		Vector3f normal2 = new Vector3f();
		Vector3f normal3 = new Vector3f();
		Vector3f normal4 = new Vector3f();
		Vector3f normal5 = new Vector3f();
		
		for (int j = 0; j < zDimension - 1; j++) {
			for (int i = 0; i < xDimension - 1; i++) {
				// coordinate indices
				if (!ccw) {
					coordIndexList.add(vertexCount+0);
					coordIndexList.add(vertexCount+1);
					coordIndexList.add(vertexCount+2);

					coordIndexList.add(vertexCount+3);
					coordIndexList.add(vertexCount+4);
					coordIndexList.add(vertexCount+5);
				}
				else {
					coordIndexList.add(vertexCount+0);
					coordIndexList.add(vertexCount+2);
					coordIndexList.add(vertexCount+1);

					coordIndexList.add(vertexCount+3);
					coordIndexList.add(vertexCount+5);
					coordIndexList.add(vertexCount+4);
				}
				
				vertexCount += 6;
				
				// vertices
				egc.getVertex(vertex0, i + 0, j + 0);
				vertexList.add(vertex0.x);
				vertexList.add(vertex0.y);
				vertexList.add(vertex0.z);
			
				egc.getVertex(vertex1, i + 1, j + 0);
				vertexList.add(vertex1.x);
				vertexList.add(vertex1.y);
				vertexList.add(vertex1.z);
				
				egc.getVertex(vertex2, i + 0, j + 1);
				vertexList.add(vertex2.x);
				vertexList.add(vertex2.y);
				vertexList.add(vertex2.z);
				
				egc.getVertex(vertex3, i + 1, j + 1);
				vertexList.add(vertex3.x);
				vertexList.add(vertex3.y);
				vertexList.add(vertex3.z);

				egc.getVertex(vertex4, i + 0, j + 1);
				vertexList.add(vertex4.x);
				vertexList.add(vertex4.y);
				vertexList.add(vertex4.z);
				
				egc.getVertex(vertex5, i + 1, j + 0);
				vertexList.add(vertex5.x);
				vertexList.add(vertex5.y);
				vertexList.add(vertex5.z);

				// texutre coordinates
				if (texCoords != null) {
					textureData.add( texCoords[2 * ( (i + 0) + (j + 0) * xDimension ) + 0 ] ); 
					textureData.add( texCoords[2 * ( (i + 0) + (j + 0) * xDimension ) + 1 ] ); 

					textureData.add( texCoords[2 * ( (i + 1) + (j + 0) * xDimension ) + 0 ] ); 
					textureData.add( texCoords[2 * ( (i + 1) + (j + 0) * xDimension ) + 1 ] ); 

					textureData.add( texCoords[2 * ( (i + 0) + (j + 1) * xDimension ) + 0 ] ); 
					textureData.add( texCoords[2 * ( (i + 0) + (j + 1) * xDimension ) + 1 ] ); 

					textureData.add( texCoords[2 * ( (i + 1) + (j + 1) * xDimension ) + 0 ] ); 
					textureData.add( texCoords[2 * ( (i + 1) + (j + 1) * xDimension ) + 1 ] ); 

					textureData.add( texCoords[2 * ( (i + 0) + (j + 1) * xDimension ) + 0 ] ); 
					textureData.add( texCoords[2 * ( (i + 0) + (j + 1) * xDimension ) + 1 ] ); 

					textureData.add( texCoords[2 * ( (i + 1) + (j + 0) * xDimension ) + 0 ] ); 
					textureData.add( texCoords[2 * ( (i + 1) + (j + 0) * xDimension ) + 1 ] ); 
				}
				else {
					textureData.add((float) (i + 0) / (xDimension - 1));
					textureData.add((float) (j + 0) / (zDimension - 1));
					
					textureData.add((float) (i + 1) / (xDimension - 1));
					textureData.add((float) (j + 0) / (zDimension - 1));
					
					textureData.add((float) (i + 0) / (xDimension - 1));
					textureData.add((float) (j + 1) / (zDimension - 1));

					textureData.add((float) (i + 1) / (xDimension - 1));
					textureData.add((float) (j + 1) / (zDimension - 1));
					
					textureData.add((float) (i + 0) / (xDimension - 1));
					textureData.add((float) (j + 1) / (zDimension - 1));

					textureData.add((float) (i + 1) / (xDimension - 1));
					textureData.add((float) (j + 0) / (zDimension - 1));
				}
				
				// normals
				if (normals != null) {
					
					if (normalPerVertex) {
						normalData.add( normals[3 * ( (i + 0) + (j + 0) * xDimension ) + 0 ] ); 
						normalData.add(-normals[3 * ( (i + 0) + (j + 0) * xDimension ) + 2 ] ); 
						normalData.add( normals[3 * ( (i + 0) + (j + 0) * xDimension ) + 1 ] );
						
						normalData.add( normals[3 * ( (i + 1) + (j + 0) * xDimension ) + 0 ] ); 
						normalData.add(-normals[3 * ( (i + 1) + (j + 0) * xDimension ) + 2 ] ); 
						normalData.add( normals[3 * ( (i + 1) + (j + 0) * xDimension ) + 1 ] );

						normalData.add( normals[3 * ( (i + 0) + (j + 1) * xDimension ) + 0 ] ); 
						normalData.add(-normals[3 * ( (i + 0) + (j + 1) * xDimension ) + 2 ] ); 
						normalData.add( normals[3 * ( (i + 0) + (j + 1) * xDimension ) + 1 ] );

						normalData.add( normals[3 * ( (i + 1) + (j + 1) * xDimension ) + 0 ] ); 
						normalData.add(-normals[3 * ( (i + 1) + (j + 1) * xDimension ) + 2 ] ); 
						normalData.add( normals[3 * ( (i + 1) + (j + 1) * xDimension ) + 1 ] );

						normalData.add( normals[3 * ( (i + 0) + (j + 1) * xDimension ) + 0 ] ); 
						normalData.add(-normals[3 * ( (i + 0) + (j + 1) * xDimension ) + 2 ] ); 
						normalData.add( normals[3 * ( (i + 0) + (j + 1) * xDimension ) + 1 ] );

						normalData.add( normals[3 * ( (i + 1) + (j + 0) * xDimension ) + 0 ] ); 
						normalData.add(-normals[3 * ( (i + 1) + (j + 0) * xDimension ) + 2 ] ); 
						normalData.add( normals[3 * ( (i + 1) + (j + 0) * xDimension ) + 1 ] );
					}
					else {
						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 0] );
						normalData.add(-normals[3 * (i + j * (xDimension - 1)) + 2] );
						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 1] );
						
						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 0] );
						normalData.add(-normals[3 * (i + j * (xDimension - 1)) + 2] );
						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 1] );

						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 0] );
						normalData.add(-normals[3 * (i + j * (xDimension - 1)) + 2] );
						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 1] );

						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 0] );
						normalData.add(-normals[3 * (i + j * (xDimension - 1)) + 2] );
						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 1] );
						
						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 0] );
						normalData.add(-normals[3 * (i + j * (xDimension - 1)) + 2] );
						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 1] );

						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 0] );
						normalData.add(-normals[3 * (i + j * (xDimension - 1)) + 2] );
						normalData.add( normals[3 * (i + j * (xDimension - 1)) + 1] );
					}
				}
				else {
					egc.getNormal(normal0, vertex0, vertex1, vertex2);
					egc.getNormal(normal1, vertex3, vertex4, vertex5);
					
//					egc.getCreaseNormal(normal0, normal1, normal2, normal3, normal4, normal5, i, j);
					
					normalData.add(normal0.x);
					normalData.add(normal0.y);
					normalData.add(normal0.z);

					normalData.add(normal0.x);
					normalData.add(normal0.y);
					normalData.add(normal0.z);

					normalData.add(normal0.x);
					normalData.add(normal0.y);
					normalData.add(normal0.z);

					normalData.add(normal1.x);
					normalData.add(normal1.y);
					normalData.add(normal1.z);

					normalData.add(normal1.x);
					normalData.add(normal1.y);
					normalData.add(normal1.z);

					normalData.add(normal1.x);
					normalData.add(normal1.y);
					normalData.add(normal1.z);
				}
				
			}
		}
		
		p.setIndexData(coordIndexList);
		p.setVertexData(vertexList);
		p.setTextureData(textureData.elements);
		p.setNormalData(normalData.elements);
		
		node.setPolygons(p);
		node.setVisibleSides(solid ? 0 : 2);
	}
	

	
}
