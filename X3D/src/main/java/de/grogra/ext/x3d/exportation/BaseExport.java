package de.grogra.ext.x3d.exportation;

import java.io.IOException;
import javax.vecmath.Matrix4d;
import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.LightNode;
import de.grogra.imp3d.objects.SceneTreeWithShader;
import de.grogra.imp3d.objects.Sky;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;

public abstract class BaseExport implements SceneGraphExport.NodeExport {

	public void export(Leaf node, InnerNode transform, SceneGraphExport sge)
			throws IOException {
		X3DExport export = (X3DExport) sge;
		export.increaseProgress();
		
		Transform parentNode = export.getLastTransform();
		
		// is transformation given create a x3d transform element only for this node (no inheritation)
		if (transform != null) {
			Matrix4d transMatrix = new Matrix4d();
			transform.get(transMatrix);
			Transform transformNode = TransformExport.handleTransform(transMatrix, parentNode);
			parentNode = transformNode;				
		}
		
		// if groimp node is an axis node then correct the center of it
		if (this instanceof AxisExport) {
			Matrix4d transMatrix = new Matrix4d();
			transMatrix.setIdentity();
			Transform transformNode = TransformExport.handleTransform(transMatrix, parentNode, true, (Node) node.object, sge.getGraphState());
			parentNode = transformNode;
		}
		
		if ((node instanceof SceneTreeWithShader.Leaf) &&
//				(((SceneTreeWithShader.Leaf)node).shader != null) &&
				(!(node.object instanceof Sky)) &&
				(!(node.object instanceof LightNode))) {
			// if node is a geometry node with shader
			
			// create shape
			Shape shape = ShapeExport.handleShape(parentNode);
			
			// create appearance and material
			AppearanceExport.handleAppearance(shape, node.object, ((SceneTreeWithShader.Leaf)node).shader, export, sge.getGraphState());
			
			exportImpl(node, export, shape, parentNode);
		
		}
		else {
			// node is something else (like light)
			exportImpl(node, export, null, parentNode);
		}
	}

	protected abstract void exportImpl(Leaf node, X3DExport export, Shape shapeNode, Transform transformNode) throws IOException;
	
}
