module platform.core {
	exports de.grogra.pf.boot;
	exports de.grogra.pf.io;
	exports de.grogra.pf.registry;
	exports de.grogra.pf.registry.expr;
		
	requires graph;
	requires utilities;
	requires xl.core;
	requires java.datatransfer;
	requires java.desktop;
	requires java.logging;
	requires java.prefs;
	requires java.xml;
	requires java.base;
	requires org.apache.logging.log4j;

	requires SparseBitSet;

	
	opens de.grogra.pf.boot to utilities;
	opens de.grogra.pf.io to utilities;

}
