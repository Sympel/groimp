
package de.grogra.math.weiszfeld;

import javax.vecmath.Point3d;

/**
 * Point3d with weight
 */
public class WeightedPoint3d extends Point3d {

	private static final long serialVersionUID = 14365347L;
	private double weight = 1;

	public WeightedPoint3d() {
	}

	public WeightedPoint3d(Point3d p, double weight) {
		x = p.x;
		y = p.y;
		z = p.z;
		this.weight = weight;
	}

	public WeightedPoint3d(WeightedPoint3d p) {
		x = p.x;
		y = p.y;
		z = p.z;
		weight = p.weight;
	}
	
	public Point3d getPoint3d() {
		return new Point3d(x,y,z);
	}
	
	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public static WeightedPoint3d substraction(WeightedPoint3d p0, WeightedPoint3d p1) {
		WeightedPoint3d newP = new WeightedPoint3d(p0, 0);
		newP.x -= p1.x;
		newP.y -= p1.y;
		newP.z -= p1.z;
		return newP; 
	}
	
	/**
     * Returns the length of this vector.
     * @return the length of this vector
     */
     public final double length() {
	  return Math.sqrt(x*x + y*y + z*z);
     }

}
