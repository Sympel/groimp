package de.grogra.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.vecmath.Point3d;

/**
 * GenLloyd Library - Implementation of Generalized Lloyd (also known as Linde-Buzo-Gray or LBG) algorithm
 *
 * @see http://www.data-compression.com/vq.html#lbg
 * @see http://books.google.de/books?id=a7IYh8ncb6AC&lpg=PA29&ots=WOOoPXuNKk&dq=java
 *      %20LBG%20implementation&pg=PA29#v=onepage&q&f=false
 * 
 * @author Markus Konrad <post@mkonrad.net>
 */
public class GenLloyd {
	protected List<Point3d> samplePoints;
	protected List<Point3d> clusterPoints;

	int[] pointApproxIndices;
	protected double epsilon = 0.0005;
	protected double avgDistortion = 0.0;

	/**
	 * Create Generalized Lloyd object with an array of sample points
	 * 
	 * @param samplePoint3ds
	 *            2-dimensional array of N sample points where each point has
	 *            the same dimension K
	 */
	public GenLloyd(List<Point3d> samplePoints) {
		setSamplePoints(samplePoints);
	}

	public GenLloyd(Point3d[] samplePoints) {
		this.samplePoints = Arrays.asList(samplePoints);
	}
	
	/**
	 * Return epsilon parameter (accuracy)
	 * 
	 * @return epsilon parameter (accuracy)
	 */
	public double getEpsilon() {
		return epsilon;
	}

	/**
	 * Set epsilon parameter (accuracy). Should be a small number 0.0 < epsilon
	 * < 0.1
	 * 
	 * @param epsilon
	 *            parameter (accuracy)
	 */
	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}

	/**
	 * Set list of sample points
	 * 
	 * @param samplePoints
	 */
	public void setSamplePoints(List<Point3d> samplePoints) {
		if (samplePoints.size() > 0) {
			this.samplePoints = samplePoints;
		}
	}

	/**
	 * Get calculated cluster points. <numClusters> cluster points will be
	 * calculated and returned
	 * 
	 * @param numClusters
	 *            number of clusters that should be calculated. This should be a power of 2.
	 * @return calculated cluster points
	 */
	public List<Point3d> getClusterPoints(int numClusters) {
		calcClusters(numClusters);
		return clusterPoints;
	}

	protected void calcClusters(int numClusters) {
		// initialize with first cluster
		clusterPoints = new ArrayList<Point3d>(numClusters);

		Point3d newClusterPoint = initializeClusterPoint(samplePoints);
		clusterPoints.add(newClusterPoint);

		if (numClusters > 1) {
			// calculate initial average distortion
			avgDistortion = 0.0;
			for (Point3d samplePoint : samplePoints) {
				avgDistortion += samplePoint.distanceSquared(newClusterPoint);
			}

			avgDistortion /= samplePoints.size() * 3;

			// set up array of point approximization indices
			pointApproxIndices = new int[samplePoints.size()];

			// split the clusters
			int i = 1;
			do {
				i = splitClusters();
			} while (i < numClusters);
		}
	}

	protected int splitClusters() {
		int newClusterPointSize = 2;
		if (clusterPoints.size() != 1) {
			newClusterPointSize = clusterPoints.size() * 2;
		}

		// split clusters
		List<Point3d> newClusterPoints = new ArrayList<Point3d>(newClusterPointSize);
		for(int i = 0; i<newClusterPointSize; i++) newClusterPoints.add(i, new Point3d());
		int newClusterPointIdx = 0;
		for (Point3d clusterPoint : clusterPoints) {
			newClusterPoints.set(newClusterPointIdx,     createNewClusterPoint(clusterPoint, -1));
			newClusterPoints.set(newClusterPointIdx + 1, createNewClusterPoint(clusterPoint, +1));

			newClusterPointIdx += 2;
		}

		clusterPoints = newClusterPoints;

		// iterate to approximate cluster points
		// int iteration = 0;
		double curAvgDistortion = 0.0;
		do {
			curAvgDistortion = avgDistortion;

			// find the min values
			for (int pointIdx = 0; pointIdx < samplePoints.size(); pointIdx++) {
				double minDist = Double.MAX_VALUE;
				for (int clusterPointIdx = 0; clusterPointIdx < clusterPoints
						.size(); clusterPointIdx++) {
					double newMinDist = samplePoints.get(pointIdx)
							.distanceSquared(clusterPoints.get(clusterPointIdx));
					if (newMinDist < minDist) {
						minDist = newMinDist;
						pointApproxIndices[pointIdx] = clusterPointIdx;
					}
				}
			}

			// update codebook
			for (int clusterPointIdx = 0; clusterPointIdx < clusterPoints
					.size(); clusterPointIdx++) {
				Point3d newClusterPoint = new Point3d();
				int num = 0;
				for (int pointIdx = 0; pointIdx < samplePoints.size(); pointIdx++) {
					if (pointApproxIndices[pointIdx] == clusterPointIdx) {
						newClusterPoint.add(samplePoints.get(pointIdx));
						num++;
					}
				}

				if (num > 0) {
					newClusterPoint.scale(1.0 / num);
					clusterPoints.set(clusterPointIdx, newClusterPoint);
				}
			}

			// increase iteration count
			// System.out.println("  > Iteration = " + iteration);
			// iteration++;

			// update average distortion
			avgDistortion = 0.0;
			for (int pointIdx = 0; pointIdx < samplePoints.size(); pointIdx++) {
				avgDistortion += samplePoints.get(pointIdx).distanceSquared(
						clusterPoints.get(pointApproxIndices[pointIdx]));
			}

			avgDistortion /= samplePoints.size() * 3;

		} while (((curAvgDistortion - avgDistortion) / curAvgDistortion) > epsilon);

		return clusterPoints.size();
	}

	protected Point3d initializeClusterPoint(List<Point3d> pointsInCluster) {
		// calculate point sum
		Point3d clusterPoint = new Point3d();
		for (Point3d p : pointsInCluster) {
			clusterPoint.add(p);
		}

		// calculate average
		clusterPoint.scale(1.0 / pointsInCluster.size());

		return clusterPoint;
	}

	protected Point3d createNewClusterPoint(Point3d clusterPoint3d, int epsilonFactor) {
		Point3d newClusterPoint = new Point3d(clusterPoint3d);
		newClusterPoint.scale(1.0 + epsilonFactor * epsilon);

		return newClusterPoint;
	}

}
