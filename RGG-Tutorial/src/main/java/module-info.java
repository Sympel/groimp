module rgg.tutorial {
	exports de.grogra.rgg.tutorial;

	requires graph;
	requires platform;
	requires platform.core;
	requires rgg;
	requires utilities;
	requires xl.core;
	requires java.desktop;
}