
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.rgg.tutorial;

import java.util.HashSet;
import java.util.StringTokenizer;

import de.grogra.graph.impl.Node;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Console;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.rgg.Library;
import de.grogra.rgg.model.CompilationFilter;

public final class Methods
{
	private Methods ()
	{
	}

	public static void selectSourceFiles (Item cmd, Object name, Context ctx)
	{
		Registry reg = ctx.getWorkbench ().getRegistry ();
		Item dir = reg.getDirectory ("/project/objects/files", null);
		SourceFile sel = null;
		StringTokenizer tok = new StringTokenizer ((String) name, ",");
		HashSet set = new HashSet ();
		String first = null;
		while (tok.hasMoreTokens ())
		{
			String t = tok.nextToken ().trim ();
			if (first == null)
			{
				first = t;
			}
			set.add (t);
		}
		for (Node n = dir.getBranch (); n != null; n = n.getSuccessor ())
		{
			if (n instanceof SourceFile)
			{
				SourceFile sf = (SourceFile) n;
				boolean s = set.contains (sf.getName ());
				if (s && sf.getName ().equals (first))
				{
					sel = sf;
				}
				sf.setDisabled (!s);
			}
		}
		if (sel != null)
		{
			sel.show (ctx, null);
		}
		SourceFile.refresh (dir, null, CompilationFilter.DEACTIVATION_CATEGORY, null);
	}

	public static void enter (Item cmd, Object line, Context ctx)
	{
		Console c = Library.console ();
		if (c != null)
		{
			c.enter (String.valueOf (line));
		}
	}
}
