package de.grogra.ext.dxf;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ReaderSource;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class ImportFilter extends FilterBase implements ObjectSource {

	// must match to "outputClass" in plugin.xml
	public static final IOFlavor FLAVOR = IOFlavor.valueOf(Node.class);

	public ImportFilter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(FLAVOR);
	}

	public Object getObject() throws IOException {
		// obtain reader of the selected DXF file
		// Reader reader = ((ReaderSource)getSource()).getReader();
		BufferedReader reader = new BufferedReader(((ReaderSource) getSource())
				.getReader());

		// create a node that is the root of all objects read from the DXF
		Node result = new ShadedNull();

		// create a polygon array to hold mesh data
		IntList indices = new IntList();
		FloatList vertices = new FloatList();

		// prepare space for temporary state
		String sCode, sData;
		final HashMap<String, Integer> layers = new HashMap<String, Integer>();
		int code;
		int layer = 0;
		float x0 = 0, y0 = 0, z0 = 0, 
			x1 = 0, y1 = 0, z1 = 0, 
			x2 = 0, y2 = 0, z2 = 0, 
			x3 = 0, y3 = 0, z3 = 0;
		int i0 = 0, i1 = 0, i2 = 0, i3 = 0;
		boolean eof = false;
		boolean isTriangle = true;
		boolean faceStarted = false;
		boolean polylineStarted = false;
		boolean polymeshStarted = false;
		boolean vertexStarted = false;
		boolean meshStarted = false;
		int index = 0;
		int flags = 0;

		while (!eof) {
			// read in code and data line
			sCode = reader.readLine().trim ();
			sData = reader.readLine().trim ();

			// convert code string to code number
			code = Integer.parseInt(sCode);

			// handle code
			switch (code) {
			case 0: {
				// check if face data must be written
				if (faceStarted) {
					// write out triangle
					indices.push(index++);
					addVertex(vertices, x0, y0, z0);
					indices.push(index++);
					addVertex(vertices, x1, y1, z1);
					indices.push(index++);
					addVertex(vertices, x2, y2, z2);
					
					// if it was a quad also write out second triangle
					if (!isTriangle)
					{
						// TODO
					}
					
					// reset face flag
					faceStarted = false;
					
					// remember that a mesh is active
					meshStarted = true;
				}
				
				// check if vertex data must be written
				if (vertexStarted) {
					// write out vertex
					// bit 7 must be set (polyface mesh vertex)
					if ((flags & 0x80) != 0) {
						// bit 6 is set for vertices and clear for indices
						if ((flags & 0x40) != 0) {
							addVertex(vertices, x0, y0, z0);
						} else {
							addIndices(indices, i0, i1, i2, i3);
						}
					}
					// reset indices
					i0 = i1 = i2 = i3 = 0;
					
					// reset vertex flag
					vertexStarted = false;
				}
				
				// check if polymesh started
				if (polylineStarted)
				{
					// if bit 6 is set it is a polymesh
					if ((flags & 0x40) != 0)
					{
						polymeshStarted = true;
						meshStarted = true;
					}
					
					// reset polyline flag
					polylineStarted = false;
				}
				
				if ("EOF".equals(sData)) {
					eof = true;
				} else if ("3DFACE".equals(sData)) {
					faceStarted = true;
				} else if ("POLYLINE".equals(sData)) {
					polylineStarted = true;
				} else if ("VERTEX".equals(sData)) {
					vertexStarted = true;
				}
				
				// check if polymesh data must be written
				if (polymeshStarted && !vertexStarted)
				{
					addMesh(result, vertices, indices, layer);
					
					// prepare new arrays for indices and vertices
					indices.clear();
					vertices.clear();
					
					// reset polymesh flag
					polymeshStarted = false;
					meshStarted = false;
				}
			}
				break;
			case 8:
				// check if the layer is already known
				if (layers.containsKey(sData))
				{
					// obtain the layer number from the map
					layer = layers.get(sData);
				}
				else
				{
					// need to create a new entry for that layer
					layer = layers.size();
					layers.put(sData, layer);
				}
				break;
			case 10:
				x0 = Float.parseFloat(sData);
				break;
			case 20:
				y0 = Float.parseFloat(sData);
				break;
			case 30:
				z0 = Float.parseFloat(sData);
				break;
			case 11:
				x1 = Float.parseFloat(sData);
				break;
			case 21:
				y1 = Float.parseFloat(sData);
				break;
			case 31:
				z1 = Float.parseFloat(sData);
				break;
			case 12:
				x2 = Float.parseFloat(sData);
				break;
			case 22:
				y2 = Float.parseFloat(sData);
				break;
			case 32:
				z2 = Float.parseFloat(sData);
				break;
			case 13:
				x3 = Float.parseFloat(sData);
				isTriangle = false;
				break;
			case 23:
				y3 = Float.parseFloat(sData);
				isTriangle = false;
				break;
			case 33:
				z3 = Float.parseFloat(sData);
				isTriangle = false;
				break;
			case 70:
				flags = Integer.parseInt(sData);
				break;
			case 71:
				i0 = Integer.parseInt(sData);
				break;
			case 72:
				i1 = Integer.parseInt(sData);
				break;
			case 73:
				i2 = Integer.parseInt(sData);
				break;
			case 74:
				i3 = Integer.parseInt(sData);
				break;
			}
		}
		
		// check if mesh data is active
		if (meshStarted) {
			addMesh(result, vertices, indices, layer);
		}
		
		return result;
	}

	private static void addVertex(FloatList vertices, float x, float y, float z)
	{
		vertices.add(x);
		vertices.add(y);
		vertices.add(z);
	}
	
	static void addIndices(IntList indices, int i0, int i1, int i2, int i3) {
		// triangle is specified by having three indices != 0
		// quad is specified by having four vertices != 0
		
		int[] ia = new int[] { i0, i1, i2, i3 };
		int count = ia.length;
		
		// search for unspecified indices and remove them
		for (int i = 0; i < count; i++)
		{
			if (ia[i] == 0)
			{
				System.arraycopy(ia, i + 1, ia, i, ia.length - i - 1);
				count--;
			}
		}
		
		// search for degenerate triangle/quad
		// triangle/quad is degenerate if at least two indices are equal
		boolean degenerate = false;
		if (count == 3 && (ia[0] == ia[1] || ia[1] == ia[2] || ia[2] == ia[1])) {
			degenerate = true;
		}
		if (count == 4
				&& (ia[0] == ia[2] || ia[1] == ia[3] || ia[0] == ia[1]
						|| ia[1] == ia[2] || ia[2] == ia[3] || ia[3] == ia[0])) {
			// degenerate quad, check if it degenerates to triangle
			if (ia[0] != ia[2] && ia[1] != ia[3])
			{
				// two neighbouring indices must be the same
				// so search them and remove one of them
				if (ia[0] == ia[1]) {
					ia[2] = ia[3]; ia[1] = ia[2];
				} else if (ia[1] == ia[2])
				{
					ia[2] = ia[3];
				} // else ia[2] == ia[3] or ia[3] == ia[0]
				count = 3;
			}
			else
			{
				degenerate = true;
			}
		}
		
		// if not degenerate and triangle/quad, then write out indices
		if (!degenerate)
		{
			indices.add(ia[0] - 1);
			indices.add(ia[1] - 1);
			indices.add(ia[2] - 1);
			if (count == 4)
			{
				indices.add(ia[3] - 1);
				indices.add(ia[0] - 1);
				indices.add(ia[2] - 1);
			}
		}
	}
	
	/**
	 * 
	 * @param root
	 * @param vertices
	 * @param indices
	 * @param layer
	 */
	static void addMesh(Node root, FloatList vertices, IntList indices, int layer)
	{
		// check indices if they are inside bounds of vertex array
		// for now handle this by setting the index to zero (TODO)
		int vertexCount = vertices.size / 3;
		for (int i = 0; i < indices.size; i++)
		{
			int index = indices.get(i);
			// negative indices mark the edge as being invisible
			// this is not supported but must be handled
			if (index < 0)
			{
				index = -index;
			}
			// check if the index is valid, if not just set it to zero
			if (index >= vertexCount) {
				System.out.println("invalid index in mesh: index = " + index);
				index = 0;
			}
			indices.set(i, index);
		}
		
		// prepare a mesh node
		MeshNode mesh = new MeshNode();
		PolygonMesh pm = new PolygonMesh();
		pm.setIndexData(indices);
		pm.setVertexData(vertices);
		mesh.setPolygons(pm);
		
		// append the mesh node to the result node
		root.addEdgeBitsTo(mesh, Graph.BRANCH_EDGE, null);
		
		// set the layer of the node
		root.setLayer(layer);
	}
}
