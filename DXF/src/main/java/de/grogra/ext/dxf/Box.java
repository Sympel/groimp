
package de.grogra.ext.dxf;

import java.io.IOException;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point4d;

import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;

public class Box extends ObjectBase
{
	public void exportImpl (Leaf node, InnerNode transform, DXFExport export)
			throws IOException
	{
		//		System.err.println ("export was called");

		float w = (float) node.getDouble (Attributes.WIDTH);
		float h = (float) node.getDouble (Attributes.HEIGHT);
		float l = (float) node.getDouble (Attributes.LENGTH);
//		System.err.println ("w = " + w + "   l = " + l + "   h = " + h);
//		System.err.println ("transform: " + transform);

		double x0 = -w / 2;
		double y0 = -h / 2;
		double z0 = 0;
		double x1 = w / 2;
		double y1 = h / 2;
		double z1 = l;

		Matrix4d m = (Matrix4d) export.matrixStack.peek ();
		Matrix4d n = new Matrix4d ();
		if (transform != null)
		{
			transform.transform (m, n);
		}
		else
		{
			n.set (m);
		}
		m = n;

		Point4d[] p = new Point4d[] {new Point4d (x0, y0, z0, 1),
				new Point4d (x0, y0, z1, 1), new Point4d (x1, y0, z0, 1),
				new Point4d (x1, y0, z1, 1), new Point4d (x1, y1, z0, 1),
				new Point4d (x1, y1, z1, 1), new Point4d (x0, y1, z0, 1),
				new Point4d (x0, y1, z1, 1)};

//		System.err.println ("Transformation matrix: " + m);
		for (int i = 0; i < p.length; i++)
		{
			m.transform (p[i]);
//			System.err.println ("Transformed point: " + p[i]);
		}

		// generate a polyline
		export.out.println("0");
		export.out.println("POLYLINE");
		// polyline flag: polyface mesh
		export.out.println("70");
		export.out.println("64");
		// number of vertices
		export.out.println("71");
		export.out.println("8");
		// number of faces
		export.out.println("72");
		export.out.println("6");
		
		// vertices
		for (int i = 0; i < p.length;i++){
		export.out.println("0");
		export.out.println("VERTEX");
		export.out.println("10");
		export.out.println(round(p[i].x));
		export.out.println("20");
		export.out.println(round(p[i].y));
		export.out.println("30");
		export.out.println(round(p[i].z));
		export.out.println("70");	// vertex flags
		export.out.println("192");
		}

		// faces
		int[][] indices = new int[][] {
				{1, 2, 4, 3},
				{3,4,6,5},
				{5,6,8,7},
				{7,8,2,1},
				{2,8,6,4},
				{7,1,3,5}
		};
		for (int i = 0; i < indices.length;i++)
		{
			export.out.println("0");
			export.out.println("VERTEX");
			export.out.println("10");
			export.out.println("0");
			export.out.println("20");
			export.out.println("0");
			export.out.println("30");
			export.out.println("0");
			export.out.println("70");	// vertex flags
			export.out.println("128");
			for (int j = 0; j < indices[i].length; j++)
			{
				export.out.println("7"+(j+1));
				export.out.println(indices[i][j]);
			}
		}
		
		export.out.println("0");
		export.out.println("SEQEND");
	}

}
