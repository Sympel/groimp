package de.grogra.ext.obj;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.jagatoo.loaders.models.obj.OBJFace;
import org.jagatoo.loaders.models.obj.OBJGroup;
import org.jagatoo.loaders.models.obj.OBJModelPrototype;
import org.jagatoo.loaders.models.obj.OBJPrototypeLoader;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.VirtualFileReaderSource;
import de.grogra.xl.util.ByteList;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;
import de.grogra.xl.util.LongHashMap;

public class ImportFilter extends FilterBase implements ObjectSource {

	// must match to "outputClass" in plugin.xml
	public static final IOFlavor FLAVOR = IOFlavor.valueOf(Node.class);

	public ImportFilter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(FLAVOR);
	}

	public Object getObject() throws IOException {
		
		// create a node that is the root of all objects read from the DXF
	    VirtualFileReaderSource src = (VirtualFileReaderSource)getSource();
	    ObjImporter importer = new ObjImporter(src.getFileSystem ().getInputStream (src.getFile ()),
			src.getFileSystem ().toURL (src.getFile ()));
	    
	    return importer.doImport();
	    
	}
	
	


}
