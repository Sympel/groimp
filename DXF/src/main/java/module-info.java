module dxf {
	exports org.jagatoo.loaders.models.obj;
	exports de.grogra.ext.dxf;
	exports org.jagatoo.util.geometry;
	exports de.grogra.ext.obj;
	exports org.jagatoo.util.arrays;

	requires graph;
	requires imp;
	requires imp3d;
	requires platform;
	requires platform.core;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires java.desktop;
}