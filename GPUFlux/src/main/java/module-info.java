module gpuFlux {
	exports de.grogra.gpuflux.scene.shading;
	exports de.grogra.gpuflux.scene.light;
	exports de.grogra.gpuflux;
	exports de.grogra.gpuflux.scene.filter;
	exports de.grogra.gpuflux.utils;
	exports de.grogra.gpuflux.scene;
	exports de.grogra.gpuflux.jocl;
	exports de.grogra.gpuflux.jocl.compute;
	exports de.grogra.gpuflux.scene.BVH;
	exports de.grogra.gpuflux.scene.experiment;
	exports de.grogra.gpuflux.scene.volume;
	exports de.grogra.gpuflux.scene.shading.channel;
	exports de.grogra.gpuflux.tracer;
	exports de.grogra.gpuflux.imp3d.spectral;
	exports de.grogra.gpuflux.imp3d.io;
	exports de.grogra.gpuflux.imp3d.objects;
	exports de.grogra.gpuflux.imp3d.shading;

	requires graph;
	requires imp;
	requires imp3d;
	requires math;
	requires platform;
	requires platform.core;
	requires raytracer;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires java.datatransfer;
	requires java.desktop;
	requires java.logging;
	requires jocl;
	
	opens de.grogra.gpuflux to utilities;
}