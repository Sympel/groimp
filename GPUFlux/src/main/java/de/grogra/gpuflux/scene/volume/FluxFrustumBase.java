package de.grogra.gpuflux.scene.volume;

public abstract class FluxFrustumBase extends FluxPrimitive {
	protected static final int FRUSTUM_CYLINDER = 0x1;
	protected static final int TOP_OPEN = 0x2;
	protected static final int BASE_OPEN = 0x4;
	protected static final int UV_ROTATED = 0x8;
	
	
}
