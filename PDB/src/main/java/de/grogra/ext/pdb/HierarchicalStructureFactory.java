
package de.grogra.ext.pdb;

import de.grogra.ext.pdb.model.AminoAcid;
import de.grogra.ext.pdb.model.AminoSequence;
import de.grogra.ext.pdb.model.Atom;
import de.grogra.ext.pdb.model.IProtein;
import de.grogra.ext.pdb.model.Protein;

/**
 * @author fdill, mdube
 * This class creates a hierarchical data structure of the protein using the given pdb entries.
 * For every ATOM entry in the PDB file a new atom is added.
 * The hierarchical data structure is organized as follows:
 * Protein contains a vector of
 * AminoSequences which contains a vector of
 * AminoAcids which contains a vector of
 * Atoms.
 */
public class HierarchicalStructureFactory implements ProteinFactory
{

	private final static String SEQUENCE_TERMINATION = "OXT";
	private boolean aminoOpen = false;
	private boolean sequenceOpen = false;
	private AminoAcid currentAmino;
	private AminoSequence currentSequence;

	/**
	 * 
	 * @uml.property name="protein"
	 */
	private Protein protein;

	public HierarchicalStructureFactory ()
	{
		new HierarchicalStructureFactory ("unknown protein");
	}

	public HierarchicalStructureFactory (String proteinName)
	{
		System.out.println ("start processing protein data model...");
		protein = new Protein (proteinName);
	}

	/**
	 * @see ProteinFactory
	 */
	public void processAtomEntry (String atomName, String aminoName,
			int resSeqNr, float x, float y, float z)
	{
		if (!sequenceOpen)
		{
			openAminoSequence ();
		}
		if (!aminoOpen && (!atomName.equals (SEQUENCE_TERMINATION)))
		{
			openAminoAcid (aminoName, resSeqNr);
		}
		if (resSeqNr != currentAmino.getResSeqNr ())
		{
			closeAminoAcid ();
			openAminoAcid (aminoName, resSeqNr);
		}
		openAtom (atomName, x, y, z);
	}

	private void openAtom (String name, float x, float y, float z)
	{
		//System.out.print(" : " + name);
		if (aminoOpen && sequenceOpen)
		{
			Atom atom = new Atom (name, x, y, z);
			currentAmino.addAtom (atom);
		}
		if (name.equals (SEQUENCE_TERMINATION))
		{
			closeAminoAcid ();
			closeAminoSequence ();
		}

	}

	private void openAminoAcid (String name, int resSeqNr)
	{
		//System.out.println("open amino acid: " + name);
		if (!sequenceOpen)
		{
			openAminoSequence ();
		}
		aminoOpen = true;
		currentAmino = new AminoAcid (name, resSeqNr);
	}

	private void closeAminoAcid ()
	{
		//System.out.println(" \n close amino acid " + currentAmino.getName());
		if (aminoOpen)
		{
// RH20060629			
//			currentAmino.validate ();
			currentSequence.addAminoAcid (currentAmino);
			aminoOpen = false;
		}
		else
		{
			System.out.println ("Error while closing amino acid");
			//geht nich
		}
	}

	private void openAminoSequence ()
	{
		//System.out.println("opening amino sequence...");
		sequenceOpen = true;
		currentSequence = new AminoSequence ();
	}

	private void closeAminoSequence ()
	{
		//System.out.println("closing amino sequence " + ++sequenceCounter);
		sequenceOpen = false;
		protein.addAminoSequence (currentSequence);
	}

	/**
	 * @see ProteinFactory
	 */
	public void closeProtein ()
	{
		// RH20060629
		if (aminoOpen)
			closeAminoAcid ();
		if (sequenceOpen)
			closeAminoSequence ();
		// RH20060629

		System.out.println ("processed " + AminoSequence.numberOfSequences
				+ " sequences, " + AminoAcid.numberOfAcids
				+ " amino acids and " + Atom.numberOfAtoms + " atoms.");
		System.out.println ();
		System.out.println ();
		sequenceOpen = true;
	}

	/**
	 * @see ProteinFactory
	 * 
	 * @uml.property name="protein"
	 */
	public IProtein getProtein ()
	{
		return protein;
	}

}
