package de.grogra.ext.pdb.model;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author fdill, mdube
 * The protein contains the sequences as a vector. It also provides a text output.
 * 
 * RH20060629 changed variable name 'enum' to 'e' to avoid problems with java 1.5
 */
public class Protein implements IProtein{
	
	/**
	 * @param proteinName - the name of the protein
	 */
	public Protein(String proteinName) {
		name = proteinName;
	}

	/**
	 * 
	 * @uml.property name="name"
	 */
	private String name;

	
	private Vector sequences = new Vector();
	
	
	public Protein(){
		sequences = new Vector();
	}
	/**
	 * 
	 * @param sequence - a sequence to be added to this protein
	 */
	public void addAminoSequence(AminoSequence sequence){
		this.sequences.add(sequence);
	}

	/**
	 * text output for testing
	 */
	public void textOutput() {
		AminoSequence seq;
		//System.out.println("new sequence: \n");
		for(Enumeration e = sequences.elements(); e.hasMoreElements();){
			seq = (AminoSequence)e.nextElement();
			seq.textOutput();
		}
	}
	/**
	 * 
	 * @return - a vector containing the amino sequences of the type @link AminoSequence of this protein
	 */
	public Vector getAminoSequences(){
		return sequences;
	}

	/**
	 * @return - the name of the protein
	 * 
	 * @uml.property name="name"
	 */
	public String getName() {
		return name;
	}

	
	/**
	 * @see de.grogra.ext.pdb.model.IProtein#getAcidName(int, int)
	 */
	public String getAcidName(int sequence, int acid) {
		return ((AminoAcid)((AminoSequence)sequences.get(sequence)).getAminoAcids().get(acid)).getName();
	}
	/** 
	 * @see de.grogra.ext.pdb.model.IProtein#getResSeqNr(int, int)
	 */
	public int getResSeqNr(int sequence, int acid) {
		return ((AminoAcid)((AminoSequence)sequences.get(sequence)).getAminoAcids().get(acid)).getResSeqNr();
	}
	/**
	 * @see de.grogra.ext.pdb.model.IProtein#getXCoord(int, int, int)
	 */
	public float getXCoord(int sequence, int acid, int atom) {
		return ((Atom)((AminoAcid)((AminoSequence)sequences.get(sequence)).getAminoAcids().get(acid)).getAtoms().get(atom)).getXCoord();
	}
	/**
	 * @see de.grogra.ext.pdb.model.IProtein#getYCoord(int, int, int)
	 */
	public float getYCoord(int sequence, int acid, int atom) {
		return ((Atom)((AminoAcid)((AminoSequence)sequences.get(sequence)).getAminoAcids().get(acid)).getAtoms().get(atom)).getYCoord();
	}
	/**
	 * @see de.grogra.ext.pdb.model.IProtein#getZCoord(int, int, int)
	 */
	public float getZCoord(int sequence, int acid, int atom) {
		return ((Atom)((AminoAcid)((AminoSequence)sequences.get(sequence)).getAminoAcids().get(acid)).getAtoms().get(atom)).getZCoord();
	}
	/**
	 * @see de.grogra.ext.pdb.model.IProtein#getElement(int, int, int)
	 */
	public String getElement(int sequence, int acid, int atom) {
		return ((Atom)((AminoAcid)((AminoSequence)sequences.get(sequence)).getAminoAcids().get(acid)).getAtoms().get(atom)).getElement();
	}
	/**
	 * @see de.grogra.ext.pdb.model.IProtein#getAtomName(int, int, int)
	 */
	public String getAtomName(int sequence, int acid, int atom) {
		return ((Atom)((AminoAcid)((AminoSequence)sequences.get(sequence)).getAminoAcids().get(acid)).getAtoms().get(atom)).getAtomName();
	}
	/**
	 * @see de.grogra.ext.pdb.model.IProtein#getNumberOfSequences()
	 */
	public int getNumberOfSequences() {
		return sequences.size();
	}
	/**
	 * @see de.grogra.ext.pdb.model.IProtein#getNumberOfAcids(int)
	 */
	public int getNumberOfAcids(int sequence) {
		return ((AminoSequence)sequences.get(sequence)).getAminoAcids().size();
	}
	/**
	 * @see de.grogra.ext.pdb.model.IProtein#getNumberOfAtoms(int, int)
	 */
	public int getNumberOfAtoms(int sequence, int acid) {
		return ((AminoAcid)((AminoSequence)sequences.get(sequence)).getAminoAcids().get(acid)).getAtoms().size();		
	}
}
