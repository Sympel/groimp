module pdb {
	exports de.grogra.ext.pdb.model;
	exports de.grogra.ext.pdb;
	exports de.grogra.ext.pdb.view;

	requires graph;
	requires imp3d;
	requires math;
	requires platform;
	requires platform.core;
	requires raytracer;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires java.desktop;
	
	opens de.grogra.ext.pdb to utilities;
}