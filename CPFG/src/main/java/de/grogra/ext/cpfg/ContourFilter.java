
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.cpfg;

import java.io.*;
import de.grogra.grammar.*;
import de.grogra.util.*;
import de.grogra.math.*;
import de.grogra.pf.io.*;

public class ContourFilter extends FilterBase implements ObjectSource
{
	private static final IOFlavor FLAVOR
		= IOFlavor.valueOf (BSplineOfVertices.class);

	private static final int OPEN = Token.MIN_UNUSED;
	private static final int CLOSED = Token.MIN_UNUSED + 1;


	public ContourFilter (FilterItem item, FilterSource source)
	{
		super (item, source);
		setFlavor (FLAVOR);
	}


	public Object getObject () throws IOException
	{
		Tokenizer t = new Tokenizer (Tokenizer.FLOAT_IS_DEFAULT
									 | Tokenizer.MINUS_IS_SIGN
									 | Tokenizer.EVALUATE_NUMBERS);
		t.addToken (OPEN, "open");
		t.addToken (CLOSED, "closed");
		t.setSource (((ReaderSource) source).getReader (),
					 source.getSystemId ());

		try
		{
			int n = t.getInt (), dim = t.getInt ();
			float[] data = new float[n *= dim];
			boolean periodic = t.getToken ().getType () == CLOSED;
			for (int i = 0; i < n; i++)
			{
				data[i] = t.getFloat ();
			}
			return new BSplineOfVertices (new VertexListImpl (data, dim), 3, periodic, false);
		}
		catch (RecognitionException e)
		{
			throw new IOWrapException (e);
		}
		finally
		{
			t.getInput ().close ();
		}
	}

}
