// $ANTLR 2.7.6 (2005-12-22): "Surface.g" -> "SurfaceParser.java"$


/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.cpfg;

import javax.vecmath.*;
import de.grogra.grammar.*;
import de.grogra.graph.*;
import de.grogra.math.*;
import de.grogra.imp3d.objects.*;
import de.grogra.imp3d.objects.Attributes;

import antlr.TokenBuffer;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.ANTLRException;
import antlr.LLkParser;
import antlr.Token;
import antlr.TokenStream;
import antlr.RecognitionException;
import antlr.NoViableAltException;
import antlr.MismatchedTokenException;
import antlr.SemanticException;
import antlr.ParserSharedInputState;
import antlr.collections.impl.BitSet;

public class SurfaceParser extends antlr.LLkParser       implements SurfaceTokenTypes
 {

	private float scale;
	private Vector3d v0 = new Vector3d (), v1 = new Vector3d (),
		v2 = new Vector3d (), v3 = new Vector3d ();

	private static void setRow (Matrix4d m, int row, Vector3d v)
	{
		m.setRow (row, v.x, v.y, v.z, 0);
	}

protected SurfaceParser(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
}

public SurfaceParser(TokenBuffer tokenBuf) {
  this(tokenBuf,1);
}

protected SurfaceParser(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
}

public SurfaceParser(TokenStream lexer) {
  this(lexer,1);
}

public SurfaceParser(ParserSharedInputState state) {
  super(state,1);
  tokenNames = _tokenNames;
}

	public final Null  surfaceDefinition() throws RecognitionException, TokenStreamException {
		Null root;
		
		
				TVector3d v = new TVector3d ();
				root = new Null (v);
				TMatrix4d m = new TMatrix4d ();
				Null t = new Null (m);
				root.appendBranchNode (t);
				NURBSSurface s;
				float size;
			
		
		try {      // for error handling
			number();
			number();
			number();
			number();
			number();
			number();
			{
			switch ( LA(1)) {
			case LITERAL_PRECISION:
			{
				match(LITERAL_PRECISION);
				match(LITERAL_S);
				match(COLON);
				match(INT_LITERAL);
				match(LITERAL_T);
				match(COLON);
				match(INT_LITERAL);
				break;
			}
			case LITERAL_CONTACT:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(LITERAL_CONTACT);
			match(LITERAL_POINT);
			point(v3);
			match(LITERAL_END);
			match(LITERAL_POINT);
			point(v);
			match(LITERAL_HEADING);
			point(v2);
			match(LITERAL_UP);
			point(v1);
			match(LITERAL_SIZE);
			match(COLON);
			scale=number();
			
						scale = 1 / scale;
						v1.scale (-1);
						v2.normalize ();
						v0.cross (v1, v2);
						v0.normalize ();
						v1.cross (v2, v0);
						setRow (m, 0, v0);
						setRow (m, 1, v1);
						setRow (m, 2, v2);
						v3.scale (scale);
						m.transform (v3);
						v.scale (scale);
						m.transform (v);
						m.m03 = -v.x;
						m.m13 = -v.y;
						m.m23 = -v.z;
						v.sub (v3);
					
			{
			int _cnt4=0;
			_loop4:
			do {
				if ((LA(1)==IDENT)) {
					s=patch();
					t.addEdgeBitsTo (s, Graph.BRANCH_EDGE, null);
				}
				else {
					if ( _cnt4>=1 ) { break _loop4; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt4++;
			} while (true);
			}
			match(Token.EOF_TYPE);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_0);
		}
		return root;
	}
	
	public final float  number() throws RecognitionException, TokenStreamException {
		float n;
		
		Token  i = null;
		Token  f = null;
		n = 0;
		
		try {      // for error handling
			switch ( LA(1)) {
			case INT_LITERAL:
			{
				i = LT(1);
				match(INT_LITERAL);
				n = ((IntLiteral) i).intValue ();
				break;
			}
			case FLOAT_LITERAL:
			{
				f = LT(1);
				match(FLOAT_LITERAL);
				n = ((FloatLiteral) f).floatValue ();
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_1);
		}
		return n;
	}
	
	public final void point(
		Tuple3d p
	) throws RecognitionException, TokenStreamException {
		
		float x, y, z;
		
		try {      // for error handling
			match(LITERAL_X);
			match(COLON);
			x=number();
			match(LITERAL_Y);
			match(COLON);
			y=number();
			match(LITERAL_Z);
			match(COLON);
			z=number();
			
						p.set (x, y, z);
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_2);
		}
	}
	
	public final NURBSSurface  patch() throws RecognitionException, TokenStreamException {
		NURBSSurface s;
		
		Token  n = null;
		
				float[] data = new float[48];
				s = new NURBSSurface (new BezierSurface (data, 3, 4));
				s.setVisibleSides (Attributes.VISIBLE_SIDES_BOTH);
			
		
		try {      // for error handling
			n = LT(1);
			match(IDENT);
			s.setName (n.getText ());
			{
			switch ( LA(1)) {
			case LITERAL_TOP:
			{
				match(LITERAL_TOP);
				material();
				match(LITERAL_BOTTOM);
				material();
				break;
			}
			case LITERAL_AL:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			match(LITERAL_AL);
			match(COLON);
			neighbor();
			match(LITERAL_A);
			match(COLON);
			neighbor();
			match(LITERAL_AR);
			match(COLON);
			neighbor();
			match(LITERAL_L);
			match(COLON);
			neighbor();
			match(LITERAL_R);
			match(COLON);
			neighbor();
			match(LITERAL_BL);
			match(COLON);
			neighbor();
			match(LITERAL_B);
			match(COLON);
			neighbor();
			match(LITERAL_BR);
			match(COLON);
			neighbor();
			row(data, 0);
			row(data, 12);
			row(data, 24);
			row(data, 36);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_3);
		}
		return s;
	}
	
	public final void material() throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			match(LITERAL_COLOR);
			match(COLON);
			match(INT_LITERAL);
			match(LITERAL_DIFFUSE);
			match(COLON);
			number();
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_4);
		}
	}
	
	public final void neighbor() throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			switch ( LA(1)) {
			case 37:
			{
				match(37);
				break;
			}
			case IDENT:
			{
				match(IDENT);
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_5);
		}
	}
	
	public final void row(
		float[] data, int index
	) throws RecognitionException, TokenStreamException {
		
		
		try {      // for error handling
			vertex(data, index);
			vertex(data, index + 3);
			vertex(data, index + 6);
			vertex(data, index + 9);
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_6);
		}
	}
	
	public final void vertex(
		float[] data, int index
	) throws RecognitionException, TokenStreamException {
		
		float x, y, z;
		
		try {      // for error handling
			x=number();
			y=number();
			z=number();
			
						data[index] = x * scale;
						data[index + 1] = y * scale;
						data[index + 2] = z * scale;
					
		}
		catch (RecognitionException ex) {
			reportError(ex);
			recover(ex,_tokenSet_6);
		}
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"BOOLEAN_LITERAL",
		"INT_LITERAL",
		"LONG_LITERAL",
		"FLOAT_LITERAL",
		"DOUBLE_LITERAL",
		"CHAR_LITERAL",
		"STRING_LITERAL",
		"<identifier>",
		"\":\"",
		"\"PRECISION\"",
		"\"S\"",
		"\"T\"",
		"\"CONTACT\"",
		"\"POINT\"",
		"\"END\"",
		"\"HEADING\"",
		"\"UP\"",
		"\"SIZE\"",
		"\"TOP\"",
		"\"BOTTOM\"",
		"\"AL\"",
		"\"A\"",
		"\"AR\"",
		"\"L\"",
		"\"R\"",
		"\"BL\"",
		"\"B\"",
		"\"BR\"",
		"\"X\"",
		"\"Y\"",
		"\"Z\"",
		"\"COLOR\"",
		"\"DIFFUSE\"",
		"\"~\""
	};
	
	private static final long[] mk_tokenSet_0() {
		long[] data = { 2L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = { 25798977698L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = { 3932160L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = { 2050L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	private static final long[] mk_tokenSet_4() {
		long[] data = { 25165824L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_4 = new BitSet(mk_tokenSet_4());
	private static final long[] mk_tokenSet_5() {
		long[] data = { 4261413024L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_5 = new BitSet(mk_tokenSet_5());
	private static final long[] mk_tokenSet_6() {
		long[] data = { 2210L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_6 = new BitSet(mk_tokenSet_6());
	
	}
