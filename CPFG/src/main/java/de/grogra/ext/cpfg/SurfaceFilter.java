
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.cpfg;

import java.io.*;
import de.grogra.grammar.*;
import de.grogra.imp3d.objects.*;
import de.grogra.pf.io.*;
import de.grogra.util.*;

public class SurfaceFilter extends FilterBase implements ObjectSource
{
	public static final IOFlavor FLAVOR = IOFlavor.valueOf (Null.class);


	public static Null read (String name) throws IOException
	{
		return (Null) ((ObjectSource) IO.createPipeline
					   (FileSource.createFileSource
						(name, new MimeType ("text/x-cpfg-surface", null),
						 de.grogra.pf.registry.Registry.current (), null), FLAVOR))
			.getObject ();
	}


	public SurfaceFilter (FilterItem item, FilterSource source)
	{
		super (item, source);
		setFlavor (FLAVOR);
	}


	public Object getObject () throws IOException
	{
		Tokenizer t = new Tokenizer (Tokenizer.CREATE_TOKEN_LOCATOR
									 | Tokenizer.FLOAT_IS_DEFAULT
									 | Tokenizer.MINUS_IS_SIGN
									 | Tokenizer.EVALUATE_NUMBERS);

/*!!
#set ($INSTANCE = "t.")
#set ($INTERFACE = "SurfaceTokenTypes.")

#parse ("de/grogra/ext/cpfg/SurfaceTokenizer.inc")
!!*/
//!! #* Start of generated code
// generated
	t.addToken (SurfaceTokenTypes.COLON, ":");
	t.addToken (SurfaceTokenTypes.LITERAL_PRECISION, "PRECISION");
	t.addToken (SurfaceTokenTypes.LITERAL_S, "S");
	t.addToken (SurfaceTokenTypes.LITERAL_T, "T");
	t.addToken (SurfaceTokenTypes.LITERAL_CONTACT, "CONTACT");
	t.addToken (SurfaceTokenTypes.LITERAL_POINT, "POINT");
	t.addToken (SurfaceTokenTypes.LITERAL_END, "END");
	t.addToken (SurfaceTokenTypes.LITERAL_HEADING, "HEADING");
	t.addToken (SurfaceTokenTypes.LITERAL_UP, "UP");
	t.addToken (SurfaceTokenTypes.LITERAL_SIZE, "SIZE");
	t.addToken (SurfaceTokenTypes.LITERAL_TOP, "TOP");
	t.addToken (SurfaceTokenTypes.LITERAL_BOTTOM, "BOTTOM");
	t.addToken (SurfaceTokenTypes.LITERAL_AL, "AL");
	t.addToken (SurfaceTokenTypes.LITERAL_A, "A");
	t.addToken (SurfaceTokenTypes.LITERAL_AR, "AR");
	t.addToken (SurfaceTokenTypes.LITERAL_L, "L");
	t.addToken (SurfaceTokenTypes.LITERAL_R, "R");
	t.addToken (SurfaceTokenTypes.LITERAL_BL, "BL");
	t.addToken (SurfaceTokenTypes.LITERAL_B, "B");
	t.addToken (SurfaceTokenTypes.LITERAL_BR, "BR");
	t.addToken (SurfaceTokenTypes.LITERAL_X, "X");
	t.addToken (SurfaceTokenTypes.LITERAL_Y, "Y");
	t.addToken (SurfaceTokenTypes.LITERAL_Z, "Z");
	t.addToken (SurfaceTokenTypes.LITERAL_COLOR, "COLOR");
	t.addToken (SurfaceTokenTypes.LITERAL_DIFFUSE, "DIFFUSE");
	t.addToken (37, "~");
//!! *# End of generated code

		t.setSource (((ReaderSource) source).getReader (),
					 source.getSystemId ());
		SurfaceParser p = new SurfaceParser (t);
		p.setFilename (source.getSystemId ());
		try
		{
			return p.surfaceDefinition ();
		}
		catch (antlr.ANTLRException e)
		{
			throw new IOFormatException
				(RecognitionException.convert (e, p.getTokenNames ()));
		}
		finally
		{
			t.getInput ().close ();
		}
	}

}
