module cpfg {
	exports de.grogra.ext.cpfg;

	requires grammar;
	requires graph;
	requires imp;
	requires imp3d;
	requires math;
	requires platform.core;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires antlr;
	requires java.desktop;
	requires java.xml;
}