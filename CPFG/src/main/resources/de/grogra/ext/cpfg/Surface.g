header
{

/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.cpfg;

import javax.vecmath.*;
import de.grogra.grammar.*;
import de.grogra.graph.*;
import de.grogra.math.*;
import de.grogra.imp3d.objects.*;
import de.grogra.imp3d.objects.Attributes;
}


class SurfaceParser extends Parser;


options
{
	importVocab = Tokenizer;
	exportVocab = Surface;
	k = 1;
}


tokens
{
	COLON = ":";
}


{
	private float scale;
	private Vector3d v0 = new Vector3d (), v1 = new Vector3d (),
		v2 = new Vector3d (), v3 = new Vector3d ();

	private static void setRow (Matrix4d m, int row, Vector3d v)
	{
		m.setRow (row, v.x, v.y, v.z, 0);
	}
}


surfaceDefinition returns [Null root]
	{
		TVector3d v = new TVector3d ();
		root = new Null (v);
		TMatrix4d m = new TMatrix4d ();
		Null t = new Null (m);
		root.addChild (t);
		NURBSSurface s;
		float size;
	}
	:   number number number number number number
		("PRECISION" "S" COLON INT_LITERAL "T" COLON INT_LITERAL)?
		"CONTACT" "POINT" point[v3]
		"END" "POINT" point[v]
		"HEADING" point[v2]
		"UP" point[v1]
		"SIZE" COLON scale=number
		{
			scale = 1 / scale;
			v1.scale (-1);
			v2.normalize ();
			v0.cross (v1, v2);
			v0.normalize ();
			v1.cross (v2, v0);
			setRow (m, 0, v0);
			setRow (m, 1, v1);
			setRow (m, 2, v2);
			v3.scale (scale);
			m.transform (v3);
			v.scale (scale);
			m.transform (v);
			m.m03 = -v.x;
			m.m13 = -v.y;
			m.m23 = -v.z;
			v.sub (v3);
		}
		(s=patch {t.addEdgeBitsTo (s, Graph.BRANCH_EDGE, null);})+
		EOF
	;


patch returns [NURBSSurface s]
	{
		float[] data = new float[48];
		s = new NURBSSurface (new BezierSurface (data, 3, 4));
		s.setVisibleSides (Attributes.VISIBLE_SIDES_BOTH);
	}
	:   n:IDENT {s.setName (n.getText ());}
		("TOP" material "BOTTOM" material)?
		"AL" COLON neighbor
		"A" COLON neighbor
		"AR" COLON neighbor
		"L" COLON neighbor
		"R" COLON neighbor
		"BL" COLON neighbor
		"B" COLON neighbor
		"BR" COLON neighbor

		row[data, 0]
		row[data, 12]
		row[data, 24]
		row[data, 36]
	;


row[float[] data, int index]
	:   vertex[data, index]
		vertex[data, index + 3]
		vertex[data, index + 6]
		vertex[data, index + 9]
	;


point[Tuple3d p]
	{ float x, y, z; }
	:   "X" COLON x=number "Y" COLON y=number "Z" COLON z=number
		{
			p.set (x, y, z);
		}
	;


material
	:   "COLOR" COLON INT_LITERAL "DIFFUSE" COLON number
	;


neighbor
	:   "~" | IDENT
	;


vertex[float[] data, int index]
	{ float x, y, z; }
	:   x=number y=number z=number
		{
			data[index] = x * scale;
			data[index + 1] = y * scale;
			data[index + 2] = z * scale;
		}
	;


number returns [float n]
	{ n = 0; }
	:   i:INT_LITERAL { n = ((IntLiteral) i).intValue (); }
	|   f:FLOAT_LITERAL { n = ((FloatLiteral) f).floatValue (); }
	;
